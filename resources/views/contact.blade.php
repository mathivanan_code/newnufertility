@extends('layouts/main')
@section('meta-title')
<title>Contact Us |Best Andrologist & Urology Specialist |Nu fertility</title>

@endsection
@section('meta-description')
<meta name="description" content="NU Fertility Contacts" />

@endsection



@section('content')
<style>
.ip-banner{
background-image: url(images/Contact-Us.jpg) !important;
background-repeat: no-repeat;
background-size: 100% 100% !important;
background-size: cover;
margin: 0 auto;
height:400px;
}
.contact-us{
    font-family:contact-us;
}
.empty-space{height:6.6667rem !important;}
.contact-default{
    position:relative;
    padding-top:0.1px;
}
.contact-default h2{
line-height:1.2rem;
font-size: 30px !important;
font-family: poppins !important;
letter-spacing: .05rem !important;
}
.contact-title + .contact-sub-title{
    margin-top: 1.15em;
}
.contact-default h6{
    color:#000;font-family:Poppins, sans-serif;
    font-size: 14px;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 0.2px;
    line-height: 1.5em;
}
.contact-sub-title + .form-default{margin-top:3.45rem;}
.contact-default .trx_addons_columns_wrap{margin-right:0px;margin-left:0px;}
.contact-default .trx_addons_columns_wrap::before{content:"";display:table;}
.contact-default .form-default {color:#000000;}
.contact-default .form-default .form-select{background: none !important;
    border: none;color: #000000;
   font-size:1em;
    border-color: #e5e5e5;
    width: 100%;
    padding: 1em 3.3em;
    -webkit-border-radius: 3em;
    -moz-border-radius: 3em;
    border-radius: 3em;
    padding-right: 5em !important;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    box-sizing: border-box;
    border: 1px solid;
    height: 5.05em; line-height: 5.05em;-webkit-transition: all ease .3s;
    -moz-transition: all ease .3s;
    -ms-transition: all ease .3s;
    -o-transition: all ease .3s;
    transition: all ease .3s;}
    
    .contact-default .form-default .form-select-container {position: relative;}
    .contact-default .form-default .form-select-container select{
appearance:none;padding-top:0rem;
 
}

    .contact-default .form-default .form-select-container:before {
    content: ' ';
    display: block;
    position: absolute;
    right: 0;
    top: 0;
    bottom: 0;
    width: 3em;
    z-index: 1;
    pointer-events: none;
    cursor: pointer;
}
    .contact-default .form-default .form-select-container:after {
    content: "\f107";
    font-family:  "FontAwesome";
    display: block;
    line-height: 1em;
    width: 1.6em;
    height: 1em;
    text-align: center;
    position: absolute;
    right: 1em;
    top: 34%;
    z-index: 2;
    pointer-events: none;
    cursor: pointer;
    font-size: 1.6em;
} 
.contact-default .form-default .form-select-container:after{color:#fff;}

.contact-default .form-default .form-select-container:hover:after{color:#000;}
.contact-default .form-default .form_field {    margin-bottom: 1.3333em;}
input[type="text"], input[type="number"],
 input[type="email"], input[type="tel"], 
 input[type="search"], input[type="password"],
  textarea, textarea.wp-editor-area, .select_container, select, .select_container select{
    font-family: 'Poppins', sans-serif;
    font-size: 0.8em;
    font-weight: 400;
    font-style: normal;
    line-height: 1.2em;
    text-decoration: none;
    text-transform: none;}
    .contact-default .form-default input[type="text"],
    .contact-default .form-default input[type="email"],
    .contact-default .form-default textarea{

     
        color: #000000;font-family:sen;width:100%;
    border-color: #e5e5e5;  
    background-color: #ffffff;      
        border: 1px solid #eee;
    font-size: 1em;
    line-height: 5.1em;
    font-style: normal;
    padding: 0 3.3em;
    height: 5.1em;
    -webkit-border-radius: 3em;
    -moz-border-radius: 3em;
    border-radius: 3em;
    max-width: 100%;
    margin-bottom: 5px;
  
}

.contact-default .form-default .form-btn{float:right;}
.contact-default .form-default .form-btn button{color: #ffffff;
margin-bottom:20px;

background:#00aff0;background:linear-gradient(to right, #f6a2de 50%, #00aff0 50%) no-repeat scroll right bottom / 210% 100% #00aff0;
    background-size: 200%;
    transition: .5s ease-out;cursor: pointer;
    display: inline-block;
    text-transform: none;
    white-space: nowrap;
    padding: 1.4em 3.9em 1.47em;
    font-size: 1.071em;
    line-height: 1em;
    font-weight: 600;
    letter-spacing: 0;
    border: none;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    -webkit-border-radius: 3em;
    -moz-border-radius: 3em;
    border-radius: 3em;
}

    .contact-default .form-default .form-btn button:hover {
    color: #ffffff !important;

    background-position: left; 
    }
.confirm-hover{background:linear-gradient(to right, #f6a2de 50%, #00aff0 50%) no-repeat scroll right bottom / 210% 100% #00aff0 !important;}
.contact-default .form-default .form-checkbox label{display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 700;}
textarea{overflow: auto;
    vertical-align: top;font:inherit;  font-weight: 700;width:100%;
    min-height: 13.5em;}
    textarea::-webkit-input-placeholder {
            color: #000;font-weight:700;
    }
    textarea:-moz-placeholder { /* Upto Firefox 18, Deprecated in Firefox 19  */
            color: #000;font-weight:700;
    }
    textarea::-moz-placeholder {  /* Firefox 19+ */
            color: #000;font-weight:700;  
    }
    textarea:-ms-input-placeholder {  
       color: #000;font-weight:700;  
    }
    .contact-default .form-default input[type="text"]:hover,
     .contact-default .form-default input[type="password"]:hover,
      .contact-default .form-default input[type="email"]:hover,
       .contact-default .form-default input[type="number"]:hover,
        .contact-default .form-default input[type="tel"]:hover,
         .contact-default .form-default input[type="search"]:hover,
          .contact-default .form-default textarea:hover
          {border-color:#000;}

          .contact-default .form-default input[type="text"]:focus,
     .contact-default .form-default input[type="password"]:focus,
      .contact-default .form-default input[type="email"]:focus,
       .contact-default .form-default input[type="number"]:focus,
        .contact-default .form-default input[type="tel"]:focus,
         .contact-default .form-default input[type="search"]:focus,
          .contact-default .form-default textarea:focus
          {border:1px solid #3eb8d7;}

    button, input, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;

}
input[type="radio"], input[type="checkbox"]{display:none;}
.column-container{
    padding-left: 0px;padding-right: 0px;
}
.column-inner{background-color: #f6a2de;
    min-height: 50%;
}
.column-inner::after, .column-inner::before {
    content: " ";
    display: table;
}
.col-lg-8 .row{margin-left:-30px;margin-right:0px;}
.column-container h5{font-size: 1.229em;
    color: #000000b5;}
    .column-container p{padding-left: 0px;color: #ffffff;}
    .column-container p a{font-size: 0.9em; font-weight: 600;color:#fff;}
    .column-container .height-medium-plus{
        height:5.4rem !important;
    }
    .column-container .height-medium-plus1{
        height:4.02rem !important;
    }
.column-container1{
    padding-left: 5px;padding-right: 0px;}
    .google-map-inner{position:relative;height:350px;width:100%;}
    .top-p{padding-left: 5px; margin-top:30px;margin-bottom:20px;}
   
   
    @media screen and (min-width: 1024px) and (max-width: 1200px) {
        .column-container p a{font-size: 0.7em;}
        .column-container .height-medium-plus1{height:6.9em !important;}
        .col-lg-8 .row{margin-left:-26px;margin-right:0px;}
        .contact-default .form-default input[type="text"],
    .contact-default .form-default input[type="email"],
    .contact-default .form-default textarea,
    .contact-default .form-default .form-select,.contact-default .form-default .form-btn button{
    font-size:0.8em;padding-right:2.3em;padding-left:2.3em;}
    }

    @media screen and (min-width: 768px) and (max-width: 900px) {
        
        .col-lg-8 .row{margin-left:-10px;margin-right:0px;}
        .column-container p a{font-size: 0.8em;}
        
            }
            


    @media screen and (max-width: 479px) {
     .contact-us{   width: 100%;
    max-width: 270px;
    margin-left: auto;
    margin-right: auto;
        
    }
    .col-lg-8 .row{
        margin:0px;
    }
    .col-lg-6.col-md-6.col-sm-12 .column-inner{padding-left:30px;padding-right:30px;}
    .column-container .height-medium-plus1,.column-container .height-medium-plus{height:2.4rem !important;}
    .hide_on_mobile{display:none;}
    .column-container p a{font-size: 1em;}
    .content-element{padding-left:30px;text-align:left;padding-right:30px;p}
    }
</style>
<script>
$(".confirm").hover(
  function () {
    $(this).addClass("confirm-hover");
  },
  function () {
    $(this).removeClass("confirm-hover");
  }
);
</script>
<div class="ip-banner">

</div>
</div>
<div class="container-fluid contact-us">
<div class="row">
    <div class="col-lg-4 col-md-12 col-sm-12">
<div class="row">
    <div class="col-lg-2 col-md-2 col-sm-1"></div>
    <div class="col-lg-9 col-md-9 col-sm-10">
<div class="column-default">
    <div class="empty-space">
        
    </div>
    <div class="contact-default">
        <h2 class="contact-title">Drop a Line</h2>
        <h6 class="contact-sub-title">Contact Us</h6>
<form id="contact-form" method="post" class="form-default">
<div class="trx_addons_columns_wrap"></div>
<div class="mb-3 mt-3 form_field">
    <input type="text"   id="name" placeholder="Your name" name="name"/>
  </div>
  <div class="mb-3 mt-3 form_field">
    <input type="email"   id="email" placeholder="Your e-mail" name="email"/>
    </div>
<div class="mb-3 mt-3 form_field form-select-container">
    <select class="form-select" name="your-location" id="your-location">
            <option value="">Select Location</option>
            <option value="Bangalore">Bangalore</option>
            <option value="Shivamogga">Shivamogga</option>
            <option value="Bangalore+ Shivamogga">Bangalore+ Shivamogga</option>
            <option value="Maldives (Male’)">Maldives (Male’)</option>
        </select>
      


  </div>
<div class="mb-3 mt-3 form_field">
    <textarea  name="message" id="message" aria-required="true" placeholder="Your message"></textarea>
</div>
<div class="mb-3 mt-3 form_field form-checkbox">
        <input type="checkbox" id="i_agree_privacy_policy_sc_form_1" name="i_agree_privacy_policy" class="sc_form_privacy_checkbox inited" value="1">
        <input type="hidden" name="bot_check" value="">
        <label for="i_agree_privacy_policy_sc_form_1">I agree that my submitted data is being collected and stored.</label>
    
</div>

<div class="mb-3 mt-3 form_field form-btn">
<button class="confirm" id="confirm">Send Message</button>
</div>
</form>
    </div>
</div>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1"></div>
</div>
    </div><!--col-4 end -->
    
    <div class="col-lg-8 col-md-12 col-sm-12 column-container">
       <div class="row">
           <div class="col-lg-6 col-md-12 col-sm-12 column-container">
<div class="column-inner">
    <div class="row column-container">
        <div class="col-lg-1 col-md-1 col-sm-1 column-container"></div>
        <div class="col-lg-5 col-md-5 col-sm-5 column-container">
            <div class="height-medium-plus"></div>
            <div class="content-element">
                <h5>NU Fertility @ NU Hospitals Rajajinagar (West)
</h5>
<p ># 4/1, West of Chord Road, Near to ISKCON, Rajajinagar, Bengaluru, Karnataka, 560010. India</p>
           
<h5>
For International Patients
</h5>
<p><a href="tel:+91 76-25063883">+91 76-25063883</a></p>
<p ><a href="mailto: connect@nuhospitals.com">connect@nuhospitals.com</a></p>

</div>
            <div class="height-medium-plus1 hide_on_mobile"></div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1 column-container"></div>
        <div class="col-lg-5 col-md-5 col-sm-5 column-container">
            
            <div class="height-medium-plus"></div>
            <div class="content-element">
                <h5>Contact us on
</h5>
<p ><a href="tel:08046699999">08046699999</a></p>
      <br>     
<h5>Email
</h5><p><a href="mailto: fertility@nuhospitals.com">fertility@nuhospitals.com</a></p>
<p><a href="mailto: andrology@nuhospitals.com">andrology@nuhospitals.com</a></p>
<p><a href="mailto: care@nuhospitals.com">care@nuhospitals.com</a></p>
</div>
            <div class="height-medium-plus hide_on_mobile "></div>
        
        </div>
       </div>
</div>
<div id="google-map-inner" class="google-map-inner">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3887.3698227467175!2d77.55057701417618!3d13.012105390829252!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae3d785db91f89%3A0x8d697ed0f2a9f270!2sNU%20Hospitals%20(West)!5e0!3m2!1sen!2sin!4v1596805930200!5m2!1sen!2sin" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
           </div>
           <div class="col-lg-6 col-md-12 col-sm-12 column-container1">
            <div class="column-inner">
                <div class="row column-container">
                    <div class="col-lg-1 col-md-1 col-sm-1 column-container"></div>
                    <div class="col-lg-5 col-md-5 col-sm-5 column-container">
                        <div class="height-medium-plus"></div>
                        <div class="content-element">
                            <h5>NU Fertility @ NU Hospitals Padmanabhanagar (West)
                                                                                        
            </h5>
            <p >#6, 15th Main, 11th Cross, Padmanabhanagar, Bengaluru, Karnataka, 560070. India.</p>
                       
            <h5>
            For International Patients
            </h5>
            <p><a href="tel:+91 76-25063883">+91 76-25063883</a></p>
            <p ><a href="mailto: connect@nuhospitals.com">connect@nuhospitals.com</a></p>
            </div>
                        <div class="height-medium-plus hide_on_mobile"></div>
                    </div>
                    <div class="col-lg-1 col-md-1 col-sm-1 column-container"></div>
                    <div class="col-lg-5 col-md-5 col-sm-5 column-container">
                        
                        <div class="height-medium-plus hide_on_mobile"></div>
                        <div class="content-element">
                            <h5>Contact us on
            </h5>
            <p ><a href="tel:08046699999">08046699999</a></p>
                  <br>     
            <h5>Email
            </h5><p><a href="mailto: fertility@nuhospitals.com">fertility@nuhospitals.com</a></p>
            <p><a href="mailto: andrology@nuhospitals.com">andrology@nuhospitals.com</a></p>
            <p><a href="mailto: care@nuhospitals.com">care@nuhospitals.com</a></p>
            </div>
                        <div class="height-medium-plus hide_on_mobile"></div>
                    
                    </div>
                   </div>
            </div>
            
<div id="google-map-inner" class="google-map-inner">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3888.839779186148!2d77.55364451417492!3d12.918017290891058!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae3fd363270f71%3A0xf3140025205b1d78!2sNU%20Hospitals%20(South)!5e0!3m2!1sen!2sin!4v1596807048702!5m2!1sen!2sin" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> </div>         
        </div>
       </div>
       <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12 column-container top-p">
<div class="column-inner">
 <div class="row column-container">
     <div class="col-lg-1 col-md-1 col-sm-1 column-container"></div>
     <div class="col-lg-5 col-md-5 col-sm-5 column-container">
         <div class="height-medium-plus hide_on_mobile"></div>
         <div class="content-element">
             <h5>NU Hospitals Super Speciality Clinic – Shivamogga (Nephrology-Urology & Fertility Out-Patient Clinic)
</h5>
<p >No.6/2265, Kuvempu Road, Shivamogga-577201</p>
        
</div>
         <div class="height-medium-plus hide_on_mobile"></div>
     </div>
     <div class="col-lg-1 col-md-1 col-sm-1 column-container"></div>
     <div class="col-lg-5 col-md-5 col-sm-5 column-container">
         
         <div class="height-medium-plus hide_on_mobile"></div>
         <div class="content-element">
             <h5>Contact us on
</h5>
<p ><a href="tel:08182400444">08182400444</a></p>
<p ><a href="tel:+91 966 3032800">+91 966 3032800</a></p>
<p ><a href="tel:+91 9148555202">+91 9148555202</a></p>
   <br>     
<h5>Email
</h5><p><a href="mailto: fo.clinic@nuhospitals.com">fo.clinic@nuhospitals.com</a></p>
<p><a href="mailto: andrology@nuhospitals.com">andrology@nuhospitals.com</a></p>
<p><a href="mailto: care@nuhospitals.com">care@nuhospitals.com</a></p>
</div>
         <div class="height-medium-plus hide_on_mobile"></div>
     
     </div>
    </div>
</div>
<div id="google-map-inner" class="google-map-inner">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3872.347493549188!2d75.5665650141888!3d13.93790329022725!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bbba8d40331448b%3A0xeef35ae059d78b5d!2s6%2C%20Kuvempu%20Rd%2C%20Mission%20Compound%2C%20Shivamogga%2C%20Karnataka%20577201!5e0!3m2!1sen!2sin!4v1598391230993!5m2!1sen!2sin" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</div>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12 column-container1 top-p">
         <div class="column-inner">
             <div class="row column-container">
                 <div class="col-lg-1 col-md-1 col-sm-1 column-container"></div>
                 <div class="col-lg-5 col-md-5 col-sm-5 column-container">
                     <div class="height-medium-plus hide_on_mobile"></div>
                     <div class="content-element">
                         <h5>NU Hospitals – Shivamogga
                                                                                     
         </h5>
         <p >CA-P1, Machenahalli Industrial Area Bhadravathi Taluk, Shivamogga, Karnataka 577222</p>
                    
     
         </div>
                     <div class="height-medium-plus hide_on_mobile"></div>
                 </div>
                 <div class="col-lg-1 col-md-1 col-sm-1 column-container"></div>
                 <div class="col-lg-5 col-md-5 col-sm-5 column-container">
                     
                     <div class="height-medium-plus"></div>
                     <div class="content-element">
                         <h5>Contact us on
         </h5>
         <p ><a href="tel:+91 6366178822">+91 6366178822</a></p>
         <p ><a href="tel:+91 6366148822">+91 6366148822</a></p>
         <p ><a href="tel:+91 9148555202">+91 9148555202</a></p>
               <br>     
         <h5>Email
         </h5><p><a href="mailto: fo.shivamogga@nuhospitals.com">fo.shivamogga@nuhospitals.com</a></p>
         <p><a href="mailto: andrology@nuhospitals.com">andrology@nuhospitals.com</a></p>
         <p><a href="mailto: care@nuhospitals.com">care@nuhospitals.com</a></p>
         </div>
                     <div class="height-medium-plus hide_on_mobile"></div>
                 
                 </div>
                </div>
         </div>
         
<div id="google-map-inner" class="google-map-inner">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7746.834759189819!2d75.64944897186213!3d13.87397376107382!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bbbab839fb9bf23%3A0xd97fac1ffc8d1055!2sNU%20Hospitals%2CShivamogga!5e0!3m2!1sen!2sin!4v1598391786209!5m2!1sen!2sin" width="100%" height="350" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
 </div>         
     </div>
    </div>
    </div><!-- col - 8 end-->
</div>
</div>
@endsection
