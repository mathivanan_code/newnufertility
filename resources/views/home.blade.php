@extends('layouts/main')

@section('meta-title')
<title>Fertility hospital in Bangalore | Infertility Treatment in Bangalore | IVF Treatment | NU Fertility</title>
@endsection
@section('meta-description')
<meta name="description" content="Infertility Issues? NU Fertility is One Of The best Fertility Center in Bangalore.Our Hospital ISAR Accredited.Contact Us To Find Out Which Fertility Treatment Is Right For You." />
@endsection

@section('content')
<div class="container-Fluid">
    <style>
    /* Center the loader */
    #loader {
        position: relative;
        left: 50%;
        top: 50%;
        z-index: 1;
        width: 30px;
        height: 30px;
        margin: 140px 10px;
        border: 6px solid #f3f3f3;
        border-radius: 50%;
        border-top: 6px solid #3498db;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }

    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }

    /* Add animation to "page content" */
    .animate-bottom {
        position: relative;
        -webkit-animation-name: animatebottom;
        -webkit-animation-duration: 1s;
        animation-name: animatebottom;
        animation-duration: 1s
    }

    @-webkit-keyframes animatebottom {
        from {
            bottom: -100px;
            opacity: 0
        }

        to {
            bottom: 0px;
            opacity: 1
        }
    }

    @keyframes animatebottom {
        from {
            bottom: -100px;
            opacity: 0
        }

        to {
            bottom: 0;
            opacity: 1
        }
    }



   
    


    </style>





    <div>
        <style>
  
        .banner-carousel .owl-nav {
            /* position: absolute; */
            width: 100%;
            /* display: none !important; */
        }

        .banner-carousel .owl-nav button.owl-prev,
        .banner-carousel .owl-nav button.owl-next,
        .banner-carousel .owl-nav button.owl-prev:hover,
        .banner-carousel .owl-nav button.owl-next:hover {
            position: absolute;
            background: #00000063;
            color: #fff;
            width: 45px;
            height: 45px;
            font-size: 25px;
            margin: 0;
        }

        .banner-carousel .owl-nav button.owl-prev:hover,
        .banner-carousel .owl-nav button.owl-next:hover {
            background: #000000d4;
        }

        .banner-carousel .owl-nav button.owl-prev {
            left: 0;
            top: 32%;
        }

        .banner-carousel .owl-nav button.owl-next {
            left: 0;
            top: calc(32% + 46px);
        }

        .banner-carousel .owl-nav .owl-prev span,
        .banner-carousel .owl-nav .owl-next span {
            color: #fff;
        }

        @media(min-width: 768px) {

            .banner-carousel.owl-carousel .owl-stage,
            .banner-carousel.owl-carousel .slide-item {
                height: 100%;
                max-height: 650px;
                display: flex;
            }
        }
        .slide-item span{
            color: #000;
    font-weight: 600;
    font-size: 18px;
        }
        </style>


        <div id="loader"></div>


@php
$countryArray = array(
	'AD'=>array('name'=>'ANDORRA','code'=>'376'),
	'AE'=>array('name'=>'UNITED ARAB EMIRATES','code'=>'971'),
	'AF'=>array('name'=>'AFGHANISTAN','code'=>'93'),
	'AG'=>array('name'=>'ANTIGUA AND BARBUDA','code'=>'1268'),
	'AI'=>array('name'=>'ANGUILLA','code'=>'1264'),
	'AL'=>array('name'=>'ALBANIA','code'=>'355'),
	'AM'=>array('name'=>'ARMENIA','code'=>'374'),
	'AN'=>array('name'=>'NETHERLANDS ANTILLES','code'=>'599'),
	'AO'=>array('name'=>'ANGOLA','code'=>'244'),
	'AQ'=>array('name'=>'ANTARCTICA','code'=>'672'),
	'AR'=>array('name'=>'ARGENTINA','code'=>'54'),
	'AS'=>array('name'=>'AMERICAN SAMOA','code'=>'1684'),
	'AT'=>array('name'=>'AUSTRIA','code'=>'43'),
	'AU'=>array('name'=>'AUSTRALIA','code'=>'61'),
	'AW'=>array('name'=>'ARUBA','code'=>'297'),
	'AZ'=>array('name'=>'AZERBAIJAN','code'=>'994'),
	'BA'=>array('name'=>'BOSNIA AND HERZEGOVINA','code'=>'387'),
	'BB'=>array('name'=>'BARBADOS','code'=>'1246'),
	'BD'=>array('name'=>'BANGLADESH','code'=>'880'),
	'BE'=>array('name'=>'BELGIUM','code'=>'32'),
	'BF'=>array('name'=>'BURKINA FASO','code'=>'226'),
	'BG'=>array('name'=>'BULGARIA','code'=>'359'),
	'BH'=>array('name'=>'BAHRAIN','code'=>'973'),
	'BI'=>array('name'=>'BURUNDI','code'=>'257'),
	'BJ'=>array('name'=>'BENIN','code'=>'229'),
	'BL'=>array('name'=>'SAINT BARTHELEMY','code'=>'590'),
	'BM'=>array('name'=>'BERMUDA','code'=>'1441'),
	'BN'=>array('name'=>'BRUNEI DARUSSALAM','code'=>'673'),
	'BO'=>array('name'=>'BOLIVIA','code'=>'591'),
	'BR'=>array('name'=>'BRAZIL','code'=>'55'),
	'BS'=>array('name'=>'BAHAMAS','code'=>'1242'),
	'BT'=>array('name'=>'BHUTAN','code'=>'975'),
	'BW'=>array('name'=>'BOTSWANA','code'=>'267'),
	'BY'=>array('name'=>'BELARUS','code'=>'375'),
	'BZ'=>array('name'=>'BELIZE','code'=>'501'),
	'CA'=>array('name'=>'CANADA','code'=>'1'),
	'CC'=>array('name'=>'COCOS (KEELING) ISLANDS','code'=>'61'),
	'CD'=>array('name'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE','code'=>'243'),
	'CF'=>array('name'=>'CENTRAL AFRICAN REPUBLIC','code'=>'236'),
	'CG'=>array('name'=>'CONGO','code'=>'242'),
	'CH'=>array('name'=>'SWITZERLAND','code'=>'41'),
	'CI'=>array('name'=>'COTE D IVOIRE','code'=>'225'),
	'CK'=>array('name'=>'COOK ISLANDS','code'=>'682'),
	'CL'=>array('name'=>'CHILE','code'=>'56'),
	'CM'=>array('name'=>'CAMEROON','code'=>'237'),
	'CN'=>array('name'=>'CHINA','code'=>'86'),
	'CO'=>array('name'=>'COLOMBIA','code'=>'57'),
	'CR'=>array('name'=>'COSTA RICA','code'=>'506'),
	'CU'=>array('name'=>'CUBA','code'=>'53'),
	'CV'=>array('name'=>'CAPE VERDE','code'=>'238'),
	'CX'=>array('name'=>'CHRISTMAS ISLAND','code'=>'61'),
	'CY'=>array('name'=>'CYPRUS','code'=>'357'),
	'CZ'=>array('name'=>'CZECH REPUBLIC','code'=>'420'),
	'DE'=>array('name'=>'GERMANY','code'=>'49'),
	'DJ'=>array('name'=>'DJIBOUTI','code'=>'253'),
	'DK'=>array('name'=>'DENMARK','code'=>'45'),
	'DM'=>array('name'=>'DOMINICA','code'=>'1767'),
	'DO'=>array('name'=>'DOMINICAN REPUBLIC','code'=>'1809'),
	'DZ'=>array('name'=>'ALGERIA','code'=>'213'),
	'EC'=>array('name'=>'ECUADOR','code'=>'593'),
	'EE'=>array('name'=>'ESTONIA','code'=>'372'),
	'EG'=>array('name'=>'EGYPT','code'=>'20'),
	'ER'=>array('name'=>'ERITREA','code'=>'291'),
	'ES'=>array('name'=>'SPAIN','code'=>'34'),
	'ET'=>array('name'=>'ETHIOPIA','code'=>'251'),
	'FI'=>array('name'=>'FINLAND','code'=>'358'),
	'FJ'=>array('name'=>'FIJI','code'=>'679'),
	'FK'=>array('name'=>'FALKLAND ISLANDS (MALVINAS)','code'=>'500'),
	'FM'=>array('name'=>'MICRONESIA, FEDERATED STATES OF','code'=>'691'),
	'FO'=>array('name'=>'FAROE ISLANDS','code'=>'298'),
	'FR'=>array('name'=>'FRANCE','code'=>'33'),
	'GA'=>array('name'=>'GABON','code'=>'241'),
	'GB'=>array('name'=>'UNITED KINGDOM','code'=>'44'),
	'GD'=>array('name'=>'GRENADA','code'=>'1473'),
	'GE'=>array('name'=>'GEORGIA','code'=>'995'),
	'GH'=>array('name'=>'GHANA','code'=>'233'),
	'GI'=>array('name'=>'GIBRALTAR','code'=>'350'),
	'GL'=>array('name'=>'GREENLAND','code'=>'299'),
	'GM'=>array('name'=>'GAMBIA','code'=>'220'),
	'GN'=>array('name'=>'GUINEA','code'=>'224'),
	'GQ'=>array('name'=>'EQUATORIAL GUINEA','code'=>'240'),
	'GR'=>array('name'=>'GREECE','code'=>'30'),
	'GT'=>array('name'=>'GUATEMALA','code'=>'502'),
	'GU'=>array('name'=>'GUAM','code'=>'1671'),
	'GW'=>array('name'=>'GUINEA-BISSAU','code'=>'245'),
	'GY'=>array('name'=>'GUYANA','code'=>'592'),
	'HK'=>array('name'=>'HONG KONG','code'=>'852'),
	'HN'=>array('name'=>'HONDURAS','code'=>'504'),
	'HR'=>array('name'=>'CROATIA','code'=>'385'),
	'HT'=>array('name'=>'HAITI','code'=>'509'),
	'HU'=>array('name'=>'HUNGARY','code'=>'36'),
	'ID'=>array('name'=>'INDONESIA','code'=>'62'),
	'IE'=>array('name'=>'IRELAND','code'=>'353'),
	'IL'=>array('name'=>'ISRAEL','code'=>'972'),
	'IM'=>array('name'=>'ISLE OF MAN','code'=>'44'),
	'IN'=>array('name'=>'INDIA','code'=>'91'),
	'IQ'=>array('name'=>'IRAQ','code'=>'964'),
	'IR'=>array('name'=>'IRAN, ISLAMIC REPUBLIC OF','code'=>'98'),
	'IS'=>array('name'=>'ICELAND','code'=>'354'),
	'IT'=>array('name'=>'ITALY','code'=>'39'),
	'JM'=>array('name'=>'JAMAICA','code'=>'1876'),
	'JO'=>array('name'=>'JORDAN','code'=>'962'),
	'JP'=>array('name'=>'JAPAN','code'=>'81'),
	'KE'=>array('name'=>'KENYA','code'=>'254'),
	'KG'=>array('name'=>'KYRGYZSTAN','code'=>'996'),
	'KH'=>array('name'=>'CAMBODIA','code'=>'855'),
	'KI'=>array('name'=>'KIRIBATI','code'=>'686'),
	'KM'=>array('name'=>'COMOROS','code'=>'269'),
	'KN'=>array('name'=>'SAINT KITTS AND NEVIS','code'=>'1869'),
	'KP'=>array('name'=>'KOREA DEMOCRATIC PEOPLES REPUBLIC OF','code'=>'850'),
	'KR'=>array('name'=>'KOREA REPUBLIC OF','code'=>'82'),
	'KW'=>array('name'=>'KUWAIT','code'=>'965'),
	'KY'=>array('name'=>'CAYMAN ISLANDS','code'=>'1345'),
	'KZ'=>array('name'=>'KAZAKSTAN','code'=>'7'),
	'LA'=>array('name'=>'LAO PEOPLES DEMOCRATIC REPUBLIC','code'=>'856'),
	'LB'=>array('name'=>'LEBANON','code'=>'961'),
	'LC'=>array('name'=>'SAINT LUCIA','code'=>'1758'),
	'LI'=>array('name'=>'LIECHTENSTEIN','code'=>'423'),
	'LK'=>array('name'=>'SRI LANKA','code'=>'94'),
	'LR'=>array('name'=>'LIBERIA','code'=>'231'),
	'LS'=>array('name'=>'LESOTHO','code'=>'266'),
	'LT'=>array('name'=>'LITHUANIA','code'=>'370'),
	'LU'=>array('name'=>'LUXEMBOURG','code'=>'352'),
	'LV'=>array('name'=>'LATVIA','code'=>'371'),
	'LY'=>array('name'=>'LIBYAN ARAB JAMAHIRIYA','code'=>'218'),
	'MA'=>array('name'=>'MOROCCO','code'=>'212'),
	'MC'=>array('name'=>'MONACO','code'=>'377'),
	'MD'=>array('name'=>'MOLDOVA, REPUBLIC OF','code'=>'373'),
	'ME'=>array('name'=>'MONTENEGRO','code'=>'382'),
	'MF'=>array('name'=>'SAINT MARTIN','code'=>'1599'),
	'MG'=>array('name'=>'MADAGASCAR','code'=>'261'),
	'MH'=>array('name'=>'MARSHALL ISLANDS','code'=>'692'),
	'MK'=>array('name'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','code'=>'389'),
	'ML'=>array('name'=>'MALI','code'=>'223'),
	'MM'=>array('name'=>'MYANMAR','code'=>'95'),
	'MN'=>array('name'=>'MONGOLIA','code'=>'976'),
	'MO'=>array('name'=>'MACAU','code'=>'853'),
	'MP'=>array('name'=>'NORTHERN MARIANA ISLANDS','code'=>'1670'),
	'MR'=>array('name'=>'MAURITANIA','code'=>'222'),
	'MS'=>array('name'=>'MONTSERRAT','code'=>'1664'),
	'MT'=>array('name'=>'MALTA','code'=>'356'),
	'MU'=>array('name'=>'MAURITIUS','code'=>'230'),
	'MV'=>array('name'=>'MALDIVES','code'=>'960'),
	'MW'=>array('name'=>'MALAWI','code'=>'265'),
	'MX'=>array('name'=>'MEXICO','code'=>'52'),
	'MY'=>array('name'=>'MALAYSIA','code'=>'60'),
	'MZ'=>array('name'=>'MOZAMBIQUE','code'=>'258'),
	'NA'=>array('name'=>'NAMIBIA','code'=>'264'),
	'NC'=>array('name'=>'NEW CALEDONIA','code'=>'687'),
	'NE'=>array('name'=>'NIGER','code'=>'227'),
	'NG'=>array('name'=>'NIGERIA','code'=>'234'),
	'NI'=>array('name'=>'NICARAGUA','code'=>'505'),
	'NL'=>array('name'=>'NETHERLANDS','code'=>'31'),
	'NO'=>array('name'=>'NORWAY','code'=>'47'),
	'NP'=>array('name'=>'NEPAL','code'=>'977'),
	'NR'=>array('name'=>'NAURU','code'=>'674'),
	'NU'=>array('name'=>'NIUE','code'=>'683'),
	'NZ'=>array('name'=>'NEW ZEALAND','code'=>'64'),
	'OM'=>array('name'=>'OMAN','code'=>'968'),
	'PA'=>array('name'=>'PANAMA','code'=>'507'),
	'PE'=>array('name'=>'PERU','code'=>'51'),
	'PF'=>array('name'=>'FRENCH POLYNESIA','code'=>'689'),
	'PG'=>array('name'=>'PAPUA NEW GUINEA','code'=>'675'),
	'PH'=>array('name'=>'PHILIPPINES','code'=>'63'),
	'PK'=>array('name'=>'PAKISTAN','code'=>'92'),
	'PL'=>array('name'=>'POLAND','code'=>'48'),
	'PM'=>array('name'=>'SAINT PIERRE AND MIQUELON','code'=>'508'),
	'PN'=>array('name'=>'PITCAIRN','code'=>'870'),
	'PR'=>array('name'=>'PUERTO RICO','code'=>'1'),
	'PT'=>array('name'=>'PORTUGAL','code'=>'351'),
	'PW'=>array('name'=>'PALAU','code'=>'680'),
	'PY'=>array('name'=>'PARAGUAY','code'=>'595'),
	'QA'=>array('name'=>'QATAR','code'=>'974'),
	'RO'=>array('name'=>'ROMANIA','code'=>'40'),
	'RS'=>array('name'=>'SERBIA','code'=>'381'),
	'RU'=>array('name'=>'RUSSIAN FEDERATION','code'=>'7'),
	'RW'=>array('name'=>'RWANDA','code'=>'250'),
	'SA'=>array('name'=>'SAUDI ARABIA','code'=>'966'),
	'SB'=>array('name'=>'SOLOMON ISLANDS','code'=>'677'),
	'SC'=>array('name'=>'SEYCHELLES','code'=>'248'),
	'SD'=>array('name'=>'SUDAN','code'=>'249'),
	'SE'=>array('name'=>'SWEDEN','code'=>'46'),
	'SG'=>array('name'=>'SINGAPORE','code'=>'65'),
	'SH'=>array('name'=>'SAINT HELENA','code'=>'290'),
	'SI'=>array('name'=>'SLOVENIA','code'=>'386'),
	'SK'=>array('name'=>'SLOVAKIA','code'=>'421'),
	'SL'=>array('name'=>'SIERRA LEONE','code'=>'232'),
	'SM'=>array('name'=>'SAN MARINO','code'=>'378'),
	'SN'=>array('name'=>'SENEGAL','code'=>'221'),
	'SO'=>array('name'=>'SOMALIA','code'=>'252'),
	'SR'=>array('name'=>'SURINAME','code'=>'597'),
	'ST'=>array('name'=>'SAO TOME AND PRINCIPE','code'=>'239'),
	'SV'=>array('name'=>'EL SALVADOR','code'=>'503'),
	'SY'=>array('name'=>'SYRIAN ARAB REPUBLIC','code'=>'963'),
	'SZ'=>array('name'=>'SWAZILAND','code'=>'268'),
	'TC'=>array('name'=>'TURKS AND CAICOS ISLANDS','code'=>'1649'),
	'TD'=>array('name'=>'CHAD','code'=>'235'),
	'TG'=>array('name'=>'TOGO','code'=>'228'),
	'TH'=>array('name'=>'THAILAND','code'=>'66'),
	'TJ'=>array('name'=>'TAJIKISTAN','code'=>'992'),
	'TK'=>array('name'=>'TOKELAU','code'=>'690'),
	'TL'=>array('name'=>'TIMOR-LESTE','code'=>'670'),
	'TM'=>array('name'=>'TURKMENISTAN','code'=>'993'),
	'TN'=>array('name'=>'TUNISIA','code'=>'216'),
	'TO'=>array('name'=>'TONGA','code'=>'676'),
	'TR'=>array('name'=>'TURKEY','code'=>'90'),
	'TT'=>array('name'=>'TRINIDAD AND TOBAGO','code'=>'1868'),
	'TV'=>array('name'=>'TUVALU','code'=>'688'),
	'TW'=>array('name'=>'TAIWAN, PROVINCE OF CHINA','code'=>'886'),
	'TZ'=>array('name'=>'TANZANIA, UNITED REPUBLIC OF','code'=>'255'),
	'UA'=>array('name'=>'UKRAINE','code'=>'380'),
	'UG'=>array('name'=>'UGANDA','code'=>'256'),
	'US'=>array('name'=>'UNITED STATES','code'=>'1'),
	'UY'=>array('name'=>'URUGUAY','code'=>'598'),
	'UZ'=>array('name'=>'UZBEKISTAN','code'=>'998'),
	'VA'=>array('name'=>'HOLY SEE (VATICAN CITY STATE)','code'=>'39'),
	'VC'=>array('name'=>'SAINT VINCENT AND THE GRENADINES','code'=>'1784'),
	'VE'=>array('name'=>'VENEZUELA','code'=>'58'),
	'VG'=>array('name'=>'VIRGIN ISLANDS, BRITISH','code'=>'1284'),
	'VI'=>array('name'=>'VIRGIN ISLANDS, U.S.','code'=>'1340'),
	'VN'=>array('name'=>'VIET NAM','code'=>'84'),
	'VU'=>array('name'=>'VANUATU','code'=>'678'),
	'WF'=>array('name'=>'WALLIS AND FUTUNA','code'=>'681'),
	'WS'=>array('name'=>'SAMOA','code'=>'685'),
	'XK'=>array('name'=>'KOSOVO','code'=>'381'),
	'YE'=>array('name'=>'YEMEN','code'=>'967'),
	'YT'=>array('name'=>'MAYOTTE','code'=>'262'),
	'ZA'=>array('name'=>'SOUTH AFRICA','code'=>'27'),
	'ZM'=>array('name'=>'ZAMBIA','code'=>'260'),
	'ZW'=>array('name'=>'ZIMBABWE','code'=>'263')
);
@endphp


<!-- <div id="loader" ></div> -->
        <!-- Bnner Section -->
        <section class="banner-section">
            <div class="banner-carousel owl-carousel owl-theme animate-bottom">
                <div class="slide-item">

                    <a href="javascript:void(0)" class="w-100">
                        <img class="img-responsive w-100 h-100"
                            src="{{asset('images/great-place-to-work-banner.jpeg')}}" alt="">
                    </a>
                </div>
                <div class="slide-item">

                    <a href="javascript:void(0)" class="w-100">
                        <img class="img-responsive w-100 h-100" src="{{asset('images/NU-Fertility-BANNER_1-1.jpg')}}"
                            alt="">
                    </a>
                </div>
                <div class="slide-item">

                    <a href="javascript:void(0)" class="w-100">
                        <img class="img-responsive w-100 h-100"
                            src="{{asset('images/NU-Fertility-BANNER_2-1-new.jpg')}}" alt="">
                    </a>
                </div>

                <div class="slide-item">
                    <a href="#" class="w-100">
                        <img class="img-responsive w-100 h-100"
                            src="{{asset('images/Web Banner for Erectile Dysfunction.jpg')}}" alt="">
                    </a>
                </div>
            </div>
        </section>
        <!-- End Bnner Section -->


        <style>
            li.country_sel{color:#000;}
        .book-form-section .shadow-lg {
            -webkit-box-shadow: 0 1rem 3rem rgb(0 0 0 / 18%) !important;
            box-shadow: 0 1rem 3rem rgb(0 0 0 / 18%) !important;
        }

        .book-form-section .bg-light {
            background-color: #F2FAFF !important;
        }

        .book-form-section .homepage-form {
            padding: 30px;
            background-color: white;
            z-index: 1000;
            margin-top: 30px;
        }


        .book-form-section .err-msg {
            display: none;
        }

        .book-form-section .select_container select {
            background: #11aff0 !important;
        }

        .book-form-section .book-form-control {
            background-color: #11aff0 !important;
            border: 1px solid #11aff0 !important;
            color:#fff !important;font-weight:700;font-size:16px;
            line-height:22px;font-family:sen;
        }
        
@media only screen and (min-width: 800px) and (max-width:1200px){
        .book-form-section .book-form-control {font-size:12px;}
    }
        #TxtRemarks{
        color: #000000;min-height:6.5rem;font-family:sen;font-weight:400;
    border-color: #e5e5e5;}
    #TxtRemarks:focus{color:#000;border-color:#e5e5e5;background:#e5e5e5;}
    .form-control {
    display: block !important;
    width: 100% !important;
    padding: 0.375rem 0.75rem !important;
    line-height: 1.6 !important;
    background-clip: padding-box !important;
    border-radius: 0.25rem !important;
}
        ::placeholder {color:#fff !important;}
        @media only screen and (min-width: 768px){
            .phone_flex{
justify-content:normal;
            }
.Offerings-section.section-padding .col-xs-6.col-lg-2.col-md-2{-ms-flex: 0 0 207%;
    flex: 0 0 20%;
    max-width: 20%;}
        }

        </style>

        <section class="book-form-section section-padding">
            <div class="container">
                <div class="row">

                    <div class="col-md-1"></div>
                    <div class="col-md-10 m-auto book-form">
                        <form name='BookAppointmentFrm' id='BookAppointmentFrm'
                            class="homepage-form shadow-lg bg-light">
                            <div class="row">
                                <div class="col-md-12 pb-3">
                                    <h2>Request an Appointment</h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <select name="doctor" class="form-control department-holder book-form-control"
                                            id="TxtDoctor">
                                            <option value="0">Doctors</option>
                                            <option value="Dr Ashwini S">Dr Ashwini S</option>
                                            <option value="Dr Sneha J">Dr Sneha J</option>
                                            <option value="Dr Pramod Krishnappa">Dr Pramod Krishnappa</option>
                                            <option value="Dr Kavya V Pradeep">Dr Kavya V Pradeep</option>
                                            <option value="Dr. Prakrutha Sreenath">Dr. Prakrutha Sreenath</option>
                                        </select>
                                        <div class="err-msg" id='err-doctor'>Please Enter Doctor</div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="name" id="TxtName"
                                            class="form-control book-form-control" placeholder="Full Name">
                                        <div class="err-msg" id='err-name'>Please Enter Name</div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-12 phone_flex">
                                    
                                    <div class="form-group">
                                        <div class="form-control book-form-control">
                                    <input type="hidden" name="country_code_val"   class="book-form-control" id="country_code_val" value="91">
                                            <img src="{{url('images/flags/in.png')}}" alt="" id="ctry_sel_id" class="up">
                                            <i class="fa fa-caret-down changes_arrow" aria-hidden="true"></i>
                                    <ul name="country_code" id="country_code" style="display:none">
                                            @foreach($countryArray as $key => $ca)
                                            <li class="country_sel"  data-value="{{$ca['code']}}" @if($key == 'IN') selected @endif >
                                            <img   src="{{asset('images/flags/'.strtolower($key).'.png')}} " alt="">
                                               <div>{{$ca['name']}}</div> 
                                            </li>
                                            @endforeach
                                            </ul>
                                   
                                        <input type="text" name='phone' id='TxtPhone'
                                            class="book-form-control" placeholder="Phone Number">
                                        <div class="err-msg" id='err-phone'>Enter phone No. Correctly</div>
                                    </div>
                                </div>
    </div>


                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="email" id="TxtEmail"
                                            class="form-control book-form-control" placeholder="Email">
                                        <div class="err-msg" id='err-email'>Please Enter Email</div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-12">
                                    <div class="form-group">
                                        <select class="form-control book-form-control" name='appointment_type'
                                            id="DdlPatientsType">
                                            <option value=''>Appointment Type</option>
                                            <option value='New Appointment'>New Appointment</option>
                                            <option value='Existing Patient'>Existing Patient</option>
                                        </select>
                                        <div class="err-msg" id='err-appointment_type'>Please Enter Remarks</div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group" style="position:relative;">
                                        <input type="date" class="form-control book-form-control"
                                            name='appointment_date' id='datetimepicker1' placeholder="Appointment Date">
                                        <div class="err-msg" id='err-datetimepicker'>Please Choose Date Correctly
                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-6 col-md-12">
                                    <div class="form-group">
                                        <select class="form-control book-form-control" name='time_slot' id="time_slot">
                                            <option value="">Appointment Slot</option>
                                            <option value='11:00 to 1:30'>11:00 to 1:30</option>
                                            <option value='2:00 to 4:30'>2:00 to 4:30</option>
                                        </select>
                                        <div class="err-msg" id='err-timeslot'>Please Enter Remarks</div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                                    <div class="form-group">
                                        <textarea name="remarks" id="TxtRemarks" class="form-control"
                                            placeholder="Remarks" rows="3">Remarks</textarea>
                                        <div class="err-msg" id='err-remarks'>Please Enter Remarks</div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                                    <div class="form-group prn-field">
                                        <input class="form-control book-form-control" type="Text" name="prn_field"
                                            id='TxtPrnNumber' placeholder="PRN Number">
                                        <div class="err-msg" id="errPrn">PRN Number is Incorrect</div>
                                    </div>
                                    <input type="hidden" name="bot_check">


                                    <div class="form-group">
                                        <a href='javascript:void(0);'
                                            style="color: #ffffff !important; background-color: #f6a2de; border: solid 1px #f6a2de;"
                                            id='BtnSubmit' class="btn btn-danger btn-rounded btn-block">Confirm</a>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </section>

        <style>
        .welcome-section {
            background-image: url(http://felizia.ancorathemes.com/wp-content/uploads/2016/11/background-8.jpg?id=271) !important;
            background-position: -250px 0px;
        }

        @media only screen and (max-width: 767px) {
            .welcome-section {
                background-position: 150px 0px;
            background-image:none !important;background-color:#f6f6f6;
        }

        .welcome-section .sc_button_text {

            background-color: #11aff0;
            padding: 15px 50px;
            border-radius: 100px;
            color: #fff;
        }

        .welcome-section .sc_item_button {
            margin-top: 60px;
        }

        .sc_title {
            padding: 20px 20px;
        }


        .sc_button_text:hover {
            /*    transition: background 4s;
background: linear-gradient(to right, #f6a2de 50%, #11aff0 50%) no-repeat scroll right bottom / 210% 100% #11aff0 !important;*/
            background-color: #f6a2de;

        }
        </style>

        <section class="welcome-section section-padding">

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-12"></div>
                    <div class="col-lg-6 col-md-12">
                        <div>
                            <h2>
                                Welcome to NU Fertility Center</h2>
                        </div><!-- sc_title -->


                        <div class="pt-3">
                            <div class="">
                                <p>NU Fertility at NU Hospitals, Rajajinagar, Bangalore, India, was started keeping
                                    in mind the emerging needs of infertility and ART services. The Centre is
                                    equipped with comprehensive assessment for both male and female infertility
                                    related problems. The state-of-the-art embryology laboratory is built according
                                    to the ESHRE guidelines and the quality is constantly monitored, ensuring the
                                    appropriate culture and conditions are maintained.</p>
                            </div>
                        </div>


                        <div class="sc_item_button">
                            <a href="#" class="sc_button">
                                <span class="sc_button_text">
                                    <span class="sc_button_title">More About Us</span>
                                </span>
                            </a>
                            <!-- sc_button -->
                        </div>
                    </div>
                </div>
        </section>


        <style>
        .circle {
            border: 6px solid #11aff0;
            padding: 10px 0px;
            border-radius: 100%;
            height: 130px;
            width: 130px;

        }

        .circle:hover {
            border: 6px solid #f6a2de;


        }


        .nuf_log {
            width: 120px;
            height: 95px;
        }

        .sc_services_item_title {
            text-align: center;
    color: #000;
    padding-top: 25px;
    padding-bottom: 20px;
    font-size: 20px;
    text-transform: uppercase;
    min-height: 3.4em;
    font-weight: 600;
    margin: 0;
    font-size: 18px;
        }
        
        @media only screen and (min-width: 768px){
            
        .sc_services_item_title {font-size:15px;}
        }
        </style>
        <section class="Offerings-section section-padding">
            <div class="container-fluid">
                <div class="sc_title">
                    <h2>Our Offerings</h2>
                </div><!-- /.sc_title -->
                <center>
                    <div class="row">

                        <div class="col-xs-6 col-lg-2 col-md-2">
                            <div>
                                <div class="circle-out">
                                    <a href="#">
                                        <div class="circle ">
                                            <img class="nuf_log img-fluid"
                                                src="{{asset('images/Female-Fertility-ovulation-icon.png')}}" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div>
                                    <a href="#">
                                        <h4 class="sc_services_item_title">
                                            FEMALE INFERTILITY
                                        </h4>
                                    </a>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-6 col-lg-2 col-md-2">
                            <div>
                                <div class="circle-out">
                                    <div class="circle">
                                        <a href="#"> <img class="nuf_log img-fluid"
                                                src="{{asset('images/IVF-icon.png')}}" alt=""> </a>
                                    </div>
                                </div>
                                <div>
                                    <a href="#">
                                        <h4 class="sc_services_item_title">
                                            IVF
                                        </h4>
                                    </a>
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-6 col-lg-2 col-md-2">
                            <div class="circle-out">
                                <div class="circle">
                                    <a href="#"> <img class="nuf_log" src="{{asset('images/Andrology-icon.png')}}"
                                            alt="">
                                    </a>
                                </div>
                            </div>
                            <div>
                                <a href="#">
                                    <h4 class="sc_services_item_title">


                                        ANDROLOGY <br>(MEN’S SEXUAL HEALTH)
                                    </h4>
                                </a>
                            </div>


                        </div>
                        <div class="col-xs-6 col-lg-2 col-md-2">
                            <div class="circle-out">
                                <div class="circle">
                                    <a href="#"> <img class="nuf_log" src="{{asset('images/iui-icon.png')}}" alt="">
                                    </a>
                                </div>
                            </div>
                            <div>
                                <a href="#">
                                    <h4 class="sc_services_item_title">
                                        IUI
                                    </h4>
                                </a>
                            </div>


                        </div>
                        <div class="col-xs-6 col-lg-2 col-md-2">
                            <div class="circle-out">
                                <div class="circle">
                                    <a href="#"> <img class="nuf_log" src="{{asset('images/laparoscopic-icon.png')}}"
                                            alt=""> </a>
                                </div>
                            </div>
                            <div>
                                <a href="#">
                                    <h4 class="sc_services_item_title" style="padding-bottom: 0px;">
                                        LAPAROSCOPIC <br> SURGERY

                                    </h4>
                                </a>
                            </div>


                        </div>



                    </div>
                </center>
               


            </div>
        </section>

        <section class="about-centers-section">
            <div class="container">
                <div class="sc_title">
                    <h2>About Our Centers</h2>
                </div><!-- /.sc_title -->

                <div class="row">
                    <div class="col-lg-12">


                        <div class="centers-carousel owl-carousel owl-theme">
                            <div class="slide-item">

                                <center>
                                    <img class="img-responsive w-100 h-100" src="{{asset('images/Image 1.jpg')}}"
                                        alt="">
                                    <span class="text-center slide-item-title">Experienced and Qualified team of
                                        Specialists</span>
                                </center>
                            </div>
                            <center>
                                <div class="slide-item">


                                    <img class="img-responsive w-100 h-100" src="{{asset('images/Image 2.jpg')}}"
                                        alt="">
                                    <span class="text-center slide-item-title">Advanced technology for IVF
                                        treatment</span>

                                </div>
                            </center>
                            <center>
                                <div class="slide-item">


                                    <img class="img-responsive w-100 h-100" src="{{asset('images/Image 3.jpg')}}"
                                        alt="">
                                    <span class="text-center slide-item-title">High percentage of Positive
                                        Results</span>

                                </div>
                            </center>
                            <center>
                                <div class="slide-item">

                                    <img class="img-responsive w-100 h-100" src="{{asset('images/ivf.jpg')}}" alt="">
                                    <span class="text-center slide-item-title">Over 60%+ successful IVF pregnancy
                                    </span>

                                </div>
                            </center>
                            <center>

                                <div class="slide-item">

                                    <img class="img-responsive w-100 h-100" src="{{asset('images/FET.jpg')}}" alt="">
                                    <span class="text-center slide-item-title">Over 64%+ successful FET (Frozen Embryo
                                        Transfer) pregnancy</span>
                                </div>
                            </center>
                            <center>
                                <div class="slide-item">

                                    <img class="img-responsive w-100 h-100" src="{{asset('images/IUI.jpg')}}" alt="">
                                    <span class="text-center slide-item-title">Over 20%+ successful IUI pregnancy</span>
                                </div>
                            </center>
                            <center>
                                <div class="slide-item">

                                    <img class="img-responsive w-100 h-100"
                                        src="{{asset('images/IVF with dono eggs.jpg')}}" alt="">
                                    <span class="text-center slide-item-title">Over 90%+ successful IVF with donor
                                        eggs</span>
                                </div>
                            </center>
                            <center>
                                <div class="slide-item">

                                    <img class="img-responsive w-100 h-100" src="{{asset('images/1250 families.jpg')}}"
                                        alt="">
                                    <span class="text-center slide-item-title">Over 1250 happy families </span>
                                </div>

                            </center>



                        </div>
                    </div>
                </div>
            </div>

        </section>

        <style>
        .meet-doctors-section {
            background-image: url(http://felizia.ancorathemes.com/wp-content/uploads/2016/11/background-9.jpg?id=272) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: cover !important;
        }

        .card-padding {
            padding-top: 10px;
        }
        .card{background:#f5f5f5;}
        .card-body{
            margin-bottom: 10%;
    height: auto;
    background-color: #f5f5f5;
    min-height: 250px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    font-size: 0.8em;
    padding: 3em 1em 3.3em;
        }
        .card-body h5{font-size:18px;}
        .card-body p{
            font-size: 1em;
    font-weight: 400;
    letter-spacing: 0;
    margin-top: 1.25em;
    text-transform: uppercase;
    line-height: 1.45em;
        }
        
@media only screen and (min-width: 768px){
    .card-body h5{font-size:15px;}
    .card-body p{font-size:0.8em;}
}
        .card-padding:hover .card-body{background:#f5f5f5;}
.card-padding .card:hover .card-body{
    margin-top: -3.6em;
    padding-bottom: 6.9em;margin-bottom:5%;
}

.card-padding .card .card-link .card-img-top{height:286px !important;}
.card-padding .card .card-link .mask{background-color:rgba(65, 65, 65, 0.7);
            position: absolute; 
            top: 0;height:241px;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            opacity: 0;
            -webkit-transition: all 400ms ease-in-out;
            -moz-transition: all 400ms ease-in-out;
            -o-transition: all 400ms ease-in-out;
            transition: all 400ms ease-in-out;
        }

        
.card-padding .card .card-link:hover .mask{    opacity: 1;
        ;}
        

        .sc_title_color {
            color: white;margin-bottom: 28px;
    font-size: 25px !important;
        }
        .sc_button_simple {
    text-transform: uppercase;
    font-size: 0.8667em;
    letter-spacing: 3px;
    color:#000;
}

.sc_team_item_button a:hover {
   text-decoration:underline;
    color:#11aff0;
}
.meet-doctors-section .fa {
    position:relative;
    left:10px;
    bottom:3px;
    padding: 1px 0px 1px 1px;
    background:black;
    color:#fff;  
}



.sc_team_item_button:hover + .meet-doctors-section .fa{
  background:green;
}
        </style>

        <section class="meet-doctors-section section-padding">
            <div class="container-fluid">
                <div class="sc_title">
                    <h2 class="sc_title_color">Meet Our Doctors</h2>
                </div><!-- /.sc_title -->

                <center>
                    <div class="row">
                        <div class="col-lg-9 col-md-9">

                            <h4 class="sc_title_color"> Reproductive Medicine(RM) </h4>

                            <div class="row">
                                <div class="col-lg-4 col-md-4 card-padding">

                                    <div class="card text-center" >
<a href="" class="card-link">
                                        <img class="img-responsive card-img-top"
                                            src="{{asset('images/Dr.-Aswino-new.jpg')}}" alt="Card image cap">
                                            <div class="mask"></div>
                                        </a>

                                        <div class="card-body">
                                            <h5 class="card-title"> <a href="#" style="color: black;">Dr. Ashwini S</a>
                                            </h5>
                                            <p class="card-text">MBBS, DGO, DNB, FIRM (REPRODUCTIVE
                                                MEDICINE, RGUHS)
                                                <br>
                                                <b>Location: Rajajinagar,
                                                    Bengaluru</b>
                                            </p>
                                            <div class="sc_team_item_button">
                                                <a href="#"
                                                    class="sc_button sc_button_simple">Learn more<i class="fa fa-arrow-right"
                                                    aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 card-padding">
                                    <div class="card">
                                    <a href="" class="card-link">
                                        <img class="img-responsive w-100 h-100 card-img-top"
                                            src="{{asset('images/Dr-Sneha.jpg')}}" alt="Card image cap">
                                            <div class="mask"></div>
                                        </a>

                                        <div class="card-body">
                                            <h5 class="card-title">Dr. Sneha J</h5>
                                            <p class="card-text">MBBS, MD (AIIMS), DNB (OBG), FIRM (REPRODUCTIVE
                                                MEDICINE, RGUHS)
                                                <br>
                                                <b>Location: Rajajinagar &amp; Padmanabhanagar, Bengaluru</b>
                                            </p>
                                            <div class="sc_team_item_button">
                                                <a href="#"
                                                    class="sc_button sc_button_simple">Learn more<i class="fa fa-arrow-right"
                                                    aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4 card-padding">
                                    <div class="card" >
                                    <a href="" class="card-link">
                                        <img class="img-responsive w-100 h-100 card-img-top"
                                            src="{{asset('images/Dr-Kavya-2.jpg')}}" alt="Card image cap">
                                            <div class="mask"></div>
                                        </a>
                                        <div class="card-body">


                                            <h5 class="card-title">Dr Kavya V Pradeep</h5>
                                            <p class="card-text">MBBS, DGO, DNB(OBG) &amp; FIRM
                                                <br><b>Location: Shivamogga
                                                    Clinic &amp; Hospitals, Shivamogga</b>
                                            </p>

                                            <div class="sc_team_item_button">
                                                <a href="#"
                                                    class="sc_button sc_button_simple">Learn more<i class="fa fa-arrow-right"
                                                    aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-lg-4 col-md-4 card-padding">
                                    <div class="card">
                                    <a href="" class="card-link">
                                        <img class="img-responsive w-100 h-100 card-img-top"
                                            src="{{asset('images/DR. PRAKRUTHA SREENATH3.jpg')}}" alt="Card image cap">
                                            <div class="mask"></div>
                                        </a>
                                        <div class="card-body">
                                            <h5 class="card-title">Dr. Prakrutha Sreenath</h5>
                                            <p class="card-text">MBBS, MS (OBG), FIRM (REPRODUCTIVE MEDICINE
                                                SPECIALIST)
                                                <br>
                                                <b>Location: Padmanabhnagar, Bengaluru</b>
                                            </p>

                                            <div class="sc_team_item_button">
                                                <a href="#"
                                                    class="sc_button sc_button_simple">Learn more<i class="fa fa-arrow-right"
                                                    aria-hidden="true"></i>
                                                </a>
                                            </div>
                                       <!--     <a href="#" class="">LEARN MORE <i class="fa fa-arrow-right"
                                                    aria-hidden="true"></i> </a> -->

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3">
                            <h4 class="sc_title_color"> Andrology </h4>

                            <div class="row">
                                <div class="col-lg-12 col-md-12 card-padding">
                            
                                    <div class="card text-center">    <a href="" class="card-link">
                                        <img class="img-responsive w-100 h-100 card-img-top"
                                            src="{{asset('images/pramod.png')}}" alt="Card image cap">
                                            <div class="mask"></div>
                                        </a>
                                        <div class="card-body">
                                            <h5 class="card-title">Dr. Pramod Krishnappa</h5>
                                            <p class="card-text">MBBS, MS (SURGERY), DNB (UROLOGY)
                                                <br>
                                                <b>Location:
                                                    Rajajinagar &amp; Padmanabhanagar, Bengaluru</b>
                                            </p>
                                            <div class="sc_team_item_button">
                                                <a href="#"
                                                    class="sc_button sc_button_simple">Learn more<i class="fa fa-arrow-right"
                                                    aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>


                                    </div>

                                </div>






                            </div>
                        </div>
                    </div>

                    <center>
            </div>
        </section>


        <style>
        .sc-bg {
            background-color: #f5f5f5;
        }

        .custom_overlay_wrapper {
            position: relative;
        }
       .owl-item .custom_overlay_wrapper:before{position: absolute;
    content: '';
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    background-color: rgba(65, 65, 65, 0.5);
    opacity: 0.4;
    -webkit-transition: all 0.3s ease;
    -moz-transition: all 0.3s ease;
    -ms-transition: all 0.3s ease;
    -o-transition: all 0.3s ease;
    transition: all 0.3s ease;}
    
    .owl-item:hover .custom_overlay_wrapper:before {
            opacity: 0;
        }
        .custom_overlay {
            position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    -ms-box-sizing: border-box;
    box-sizing: border-box;
    text-align: left;

            opacity: 1;padding:2.1429em;
            -webkit-transition: all 400ms ease-in-out;
            -moz-transition: all 400ms ease-in-out;
            -o-transition: all 400ms ease-in-out;
            transition: all 400ms ease-in-out;
        }
        .:hover .post_featured:before {
            opacity: 0;
        }

        .custom_overlay_inner {
            position: absolute;
            top: 50%;
            left: 10px;
            right: 10px;
            transform: translateY(-50%);
        }

        .custom_overlay h4,.custom_overlay a {
            position: relative;
            margin-bottom: 4px;color:#fff !important;font-size:18px;line-height:20px;font-weight:600;font-family:sen;|
        }

        .custom_overlay p {font-size:12px;font-weight:600;
            color: #fff;font-family:Poppins, sans-serif;
            line-height: 1.4em;
        }
        .custom_overlay p a{
            color:#000 !important;top:5px;
            padding:0px 0px 0px 12px;position:relative;
        }
        .custom_overlay p a.more-info{
            color:#000 !important;
            padding:0px 0px 0px 28px;
        }
        a.more_info:before{font-size: 14px;
        font-family: sans-serif;
        content: '+';
        font-style: normal;
        top: -1px;
        opacity: 1;
        right: auto;
        left: -17px;
        width: 16px;
        height: 16px;
        background:#000;color:#fff !important;position:absolute;
        line-height: 16px;
        -webkit-border-radius: 50%;
        -moz-border-radius: 50%;
        border-radius: 50%;
        text-align: center;
        }
    a.more_info:hover{
        #11aff0 !important;}
        
        .custom_overlay h4:after {

            content: "";
            position: absolute;
            left: 35%;
            right: 35%;
            bottom: 0;
            height: 4px;
        }
        .custom_overlay p a.more-info{
            color:#000 !important;
            padding:0px 0px 0px 28px;
        }


        @media screen and (min-width: 580px) {
            .owl-theme .owl-dot{margin:5px 17px;}
}

.owl-theme .owl-dots .owl-dot{margin:0px 17px;}

@media screen and (min-width: 768px) {
.owl-theme .owl-dots .owl-dot{margin:0px 12px;}
}

@media screen and (max-width: 768px) {
.owl-theme .owl-dots .owl-dot{margin:0px 5px;}
}
        .owl-theme .owl-dots .owl-dot span{width:8.1px;height:8.1px;border-radius:50%;background:rgba(65, 65, 65, 0.5);opacity:0.5;}
        .owl-theme .owl-dots .owl-dot.active span,.owl-theme .owl-dots .owl-dot:hover span{margin:4px;background:#000;opacity:1}
        .owl-theme .owl-dots .owl-dot.active,.owl-theme .owl-dots .owl-dot:hover{border:1px solid #000;position:relative;top:-2px;border-radius:50%;}
        /*------------------- TABLET ------------------*/
        @media only screen and (min-width: 600px) and (max-width: 999px) {
            .custom_overlay h4 {
                font-size: 80%;
            }

            .custom_overlay p {
                font-size: 85%;
                line-height: 1.2em;
            }
        }
        /*------------------- MOBILE ------------------*/
        @media only screen and (max-width: 599px) {
            .custom_overlay h4 {
                font-size: 100%;
            }

            .custom_overlay p {
                font-size: 100%;
            }
        }
        </style>

        <section class="blog-section sc-bg section-padding">
            <div class="container">
                <div class="sc_title">
                    <h2>Our Blog Feed</h2>
                </div><!-- /.sc_title -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="blog-carousel owl-carousel owl-theme">

                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/3-1140x600.jpg')}}">
                                        <div class="custom_overlay">
                                            
                                                <p>Feb 6,2018</p>

                                                <h4><a href="{{url('/blog/ovulation-induction/')}}">
                                                    OVULATION INDUCTION</a>
                                                </h4>
                                                <p>
                                                    <a href="{{url('/blog/ovulation-induction/')}}" class="more_info">More Info</a></p>

                                            </div>
                                    </a></div>
                            </div>



                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/B4-1140x589.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>Feb 6, 2018</p>
                                                <h4><a href="{{url('/blog/myths-about-infertility/')}}">Myths about infertility</a></h4>
                                                <p><a href="{{url('/blog/myths-about-infertility/')}}" class="more_info">More Info</a></p>
                                            </div>
                                    </a></div>
                            </div>


                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/Egg-Freezing.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>June 24, 2019</p>
                                                <h4><a href="{{url('/blog/egg-freezing-you-can-have-control-over-your-ticking-biological-clock/')}}">
                                                    Egg freezing: You can have control over your ticking biological
                                                    clock!!</a>
                                                </h4>
                                                <p><a href="{{url('/blog/egg-freezing-you-can-have-control-over-your-ticking-biological-clock/')}}" class="more_info">More Info</a></p>
                                            </div>
                                    </a></div>
                            </div>



                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/get-rid-of-infertility.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>June 21, 2019</p>
                                                <h4><a href="{{url('/blog/the-easiest-way-to-get-rid-of-infertility/')}}">
                                                    The easiest way to get rid of infertility</a></h4>
                                                <p><a href="{{url('/blog/the-easiest-way-to-get-rid-of-infertility/')}}" class="more_info">More Info</a></p>
                                        </div>
                                    </a></div>
                            </div>



                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/Blog-Creative-02-1140x589.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>
                                                    jan 28, 2019
                                                </p>
                                                <h4><a href="{{url('/blog/5-things-you-need-to-know-about-pelvic-floor-disorder/')}}">
                                                    5 things you need to know about pelvic floor disorder</a>
                                                </h4>
                                                <p><a href="{{url('/blog/5-things-you-need-to-know-about-pelvic-floor-disorder/')}}" class="more_info">More Info</a></p>
                                            </div>
                                    </a></div>
                            </div>



                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/Blog-Creative-01-1140x589.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>
                                                    jan 14, 2019
                                                </p>
                                                <h4><a href="{{url('/blog/dealing-with-pcos-related-infertility/')}}">
                                                    Dealing With PCOS Related Infertility</a>
                                                </h4>
                                                <p><a href="{{url('/blog/dealing-with-pcos-related-infertility/')}}" class="more_info">More Info</a></p>
                                            </span></div>
                                    </a></div>
                            </div>


                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/SW6-1140x589.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>
                                                    Nov 21, 2018
                                                </p>
                                                <h4><a href="{{url('/blog/penile-implant-in-the-era-of-viagra/')}}">
                                                    Penile implant in the era of Viagra</a>
                                                </h4>
                                                <p><a href="{{url('/blog/penile-implant-in-the-era-of-viagra/')}}" class="more_info">More Info</a></p>
                                            </div>
                                    </a></div>
                            </div>

                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/SW4-1140x589.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>
                                                    Nov 21, 2018
                                                </p>
                                                <h4> <a href="{{url('/blog/low-testosterone-hypogonadism/')}}">
                                                    Low Testosterone (Hypogonadism)</a>
                                                </h4>
                                                <p><a href="{{url('/blog/low-testosterone-hypogonadism/')}}" class="more_info">More Info</a></p>
                                            </div>
                                    </a></div>
                            </div>

                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/SW1-1140x589.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>
                                                    Nov 21, 2018
                                                </p>
                                                <h4><a href="{{url('/blog/erectile-dysfunction-ed/')}}">
                                                    ERECTILE DYSFUNCTION (ED)</a>
                                                </h4>
                                                <p><a href="{{url('/blog/erectile-dysfunction-ed/')}}" class="more_info">More Info</a></p>
                                            </div>
                                    </a></div>
                            </div>

                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/Things-to-know-before-opting-for-IVF-1-1140x589.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>
                                                    Nov 15, 2018
                                                </p>
                                                <h4><a href="{{url('/blog/things-to-know-before-opting-for-ivf/')}}">
                                                    Things To Know Before Opting For IVF</a>
                                                </h4>
                                                <p><a href="{{url('/blog/things-to-know-before-opting-for-ivf/')}}" class="more_info">More Info</a></p>
                                            </div>
                                    </a></div>
                            </div>

                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/Untitled-1-1140x589.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>
                                                    Jun 12, 2018
                                                </p>
                                                <h4><a href="{{url('/blog/tips-to-boost-male-fertility-sperm-count/')}}">
                                                    Tips to Boost Male Fertility &amp; Sperm Count</a>
                                                </h4>
                                                <p><a href="{{url('/blog/tips-to-boost-male-fertility-sperm-count/')}}" class="more_info">More Info</a></p>
                                            </div>
                                    </a></div>
                            </div>

                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/Blog-Gallery-1140x589.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>
                                                    Jun 12, 2018
                                                </p>
                                                <h4><a href="{{url('/blog/effects-of-alcohol-on-fertility/')}}">
                                                    Effects of Alcohol on Female Fertility</a>
                                                </h4>
                                                <p><a href="{{url('/blog/effects-of-alcohol-on-fertility/')}}" class="more_info">More Info</a></p>
    </div>
                                    </a></div>
                            </div>

                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/B8-1140x600.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>
                                                    Mar 22, 2018
                                                </p>
                                                <h4><a hre="{{url('/blog/seeing-an-infertility-doctor-does-not-mean-you-have-to-do-an-ivf/')}}">
                                                    Seeing An Infertility Doctor Does Not Mean You Have To Do An IVF</a>
                                                </h4>
                                                <p><a href="{{url('/blog/seeing-an-infertility-doctor-does-not-mean-you-have-to-do-an-ivf/')}}" class="more_info">More Info</a></p>
                                            </div>
                                    </a></div>
                            </div>

                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/B7-1140x600.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>
                                                    Mar 21, 2018
                                                </p>
                                                <h4><a href="{{url('/blog/smoking-and-fertility/')}}">
                                                    Smoking and Fertility</a>
                                                </h4>
                                                <p><a href="{{url('/blog/smoking-and-fertility/')}}" class="more_info">More Info</a></p>
                                            </div>
                                    </a></div>
                            </div>

                            <div class="slide-item">
                                <div class="custom_overlay_wrapper"><a href="#"><img
                                            src="{{asset('images/blog/B3-1140x589.jpg')}}">
                                        <div class="custom_overlay">
                                                <p>
                                                    Feb 6, 2018
                                                </p>
                                                <h4><a href="{{url('/blog/things-you-should-know-when-battling-with-infertility/')}}">
                                                    Things you should know when Battling with Infertility</a>
                                                </h4>
                                                <p><a href="{{url('/blog/things-you-should-know-when-battling-with-infertility/')}}" class="more_info">More Info</a></p>
                                            </div>
                                    </a></div>
                            </div>










                        </div>
                    </div>
                </div>
            </div>

        </section>




    </div>
    @endsection

    @section('customjs')


    <script>
    setTimeout(showPage, 1);

    function showPage() {
        document.getElementById("loader").style.display = "none";

    }
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script>
    jQuery(".banner-carousel").owlCarousel({
        autoplay: true,
        autoplayTimeout: 12000,
        autoplayHoverPause: true,
        lazyLoad: true,
        loop: true,
        margin: 20,
        responsiveClass: true,
        nav: true,
        dots: false,
        margin: 0,
        responsive: {
            0: {
                items: 1
            },

            600: {
                items: 1
            },

            1024: {
                items: 1
            },

            1366: {
                items: 1
            }
        }
    });


    jQuery(".centers-carousel").owlCarousel({
        autoplay: true,
        autoplayTimeout: 3200,
        autoplayHoverPause: true,
        lazyLoad: true,
        loop: true,
        margin: 10,
        responsiveClass: true,

        dots: true,

        responsive: {
            0: {
                items: 1
            },

            600: {
                items: 1
            },

            1024: {
                items: 3
            },

            1366: {
                items: 3
            }
        }
    });

    jQuery(".blog-carousel").owlCarousel({
        autoplay: true,
        autoplayTimeout: 3200,
        autoplayHoverPause: true,
        lazyLoad: true,
        loop: true,
        margin: 10,
        responsiveClass: true,
        dots: true,
        responsive: {
            0: {
                items: 1
            },

            600: {
                items: 1
            },

            1024: {
                items: 2
            },

            1366: {
                items: 2
            }
        }
    });
    </script>

    @endsection
