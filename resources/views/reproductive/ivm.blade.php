@extends('layouts/reproduce-layout')

@section('meta-title')
<title>In Vitro Maturation: ivm fertility clinics | ivm treatment | ivm cost in india</title>
@endsection
@section('meta-description')
<meta name="description" content="NU Fertility,Bangalore is the best Hospital when it comes to Treatment of Fertility in Women. Top Reproductive Medicine Specialists and IVF Doctor in India are associated with us to make sure that you get the best IVM/IVF/ICSI treatments that you need." />

@endsection

@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/reproductive/IVM-Banner.jpg')}}" alt="">
@endsection

@section('content')
<div class="content">
                            <article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
                                <section class="services_page_header">
                                    <div class="services_page_featured">
                                     <img src="/images/reproductive/ivm-1.jpg" alt="" class="trt-img">
                                    </div>
                                    <h2 class="services_page_title">IVM</h2>
                                </section>
                                <section class="services_page_content entry-content" itemprop="articleBody">
                                    <div class="vc_row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element">
                                                        <div class="wpb_wrapper">
                                                            
                                                            <p>The ovaries are minimally stimulated (meaning fewer fertility medications used to make the ovaries produce eggs). Unlike traditional IVF where eggs are retrieved as close to ovulation as possible, IVM captures the eggs much sooner when they are still “immature.” The eggs are “matured” in the laboratory for about 24 to 48 hours using a culture medium containing small amounts of hormones.</p>

                                                            <div class="more-content" style="display:none;" id="more-content-1">

                                                                <p>Once mature, the eggs are fertilized using&nbsp;intracytoplasmic sperm injection (ICSI)&nbsp;– a very tiny needle containing one sperm is directly injected into the egg. The resulting embryos are transferred to the woman’s uterus.</p>

                                                                <img src="/assets/images/treatments/ivm.jpg" alt="" class="trt-img">
                                                            </div>

                                                            <button class="btn read-more-btn" onclick="toggleContent1()" id="read-more-btn-1">Read More</button>

                                                            <p>Women who typically benefit the most from IVM include women who are at higher risk for ovarian hyperstimulation syndrome (OHSS), including women with the polycystic ovarian syndrome (PCOS).</p>

                                                            <h5>Advantages of IVM include:</h5>
                                                            <ul>
                                                                <li>Less fertility medication.</li>
                                                                <li>Less risk for OHSS, a rare, but potentially serious complication.</li>
                                                                <li>Less expensive because it does not involve costly gonadotropin injections.</li>
                                                                <li>Less time than IVF.</li>
                                                            </ul>

                                                            <h5>Disadvantages of IVM include:</h5>

                                                            <div class="more-content" style="display:none;" id="more-content-2">

                                                                <ul>
                                                                    <li>Relatively new, and the overall success rates and long-term outcomes of IVM are unclear.</li>
                                                                    <li>Eggs collected via IVM are extremely sensitive and need to be handled very carefully in the lab.</li>
                                                                    <li>The outer part of these eggs can become tough for sperm to penetrate making ICSI required.</li>
                                                                </ul>

                                                               
                                                            </div>

                                                            <button class="btn read-more-btn" onclick="toggleContent2()" id="read-more-btn-2">Read More</button>
                                                            
                                                            <div class="sc_item_button sc_button_wrap">
                                                                <a href="/appointments/" id="sc_button_2017373236" class="sc_button sc_button_default sc_button_size_large sc_button_icon_left sc_button_hover_slide_left">
                                                                    <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </section>
                            </article>
                        </div>
@endsection
