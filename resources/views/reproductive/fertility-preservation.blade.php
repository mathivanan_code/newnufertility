@extends('layouts/reproduce-layout')

@section('meta-title')
<title>Embryo Freezing Fertility Treatment | Infertility Hospital & Clinic in Bangalorea | NU Fertility</title>
@endsection
@section('meta-description')
<meta name="description" content="NU Fertility Bangalore providing the high-class embryo freezing treatment in Bangalore, India. Preserve and optimize your fertility treatment with IVF embryo freezing, frozen embryo transfer process to increase your chances of getting pregnant. For more information visit Infertility Hospital & Clinic at NU Fertility center." />

@endsection



@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/reproductive/Fertility-Preservation-Bannr.jpg')}}" alt="">
@endsection


@section('content')
<div class="content">
                            <article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
                                <section class="services_page_header">
                                    <div class="services_page_featured">
                                    <img src="/images/reproductive/Fertility-Preservation_650.jpg" alt="" class="trt-img">
                                   
                                    </div>
                                    <h2 class="services_page_title">Fertility Preservation: Egg/Sperm/ Embryo Freezing</h2>
                                </section>
                                <section class="services_page_content entry-content" itemprop="articleBody">
                                    <div class="vc_row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element">
                                                        <div class="wpb_wrapper">
                                                            <p>Sperm and eggs can be frozen for future pregnancy with the help of assisted reproductive technology, like IVF/ICSI. This is often thought of as an insurance plan for planning future fertility.</p>

                                                            <h4>What is cryopreservation?</h4>
                                                            <p>Eggs freezing or medically known as oocyte cryopreservation is a procedure wherein unfertilized eggs are harvested and frozen. Many women choose this option to save her ability to get pregnant in the future when unprepared due to lifestyle or life changes in the present. It is also helpful in the case of female infertility treatment. It must be noted that human eggs don't survive freezing, as well as human embryos, which are usually aided by assisted reproductive technology.</p>
                                                            <p>Sperm freezing is medically known as sperm cryopreservation. This procedure involves freezing and storing sperm at a fertility clinic so it may be of future use. It is also useful in cases of male infertility or male infertility treatments. Samples are frozen and can be stored for years.</p>
                                                            <h4>The most common reasons for fertility preservation include the following:</h4>
                                                            <ul>
                                                                <li>Cancer patients may choose to freeze their eggs, sperm, or embryo when they are scheduled for chemotherapy</li>
                                                                <li>Stored as a backup when patients are undergoing fertility treatment for male or female fertility tests</li>
                                                                <li>It is also stored if the donor must be quarantined because of infectious diseases</li>
                                                                <li>For future plans, so that the present can be lived without the pressure of unwanted pregnancy and can be used for a planned pregnancy later on</li>
                                                            </ul>

                                                            <h4>Embryo Freezing and Frozen Embryo Transfer via assisted reproductive technology</h4>
                                                            
                                                            <p>Embryo freezing is a process wherein the eggs and sperms are fused together for fertilization in the lab. The embryos are frozen at -196 degrees in liquid nitrogen using a method called vitrification. The embryos can normally be stored for up to five years subject to periodic renewal of charges in case of male infertility. If one wishes to continue beyond that period, the center has to be intimated about it. In case of reasons to discontinue before five years, one has to again intimate the center so as to have the option of either discarding them or donating them for research or donating them for third party reproduction (as an anonymous donor).</p>
                                                            
                                                            <div class="sc_item_button sc_button_wrap">
                                                                <a href="/appointments/" id="sc_button_2017373236" class="sc_button sc_button_default sc_button_size_large sc_button_icon_left sc_button_hover_slide_left">
                                                                    <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </section>
                            </article>
                        </div>
@endsection
