@extends('layouts/reproduce-layout')
@section('meta-title')
<title>In-vitro Fertilization | Best IVF Treatment Center in Bangalore | NU Fertility</title>
@endsection
@section('meta-description')
<meta name="description" content="NU Fertility is one of the Best IVF Treatment Center in Bangalore has a team of IVF specialists expertise in diagnosing and treating infertility problems at affordable cost." />
@endsection

@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/reproductive/IVF-m.jpg')}}" alt="">
@endsection


@section('content')
<div class="content">
                            <article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
                                <section class="services_page_header">
                                    <div class="services_page_featured">
                                        <img class="trt-img" src="/images/reproductive/ivf-treatment.jpg" alt="">
                                        <img class="trt-img" src="/images/reproductive/ivf-treatment-1.jpg" alt="">
                                    </div>
                                    <h2 class="services_page_title">IVF (In-Vitro Fertilization)</h2>
                                </section>
                                <section class="services_page_content entry-content" itemprop="articleBody">
                                    <div class="vc_row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element">
                                                        <div class="wpb_wrapper">
                                                           
                                                            <p>
                                                            IVF (In vitro fertilization) or test tube baby as it is commonly called is a procedure in which eggs and the sperms are fertilized outside the body and then transferred back to the womb after 2-5 Days. 
                                                            </p>
                                                            <h5>IVF Treatment may be considered if,</h5>
                                                            <ul>
                                                                <li>You have been diagnosed with unexplained infertility</li>
                                                                <li>Your fallopian tubes are blocked</li>
                                                            </ul>
                                                            <div class="more-content" style="display:none;" id="more-content-1">
                                                            <ul>
                                                                <li>Other techniques such as fertility drugs or&nbsp;intrauterine insemination (IUI)&nbsp;have not been successful</li>
                                                                <li>The male partner has fertility problems associated with an abnormal semen analysis/ low sperm count / low motility</li>
                                                                <li>You are using your partner’s frozen sperm in your treatment and IUI is not suitable for you</li>
                                                                <li>You are using donated eggs or your own frozen eggs in your treatment</li>
                                                                <li>You are using embryo testing ( PGD) to avoid passing on a genetic condition to your child.</li>
                                                                <li>You have severe endometriosis</li>
                                                            </ul>
                                                            </div>

                                                            <button class="btn read-more-btn" onclick="toggleContent1()" id="read-more-btn-1">Read More</button>

                                                            <h5>Your IVF treatment cycle would be as follows.</h5>

                                                            <p>Step 1: Once you have decided to go ahead with IVF, you will undergo a day care procedure called&nbsp;hysteroscopy&nbsp;to assess the suitability of the uterus to hold the baby. It is generally done one month before your IVF cycle and involves the introduction of a small camera into the womb through the vagina to visualize the inside of the uterus. You will be given anaesthesia during the procedure and hence there will be no pain.</p>
                                                            <p><br>Step 2: Your husband has to freeze one semen sample as a backup.</p>
                                                            <p><br>Step 3: Typically your treatment would begin on the second or third day of periods. It involves a scan and hormone tests followed by daily injections for about 8-10 days, along with monitoring by scan and hormone tests in between. Once the eggs are sufficiently grown as per the scan and hormone tests, you will receive injection HCG as an ovulation trigger.</p>

                                                            <div class="more-content" id="more-content-2" style="display:none;">
                                                                <p>Step 4: Egg retrieval is performed transvaginally 35 – 36 hours later, with light anaesthesia, using a transvaginal ultrasound guidance. You will be discharged same evening, unless there are problems associated with bleeding, undue pain or ovarian hyper stimulation.</p><br>
                                                                <p>Step 5: After retrieval, eggs are assessed for their maturity. Meanwhile husband has to give a fresh semen sample. Mature eggs are injected with the sperms on the same day and grown in the incubator for 3-5 days.</p><br>
                                                                <p>Step 6: The final step is the embryo transfer. 3-5 days after your egg retrieval, two or three embryos are selected and gently transferred into the womb using an abdominal ultrasound guidance. The procedure is usually painless, no anaesthesia is required and you will be discharged in about two to three hours.</p><br>
                                                                <p>Step 7: You will be given certain supportive medications starting from the day of egg retrieval till your pregnancy check. You will be called for a pregnancy check 16-17 days after your embryo transfer.</p>
                                                            </div>

                                                            <button class="btn read-more-btn" onclick="toggleContent2()" id="read-more-btn-2">Read More</button>

                                                            <p>NU fertility @ NU hospitals is one of the best IVF centres in Bangalore with high success rates on par with international standards.</p>

                                                            

                                                            <div class="sc_item_button sc_button_wrap">
                                                                <a href="/appointments/" id="sc_button_2017373236" class="sc_button sc_button_default sc_button_size_large sc_button_icon_left sc_button_hover_slide_left">
                                                                    <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </section>
                            </article>
                        </div>
@endsection