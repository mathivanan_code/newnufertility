@extends('layouts/reproduce-layout')

@section('meta-title')
<title>Semen Processing for Intrauterine Insemination (IUI): Fertility Treatments in Bangalore| NU Fertility</title>
@endsection
@section('meta-description')
<meta name="description" content="Intrauterine insemination (IUI) is a fertility treatment that involves placing the washed sperms inside a woman's uterus to facilitate fertilization. It is usually combined with ovulation induction to increase the number of eggs ovulating and hence the chances of becoming pregnant." />
@endsection

@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/reproductive/IUI-b.jpg')}}" alt="">
@endsection


@section('content')
<div class="content">
                            <article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
                                <section class="services_page_header">
                                    <div class="services_page_featured">
                                        
                                    </div>
                                    <h2 class="services_page_title">Semen Processing for Intrauterine Insemination (IUI)</h2>
                                </section>
                                <section class="services_page_content entry-content" itemprop="articleBody">
                                    <div class="vc_row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element">
                                                        <div class="wpb_wrapper">
                                                            <img src="/images/reproductive/iui-trt.jpg" alt="" class="trt-img">
                                                            <p>
                                                            Intrauterine insemination (IUI) is a fertility treatment that involves placing the washed sperms inside a woman's uterus to facilitate fertilization. It is usually combined with&nbsp;ovulation induction&nbsp;to increase the number of eggs ovulating and hence the chances of becoming pregnant.
                                                            </p>
                                                            <h4>Semen Processing for IUI</h4>
                                                            <ul>
                                                                <li>The husband is asked to give semen sample on the day of the procedure which is<br>processed. This usually takes 30-40 minutes in case of fresh semen sample.</li>
                                                                <li>If frozen sample is used, it is thawed and processed on the day of the procedure.</li>
                                                            </ul>
                                                            <div class="more-content" style="display:none;"  id="more-content-1">
                                                            <ul>
                                                                <li>Since the semen contains prostaglandins which can initiate severe uterine contractions, it needs to be washed.</li>
                                                                <li>The washed sample is inseminated inside the uterine cavity. This bypasses the cervical mucous barrier increasing the number of sperm that reach the fallopian tubes and subsequently increase the chance of fertilization.</li>
                                                                <li>The woman is asked to rest for 15-20 minutes after the procedure. She can carry out her regular activities after IUI. No need for bed rest after IUI until the pregnancy check which is usually done 2 weeks after IUI.</li>
                                                            </ul>
                                                            </div>

                                                            <button class="btn read-more-btn"  onclick="toggleContent1()" id="read-more-btn-1">Read More</button>

                                                            <p>NU fertility is one of the best fertility hospitals in Bangalore offering IUI with high success rates.  Success rate of IUI ranges between 20-25% where as the cost of IUI is about 1/10 th of the cost of IVF.  Price of IUI can vary according to the medications/ injections used.</p><br>

                                                            <div class="sc_item_button sc_button_wrap">
                                                                <a href="/appointments/" id="sc_button_2017373236" class="sc_button sc_button_default sc_button_size_large sc_button_icon_left sc_button_hover_slide_left">
                                                                    <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </section>
                            </article>
                        </div>

         

@endsection