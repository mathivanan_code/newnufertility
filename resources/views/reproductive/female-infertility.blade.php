@extends('layouts/reproduce-layout')


@section('meta-title')
<title>Female Infertility | Best Fertility Services in Bangalore, India | NU Fertility India</title>
@endsection
@section('meta-description')
<meta name="description" content="Infertility is defined as the inability to conceive after 12 months of unprotected, regular sexual intercourse. It is a condition that affects up to one in six couples of reproductive age." />
@endsection


@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/reproductive/Female-Fertility-b.jpg')}}" alt="">

@endsection

@section('content')


<div class="content">
                            <article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
                                <section class="services_page_header">
                                    <div class="services_page_featured"> 
                                        <img  src="/images/reproductive/Female-Fertility-s.jpg" class="img-fluid" alt="Fetal Madcine" itemprop="image" >
                                    </div>
                                    <h2 class="services_page_title">Female Infertility</h2>
                                </section>
                                <section class="services_page_content entry-content" itemprop="articleBody">
                                    <div class="vc_row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element">
                                                        <div class="wpb_wrapper">
                                                            <p>
                                                            Infertility is defined as the inability to conceive after 12 months of unprotected, regular sexual intercourse. It is a condition that affects up to one in six couples of reproductive age.
                                                            </p>
                                                            <p>
                                                            The single biggest influence on a couple’s chances of getting pregnant is woman’s age. Male infertility plays a close second to this, together affecting almost half of all infertile couples.
                                                            </p>
                                                            <p><b>Causes of infertility may include following conditions.</b></p>
                                                            <div class="more-content" id="more-content-1" style="display:none;">
                                                                <ol>
                                                                    <li>Problem with the production of sperm or eggs.</li>
                                                                    <li>Structural or functional issues with the female or male reproductive systems.</li>
                                                                    <li>Hormone or immune conditions in men, women or both</li>
                                                                </ol>
                                                                <p>Hence it is necessary to get yourself evaluated to find out what is the cause of infertility and treat it accordingly. It is a two pronged approach to look for issues in both the male and female partner.</p>
                                                                <p>The first step for couples seeking fertility treatment begins with a thorough assessment of both partners, including a detailed medical history and a number of routine tests.</p>
                                                                <p>Fertility Tests for Women</p>
                                                            </div>

                                                            <button onclick="toggleContent1()" class="btn read-more-btn" id="read-more-btn-1">Read More</button>
                                                            
                                                            <h5>To measure female fertility, the following tests are often performed</h5>
                                                            <ul>
                                                                <li><b>Ovulation Test:</b>&nbsp;A quick test to assess if the woman is producing an egg each month or not. It could be with a blood test or over the counter ovulation kits which test the urine sample.</li>
                                                            </ul>

                                                            <div class="more-content" id="more-content-2" style="display:none;"   >
                                                                <ul>
                                                                    <li>Ultrasound Scan:&nbsp;A scan to look at the condition of the uterus for the presence of issues like fibroids, adenomyosis and ovaries for ovarian reserve, cysts or&nbsp;Polycystic Ovarian Syndrome (PCOS).</li>
                                                                    <li>Ovarian Reserve (AMH Test):&nbsp;A blood test that measures the level of AMH hormone to help estimate the number of eggs she has, especially when they are at risk of decreased ovarian reserve. Ex: Advanced maternal age/ history of previous ovarian surgery / history of cancer chemotherapy or pelvic radiation.</li>
                                                                </ul>
                                                                <p>Sometimes&nbsp; fertility specialists &nbsp;also perform additional tests for better assess the condition of the woman’s fallopian tubes, ovaries and uterus. They include</p>
                                                                <ul>
                                                                    <li>Hysterosalpingogram (HSG)</li>
                                                                    <li>Sonohysterogram</li>
                                                                    <li>Diagnostic Laparoscopy</li>
                                                                </ul>
                                                                <img src="./assets/images/treatments/female-fertility-1.jpg" alt="">
                                                            </div>

                                                            <button onclick="toggleContent2()" class="btn read-more-btn" id="read-more-btn-2">Read More</button>

                                                            <p>The most important diagnostic test to assess&nbsp;male infertility&nbsp;is a semen analysis. In nearly all cases, man does not display any obvious outward signs or symptoms that may indicate an issue with his fertility.</p>

                                                            <p>Specialist andrology assessment of sperm is essential to measure</p>

                                                            <div class="more-content" id="more-content-3" style="display:none;"   >
                                                                <ul>
                                                                    <li>The number of sperm, volume and consistency of the sample</li>
                                                                    <li>The size and shape (morphology) and ability to move (mobility) of the sperm, both of</li>
                                                                    <li>which can interfere with penetration and fertilization of an egg.</li>
                                                                </ul>
                                                                <p><b>NU Fertility offers the best fertility treatment in Bangalore (treatment for both male and female infertility), with everything available under one roof- best male and female infertility specialist, state of art embryology lab and well experienced staff.</b></p>
                                                            </div>

                                                            <button onclick="toggleContent3()" class="btn read-more-btn" id="read-more-btn-3">Read More</button>
                                                            
                                                            
                                                            <div class="sc_item_button sc_button_wrap">
                                                                <a href="/appointments/" id="sc_button_2017373236" class="sc_button sc_button_default sc_button_size_large sc_button_icon_left sc_button_hover_slide_left">
                                                                    <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                                                                </a>
                                                            </div>

                               


                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </section>
                            </article>
                        </div>

                        <script>
function toggleContent1() {
  var x = document.getElementById("more-content-1");   
  if (x.style.display === "none") {
    x.style.display = "block";
    document.getElementById("read-more-btn-1").innerHTML = "Read less";
  } else {
    x.style.display = "none";
     document.getElementById("read-more-btn-1").innerHTML = "Read More";
  }
}

function toggleContent2() {
  var x = document.getElementById("more-content-2");   
  if (x.style.display === "none") {
    x.style.display = "block";
    document.getElementById("read-more-btn-2").innerHTML = "Read less";
  } else {
    x.style.display = "none";
     document.getElementById("read-more-btn-2").innerHTML = "Read More";
  }
}

function toggleContent3() {
  var x = document.getElementById("more-content-3");   
  if (x.style.display === "none") {
    x.style.display = "block";
    document.getElementById("read-more-btn-3").innerHTML = "Read less";
  } else {
    x.style.display = "none";
     document.getElementById("read-more-btn-3").innerHTML = "Read More";
  }
}

</script>


@endsection