@extends('layouts/reproduce-layout')

@section('meta-title')
<title>Endometrial Receptivity Assay Test Cost in Bangalore, India</title>
@endsection
@section('meta-description')
<meta name="description" content="Endometrial Receptivity Assay is a procedure that ensures that the uterus is in the right condition to receive the embryo. This can significantly increase the chances of pregnancy." />
@endsection



@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/reproductive/ERA-b.jpg')}}" alt="">
@endsection

@section('content')
<div class="content">
                            <article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
                                <section class="services_page_header">
                                    <div class="services_page_featured">
                                        <img src="/images/reproductive/era.jpg" alt="" class="trt-img">
                                    </div>
                                    <h2 class="services_page_title">ERA (Endometrial Receptivity Array) Test</h2>
                                </section>
                                <section class="services_page_content entry-content" itemprop="articleBody">
                                    <div class="vc_row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element">
                                                        <div class="wpb_wrapper">
                                                            <p>The ERA Endometrial Receptivity Analysis is a personalized genetic test to diagnose the state of endometrial receptivity in the window of implantation.</p>
                                                            <p>An endometrium is receptive when it is ready for the embryo implantation. This occurs around days 19-21 in each menstrual cycle of a fertile woman. This period of receptivity is what we call the window of implantation.</p>
                                                            <p>The lack of synchronisation between the embryo ready to be implanted and endometrial receptivity is one of the causes of recurring implantation failure. This is why it is imperative to assess the endometrium in order to determine the optimal day for&nbsp;embryo transfer.</p>

                                                            <div class="more-content" style="display:none;" id="more-content-1">

                                                                <p>The ERA test requires an endometrial biopsy that should be carried out on day LH+7 (natural cycle) or day P+5 (HRT cycle). This biopsy is quickly and easily taken by a&nbsp;gynaecologist&nbsp;in their consultation room. After being sent away, the sequencing expression of 236 genes involved in the endometrial receptivity is analysed. An in-house designed computational predictor analyses the data obtained, classifying the endometrium as Receptive or Non-Receptive.</p>

                                                                <img src="/images/reproductive/era-1.jpg" alt="" class="trt-img">
                                                            </div>

                                                            <button class="btn read-more-btn" onclick="toggleContent1()" id="read-more-btn-1">Read More</button>

                                                            <p>This test has been performed on patients who have had recurring implantation failure with embryos of good morphological quality. This test is recommended for patients with seemingly normal uterus and normal endometrial thickness (≥6 mm), in which no problems are detected. A displaced window of implantation is detected in approximately 25% of these patients. This analysis helps determine the personalised window of implantation, enabling personalized embryo transfer (PET) to be performed on the basis of these results.</p>

                                                            <div class="sc_item_button sc_button_wrap">
                                                                <a href="/appointments/" id="sc_button_2017373236" class="sc_button sc_button_default sc_button_size_large sc_button_icon_left sc_button_hover_slide_left">
                                                                    <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </section>
                            </article>
                        </div>
@endsection
