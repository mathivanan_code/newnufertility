@extends('layouts/reproduce-layout')


@section('meta-title')
<title>Laparoscopic Surgery | Infertility Hospital & Clinic in Bangalore | NU Fertility</title>
@endsection
@section('meta-description')
<meta name="description" content="Laparoscopic surgery, also called minimally invasive surgery (MIS), or keyhole surgery is a modern surgical technique in which operations are performed far from their location through small incisions (usually 0.5–1.5 cm) elsewhere in the body." />

@endsection


@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/reproductive/Laparoscopic-Surgery.jpg')}}" alt="">
@endsection


@section('content')
<div class="content">
                            <article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
                                <section class="services_page_header">
                                    <div class="services_page_featured">
                                        <img src="/images/reproductive/laparoscopic-surgery.jpg" alt="" class="trt-img">
                                    </div>
                                    <h2 class="services_page_title">Laparoscopic Surgery</h2>
                                </section>
                                <section class="services_page_content entry-content" itemprop="articleBody">
                                    <div class="vc_row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element">
                                                        <div class="wpb_wrapper">

                                                            <p>Laparoscopic surgery, also called minimally invasive surgery (MIS), or keyhole surgery is a modern surgical technique in which operations are performed far from their location through small incisions (usually 0.5–1.5 cm) elsewhere in the body. Many conditions can contribute to&nbsp;infertility&nbsp;including fibroids, endometriosis, pelvic adhesions (scar tissue in the pelvis) and uterine anomalies such as a uterine septum and adhesions (scar tissue inside the uterus – Asherman’s syndrome). They can be treated by fertility enhancing laparoscopic surgeries.</p>

                                                            
                                                            <p>There are a number of advantages to the patient with laparoscopic surgery versus an open procedure. These include:</p>

                                                            <div class="more-content" style="display:none;" id="more-content-1">
                                                                <ul>
                                                                    <li>Reduced bleeding, which reduces the chance of blood transfusion.</li>
                                                                    <li>Smaller incision, which reduces pain and shortens recovery time, as well as resulting in less post-operative scarring.</li>
                                                                </ul>

                                                                <p>Although procedure times are usually slightly longer, hospital stay is less, and often with a same day discharge which leads to a faster return to everyday living.</p>

                                                                <p>Reduced exposure of internal organs to possible external contaminants thereby reduced risk of acquiring infections.</p>
                                                            </div>

                                                            <button class="btn read-more-btn" onclick="toggleContent1()" id="read-more-btn-1">Read More</button>

                                                            <h5>Fertility enhancing laparoscopic surgeries include:</h5>

                                                            <ul>
                                                                <li>Laparoscopic myomectomy for the removal of large fibroids or multiple fibroids</li>
                                                                <li>Laparoscopic treatment of advanced stage endometriosis</li>
                                                                <li>Laparoscopic ovarian drilling and chromopertubation</li>
                                                                <li>Laparoscopic tubal reanastomosis (tubal ligation reversal)</li>
                                                                <li>Laparoscopic removal of large benign ovarian masses or cysts</li>
                                                                <li>Diagnostic laparoscopy and chromopertubation</li>
                                                            </ul>

                                                            <div class="more-content" style="display:none;" id="more-content-2">
                                                                <p>Usually these procedures are combined with&nbsp;hysteroscopy&nbsp;to assess the uterine cavity better.</p>
                                                                <img src="/images/reproductive/laparoscopic-surgery-1.png" alt="" class="trt-img">
                                                            </div>

                                                            <button class="btn read-more-btn" onclick="toggleContent2()" id="read-more-btn-2">Read More</button>

                                                           

                                                            

                                                            

                                                           
                                                            
                                                            <div class="sc_item_button sc_button_wrap">
                                                                <a href="/appointments/" id="sc_button_2017373236" class="sc_button sc_button_default sc_button_size_large sc_button_icon_left sc_button_hover_slide_left">
                                                                    <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </section>
                            </article>
                        </div>
@endsection