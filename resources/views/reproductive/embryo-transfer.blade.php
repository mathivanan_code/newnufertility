@extends('layouts/reproduce-layout')

@section('meta-title')
<title>Frozen Embryo Transfer |Fertility Treatments in Bangalore| NU Fertility</title>
@endsection
@section('meta-description')
<meta name="description" content="Frozen thawed embryos can be transferred into the uterus either in a natural cycle or in a down regulated cycle." />
@endsection


@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/reproductive/Frozen-Embro-Transwer-b.jpg')}}" alt="">
@endsection

@section('content')
<div class="content">
                            <article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
                                <section class="services_page_header">
                                    <div class="services_page_featured">
                                        <img src="/images/reproductive/frozen-embry-treatment.jpg" alt="" class="trt-img">
                                    </div>
                                    <h2 class="services_page_title">Frozen Embryo Transfer</h2>
                                </section>
                                <section class="services_page_content entry-content" itemprop="articleBody">
                                    <div class="vc_row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element">
                                                        <div class="wpb_wrapper">
                                                            <p>
                                                            Frozen thawed embryos can be transferred into the uterus either in a natural cycle or in a down regulated cycle.
                                                            </p>
                                                            <h5>Natural Cycle FET:</h5>
                                                            <p>It can be done if you have ovulatory cycles. Your cycles would be monitored by scans for follicular growth and&nbsp;<span style="text-decoration: underline;"><strong>ovulation</strong></span>&nbsp;and the embryos are transferred at the appropriate time following ovulation.</p>
                                                           
                                                           <h5>Down Regulated FET:</h5>
                                                           <p>Your natural cycle is suppressed using medications and a period is artificially induced, followed by hormonal medication to prepare the uterus. </p>
                                                           <div class="more-content" style="display:none;" id="more-content-1">
                                                            <p>Once the lining of the uterus is ready, the embryos are transferred at the appropriate time. Once the frozen embryos are thawed to room temperature, only about 80-85% of them survive. Upon transferring these embryos, the success rate is good as a fresh embryo transfer approximately 50-55%.</p>
                                                            <p>Frozen embryo transfers have better success rates than the fresh embryo transfers. Hence most of us follow Freeze all policy followed by FET.</p>
                                                            </div>

                                                            <button class="btn read-more-btn" onclick="toggleContent1()" id="read-more-btn-1">Read More</button>

                                                            <h4><b>Fertility Preservation Egg &amp; Embryo Freezing</b></h4><br>
                                                            <p><b>The most common indications for fertility preservation includes the below following options.</b></p>
                                                            <ul>
                                                                <li>Cancer patients as a part of the fertility preservation strategy in those who are scheduled for cytotoxic chemotherapy.</li>
                                                                <li>Stored as a backup when patients are undergoing&nbsp;fertility treatment&nbsp;especially when they have low counts/ collection problem or deteriorating semen parameters.</li>
                                                                <li>It is also stored for donation for a period of six months as a part of the donor quarantine for infectious diseases.</li>
                                                            </ul>
                                                            
                                                            <div class="more-content" style="display:none;" id="more-content-2">
                                                                <ul>
                                                                    <li>After thawing the stored sperms, almost 50% of them do not survive and there is a definite deterioration in the quality of the semen. Depending on the quality of the thawed sample, it can be used either for,
                                                                    <ol>
                                                                    <li>Intrauterine insemination (IUI)</li>
                                                                    <li>IVF (In-vitro Fertilization)</li>
                                                                    <li>ICSI (Intracytoplasmic sperm injection)</li>
                                                                </ol>
                                                                </li>
                                                                </ul>
                                                            </div>

                                                            <button class="btn read-more-btn" onclick="toggleContent2()" id="read-more-btn-2">Read More</button>

                                                            <h5>Embryo Freezing and Frozen Embryo Transfer</h5>
                                                            <p>Embryo freezing is a process where in the unused embryos are frozen at -196 degrees in liquid nitrogen using a method called vitrification. The embryos are normally stored up to five years subject to periodic renewal of charges. If you wish to continue beyond that period, the centerhas to be intimated about it. If you wish to discontinue before five years, again you have to intimate the center and you have the option of either discarding them or donating them for research or donating them for third party reproduction (as an anonymous donor).</p>

                                                            
                                                            <div class="sc_item_button sc_button_wrap">
                                                                <a href="/appointments/" id="sc_button_2017373236" class="sc_button sc_button_default sc_button_size_large sc_button_icon_left sc_button_hover_slide_left">
                                                                    <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </section>
                                <section class="services_page_header">
                                    <div class="services_page_featured">
                                        <img src="/assets/images/treatments/blastocyst-embryo-transfer.jpg" alt="" class="trt-img">
                                        <img src="/assets/images/treatments/blastocyst-embryo-transfer-1.jpg" alt="" class="trt-img">
                                    </div>
                                    <h2 class="services_page_title">Blastocyst Embryo Transfer</h2>
                                </section>
                                <section class="services_page_content entry-content" itemprop="articleBody">
                                    <div class="vc_row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element">
                                                        <div class="wpb_wrapper">
                                                            <p>Blastocyst is a day 5 embryo. It consists of an outer trophectoderm and an inner cell mass. It is at this stage of the embryo that it gets implanted in the uterus when conception occurs naturally. Only good quality embryos reach this stage when incubated in the laboratory after activation of embryonic genome.
                                                            </p>
                                                            <p>
                                                            Thus when a blastocyst is transferred into the well prepared uterus during the window of implantation, it will have an implantation potential of 50-60%.
                                                            </p>
                                                            <div class="more-content" id="more-content-1">
                                                            <p>Fewer blastocysts can be transferred, while a higher chance of conception compared to 8-cell embryos can be guaranteed. Hence multiple&nbsp;pregnancy&nbsp;rate is also reduced.</p>
                                                            <p>NU fertility is one of the few IVF centres in Bangalore who routinely do Blastocyst transfers for IVF in more than 99% of our IVF patients thus contributing to the high success rates of IVF.</p>
                                                            </div>

                                                            <!-- <button class="btn read-more-btn" id="read-more-btn-1">Read More</button> -->

                                                        
                                                            <div class="sc_item_button sc_button_wrap">
                                                                <a href="/appointments/" id="sc_button_2017373236" class="sc_button sc_button_default sc_button_size_large sc_button_icon_left sc_button_hover_slide_left">
                                                                    <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </section>
                            </article>
                        </div>
@endsection
