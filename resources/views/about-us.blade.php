@extends('layouts/about-layout')

@section('meta-title')
<title>About NU Fertility | Infertility & ART Services in Bangalore | IVF, IVM Treatment</title>
@endsection
@section('meta-description')
<meta name="description" content="NU Fertility Bangalore started a journey in the year 2015 with a team of highly qualified professionals, who specialize in the field of infertility and reproductive services to achieve the best success in IUI, IVF, IVM, ICSI, FET, Blastocyst Transfer, Cocyte Freezing, Embryo Freezing, Semen Freezing treatments." />
@endsection

@section('content')
<div>
    <img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/about_us/About-Us.jpg')}}" alt="">
</div>
<div class="container" style="margin-top: 30px;">    
          
          <h1 class="faqmp" style="text-align: center; font-size: 30px;">About NU Fertility</h1>
          <p class="pjust">
          NU Fertility at NU Hospitals, Rajajinagar, Bangalore, India was started keeping in mind the emerging needs of infertility and ART services. The Centre is equipped with comprehensive assessment for both male and female infertility related problems. The state-of-the-art embryology laboratory is built according to the ESHRE guidelines and the quality is constantly monitored, ensuring the appropriate culture and conditions are maintained.
          A dedicated team of fertility specialists, embryologists and Andrologists continuously strive to give the best to the patient and we are proud to achieve a success rate at par with international standards. Our hospital services rank as one of the leading services in infertility treatment today.
          </p>
          <p class="pjust">
          We have a robust system of Internal Quality Assessment which captures the quality indicators from time to time and guides us on how to improve ourselves. The ambience and the warm staff further enhance your experience and ease the stream of going through the entire process of fertility treatment. 
          </p>
          <p>We offer a wide range of infertility services like</p>

          <p><b>Reproductive Medicine:</b></p>
          <ul>
          <li>Infertility Work-up,&nbsp;</li>
          <li>Ovulation Induction &amp; Follicular Monitoring,&nbsp;</li>
          <li>IUI (Intrauterine insemination)</li>
          <li>In Vitro Fertilization (IVF/ICSI)</li>
          <li>In Vitro Maturation (IVM)</li>
          <li>Egg/Sperm/Embryo Freezing,</li>
          <li>Surrogacy</li>
          <li>Donor Program</li>
          <li>Blastocyst Culture</li>
          <li>Frozen Embryo Transfer</li>
          <li>Preimplantation Genetic Testing</li>
          <li>Laparoscopic &amp; Hysteroscopic Surgeries</li>
          <li>Fertility Preservation</li>
          </ul>

          <p><b>Andrology:</b></p>
          <ul>
          <li>Erectile Dysfunction (ED) &amp; Penile Doppler Scan</li>
          <li>Penile Implant Surgery (Semirigid &amp; Inflatable)&nbsp;</li>
          <li>Penile Cosmetic Surgeries&nbsp;</li>
          <li>Andropause &amp; Testosterone Replacement Therapy</li>
          <li>Male Fertility Workup and Imaging&nbsp;</li>
          <li>Microscopic Varicocele &amp; Reconstructive Surgery</li>
          <li>Penile Vibrator for Semen collection</li>
          <li>Surgical Sperm Retrieval (PESA, TESA, TESE, Micro- TESE) </li>
          </ul>

        </div>
@endsection