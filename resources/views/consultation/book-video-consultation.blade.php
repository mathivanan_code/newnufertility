@extends('layouts/main')

@section('meta-title')
<title>Consult IVF Specialist in Bangalore | Book Appointment Online | NU Fertility</title>

@endsection
@section('meta-description')
<meta name="description" content="Childbirth is a dream most have. If you have been looking for guidance, consult IVF specialist in Bangalore. Book an appointment at NU Fertility, India and be treated at an advanced health care centre." />
@endsection

@section('content')
<style>
  ::-webkit-input-placeholder { /* Edge */
    color:#000  !important
}

:-ms-input-placeholder { /* Internet Explorer 10-11 */
    color:#000  !important}

::placeholder {
  color:#000  !important;
    border-color: #e5e5e5;
    background-color: #ffffff;
}
</style>


    <style>

.video-us{
    font-family:Poppins, sans-serif !important;
}
.empty-space{height:6.6667rem !important;}
.video-default{
    position:relative;
    padding-top:0.1px;
}
.video-default h2{
line-height:1.2rem;
font-size: 30px !important;
font-family: poppins !important;
letter-spacing: .05rem !important;
}
.video-title + .video-sub-title{
    margin-top: 1.15em;
}
.video-default h6{
    color:#000;font-family:Poppins, sans-serif;
    font-size: 14px;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 0.2px;
    line-height: 1.5em;
}
.video-sub-title + .form-default{margin-top:3.45rem;}
.video-default .trx_addons_columns_wrap{margin-right:0px;margin-left:0px;}
.video-default .trx_addons_columns_wrap::before{content:"";display:table;}
.video-default .form-default {color:#000000;}
.video-default .form-default .form_field {    margin-bottom: 1.3333em;}
input[type="text"], input[type="number"],
 input[type="email"], input[type="tel"], 
 input[type="search"], input[type="password"],
  textarea, textarea.wp-editor-area, .select_container, select, .select_container select{
    font-family: 'Poppins', sans-serif;
    font-size: 0.8em;
    font-weight: 400;
    font-style: normal;
    line-height: 1.2em;
    text-decoration: none;
    text-transform: none;}
    .video-default .form-default input[type="text"],
    .video-default .form-default input[type="email"],
    .video-default .form-default textarea{

     
        color: #000000;font-family:sen;width:100%;
    border-color: #e5e5e5;  
    background-color: #ffffff;      
        border: 1px solid #eee;
    font-size: 1em;
    line-height: 5.1em;
    font-style: normal;
    padding: 0 3.3em;
    height: 5.1em;
    -webkit-border-radius: 3em;
    -moz-border-radius: 3em;
    border-radius: 3em;
    max-width: 100%;
    margin-bottom: 5px;
  
}

.video-default .form-default .form-btn{float:right;}
.video-default .form-default .form-btn button{color: #ffffff;
margin-bottom:20px;

background:#00aff0;background:linear-gradient(to right, #f6a2de 50%, #00aff0 50%) no-repeat scroll right bottom / 210% 100% #00aff0;
    background-size: 200%;
    transition: .5s ease-out;cursor: pointer;
    display: inline-block;
    text-transform: none;
    white-space: nowrap;
    padding: 1.4em 3.9em 1.47em;
    font-size: 1.071em;
    line-height: 1em;
    font-weight: 600;
    letter-spacing: 0;
    border: none;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    -webkit-border-radius: 3em;
    -moz-border-radius: 3em;
    border-radius: 3em;
}

    .video-default .form-default .form-btn button:hover {
    color: #ffffff !important;

    background-position: left; 
    }
.confirm-hover{background:linear-gradient(to right, #f6a2de 50%, #00aff0 50%) no-repeat scroll right bottom / 210% 100% #00aff0 !important;}
.video-default .form-default .form-checkbox label{display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 700;}
textarea{overflow: auto;
    vertical-align: top;font:inherit;  font-weight: 700;width:100%;
    min-height: 13.5em;}
    textarea::-webkit-input-placeholder {
            color: #000;font-weight:700;
    }
    textarea:-moz-placeholder { /* Upto Firefox 18, Deprecated in Firefox 19  */
            color: #000;font-weight:700;
    }
    textarea::-moz-placeholder {  /* Firefox 19+ */
            color: #000;font-weight:700;  
    }
    textarea:-ms-input-placeholder {  
       color: #000;font-weight:700;  
    }
    .video-default .form-default input[type="text"]:hover,
     .video-default .form-default input[type="password"]:hover,
      .video-default .form-default input[type="email"]:hover,
       .video-default .form-default input[type="number"]:hover,
        .video-default .form-default input[type="tel"]:hover,
         .video-default .form-default input[type="search"]:hover,
          .video-default .form-default textarea:hover
          {border-color:#000;}

          .video-default .form-default input[type="text"]:focus,
     .video-default .form-default input[type="password"]:focus,
      .video-default .form-default input[type="email"]:focus,
       .video-default .form-default input[type="number"]:focus,
        .video-default .form-default input[type="tel"]:focus,
         .video-default .form-default input[type="search"]:focus,
          .video-default .form-default textarea:focus
          {border:1px solid #3eb8d7;}

    button, input, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;

}
input[type="radio"], input[type="checkbox"]{display:none;}
    @media screen and (min-width: 1024px) and (max-width: 1200px) {
        .video-default .form-default input[type="text"],
    .video-default .form-default input[type="email"],
    .video-default .form-default textarea,
    .video-default .form-default .form-btn button{
    font-size:0.8em;padding-right:2.3em;padding-left:2.3em;}
    }

            


    @media screen and (max-width: 479px) {
     .video-us{   width: 100%;
    max-width: 270px;
    margin-left: auto;
    margin-right: auto;
        
    }
    .col-lg-6.col-md-6.col-sm-12 .column-inner{padding-left:30px;padding-right:30px;}
    .content-element{padding-left:30px;text-align:left;padding-right:30px;p}
    }
</style>

<script>
$(".confirm").hover(
  function () {
    $(this).addClass("confirm-hover");
  },
  function () {
    $(this).removeClass("confirm-hover");
  }
);
</script>
<div class="ip-banner">

</div>
</div>
<div class="container-fluid video-us">
<div class="row">
    <div class="col-lg-6 col-md-12 col-sm-12">
<div class="row">
    <div class="col-lg-1 col-md-1 col-sm-1"></div>
    <div class="col-lg-10 col-md-10 col-sm-10">
<div class="column-default">
    <div class="empty-space">
        
    </div>
    <div class="video-default">
        <h2 class="video-title">Book Video Consultation!</h2>
        <h6 class="video-sub-title">BOOK ONLINE CONSULTATION TODAY!</h6>
<form id="video-form" method="post" class="form-default">
<div class="trx_addons_columns_wrap"></div>
<div class="row">
    
<div class="col-lg-6 col-md-12 col-sm-12">
<div class="mb-3 mt-3 form_field">
    <input type="text" id="name"  name="name" placeholder="Your name"/>
  </div>
</div>
<div class="col-lg-6 col-md-12 col-sm-12">
  <div class="mb-3 mt-3 form_field">
    <input type="email" id="email" placeholder="Your e-mail" name="email"/>
    </div>
</div>
</div>
<div class="mb-3 mt-3 form_field">
    <input type="text" id="phone_num" placeholder="Your Phone Number" name="phone_num"/>
    </div>
<div class="mb-3 mt-3 form_field">
    <textarea  name="message" id="message" aria-required="true" placeholder="Your message"></textarea>
</div>

<div class="mb-3 mt-3 form_field form-btn">
<button class="confirm" id="confirm">Send Message</button>
</div>
</form>
    </div>
</div>
    </div>
    <div class="col-lg-1 col-md-1 col-sm-1"></div>
</div>
    </div><!--col-6 end -->
  
    <div class="col-lg-6 col-md-12 col-sm-12"></div>
</div>
</div>

@endsection