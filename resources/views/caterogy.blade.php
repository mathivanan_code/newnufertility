@extends('layouts/blog-layout')

@section('meta-title')
<title>Best Fertility Treatment Blogs on IVF, IUI, ICSI, and IVM | NU Fertility</title>

@endsection
@section('meta-description')
<meta name="description" content="NU Fertility is one of the best fertility hospitals in Bangalore provides fertility treatments for IVF, IUI, ICSI, etc. Explore our latest articles on fertility and reproductive medicine from our fertility specialists."/>

@endsection

@section('content')
@foreach($blogs as $blog)


<article id="post-443" class="hentry">
    <div class="thumbnail">
    <a href="">
                    <img
                            class="size-full wp-image-444"
                            title="{{$blog->title}}"
                            src="{{asset('images/blog/'.$blog->images)}}"
                            alt="{{$blog->title}}"
                            width="100%" height="100%">
</a></div>
    <div class="padding-content text-center">
        <header class="entry-header">
            <h1 class="entry-title">
                <a href="{{'/'.$blog->url}}"
                   rel="bookmark">
                     {{$blog->title}}</a></h1>
        </header>
        <!--/.entry-header -->

        <div class="entry-meta">
            <ul class="list-inline">
                <li>
                    <span class="author vcard">
                        By <a class="url fn n" href="/">NU Fertility</a>                    </span>
                </li>

                <li>/</li>

                <li>
                    <span class="posted-on">Jun 24, 2019</span>
                </li>

                <li>/</li>

                <li>
                        <span class="posted-in">
                            <a href="{{'/category/'.$blog->category}}" rel="category tag">{{$blog->category}}</a>                        </span>
                </li>


            </ul>
        </div><!-- .entry-meta -->
        
        <div class="entry-content">
            
               
                {!! $blog->description !!}
           
            
         </div>

         <!-- start -->

         <!-- <div class="nc_wrapper bottom" style="background-color: rgb(255, 255, 255); display: block;">
            <div class="swp_social_panel swp_flatFresh swp_default_full_color swp_individual_full_color swp_other_full_color scale-100 scale-fullWidth swp_one nc_floater swp_three" data-position="below" data-float="bottom" data-float-mobile="bottom" data-count="0" data-floatcolor="#ffffff" style="width: 670px; left: 114px;">
                <div class="nc_tweetContainer swp_facebook" data-network="facebook"><a rel="nofollow" target="_blank" href="{{url('http://www.facebook.com/share.php?u='.$blog->url)}}" data-link="{{url('http://www.facebook.com/share.php?u='.$blog->url)}}" class="nc_tweet"><span class="swp_count swp_hide" style="transition: padding 0.1s linear 0s;"><span class="iconFiller"><span class="spaceManWilly"><i class="sw swp_facebook_icon"></i><span class="swp_share">Share</span></span></span></span></a></div>
                <div class="nc_tweetContainer swp_twitter" data-network="twitter"><a rel="nofollow" target="_blank" href="{{url('https://twitter.com/intent/tweet?text='.$blog->title.'&url='.$blog->url)}}" data-link="{{url('https://twitter.com/intent/tweet?text='.$blog->title.'&url='.$blog->url)}}" class="nc_tweet"><span class="swp_count swp_hide" style="transition: padding 0.1s linear 0s;"><span class="iconFiller"><span class="spaceManWilly"><i class="sw swp_twitter_icon"></i><span class="swp_share">Tweet</span></span></span></span></a></div>
                <div class="nc_tweetContainer swp_linkedin" data-network="linkedin"><a rel="nofollow" target="_blank" href="{{url('https://www.linkedin.com/cws/share?url='.$blog->url)}}" data-link="{{url('https://www.linkedin.com/cws/share?url='.$blog->url)}}" class="nc_tweet"><span class="swp_count swp_hide" style="transition: padding 0.1s linear 0s;"><span class="iconFiller"><span class="spaceManWilly"><i class="sw swp_linkedin_icon"></i><span class="swp_share">Share</span></span></span></span></a></div>
            </div>
        </div> -->
         <!-- end -->
    </div>
</article>
@endforeach
{!! $blogs->links() !!}

@endsection


