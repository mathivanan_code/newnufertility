@extends('layouts/doctor-layout')

@section('meta-title')
<title>Dr Prakrutha |  obstetrician-gynaecologist and a noted fertility and IVF specialis | NU Fertility</title>

@endsection
@section('meta-description')
<meta name="description" content="Dr Prakrutha is a well-known obstetrician-gynaecologist and a noted fertility and IVF specialist" />

@endsection



@section('banner_image')
<div>
    <img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('/images/doctor/Dr-Prakutha-Banner.jpg')}}" alt="">

</div>
@endsection

@section('left_content')
<div class="team_member_featured">
                                    <div class="profile-card">
                                        <div class="blank-space"></div>
                                        <div class="box-shadow">
                                            <!-- <div class="profile-img">
                                    <center><img src="assets/images/doctors/Dr Ashwini-pro.png" class="img-department"></center>
                                </div> -->
                                            <div class="profile-text text-center">
                                                <!-- <h4>Dr. Ashwini S</h4> -->
                                                <p><b>Consultant – Reproductive Medicine Specialist</b></p>
                                                <hr>
                                                <p>Padmanabhnagar, Bengaluru</p>
                                                <!-- <a href="/book-an-appointment" id="sc_button_1055736610" class="btn btn-profile row">
                                        <div class="appointment-text">Make an Appointment</div>
                                    </a> -->

                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="team_member_avatar">
                                        <img style="float: right;" 
                                            src=".images/doctor/Dr Ashwini-pro.png"
                                            class="attachment-felizia-thumb-team_sngl size-felizia-thumb-team_sngl wp-post-image"
                                            alt="Dr. Krishna Prasad T"
                                            itemprop="image"
                                        />
                                    </div>-->
                                </div>
@endsection

@section('right_content')
<div class="wpb_column vc_column_container vc_col-sm-7 sc_layouts_column_icons_position_left">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper">
                                                        <p>Dr Prakrutha is a well-known obstetrician-gynaecologist and a noted fertility and IVF specialist. A brilliant academic, she is an alumunus of the prestigious Bangalore Medical
                                                            College &amp; Research Institute. She completed her post graduation from VIMS Bellari and post
                                                            doctoral fellowship in reproductive medicine from New Delhi. She is a member of FOGSI,
                                                            BSOG, IFS, ISAR and IMA.
                                                        </p><p>A pioneer in office hysteroscopy, she is one of the earliest in Bangalore to bring hysteroscopy
                                                            into the outpatient forray. She is a young dynamic and dedicated doctor providing
                                                            individualized fertility treatments. <br><span id="dots">...</span><br><span id="more">Her areas of expertise lie in treating recurrent pregnancy
                                                                losses, implantation failures, PCOS, severe male factor infertility and treating poor
                                                                responders. Her zeal and passion for reproductive endocrinology has led her achieve immense heights,
                                                                implementing the latest protocols and recent advances.</span>
                                                        </p>
                                                        <div class="vc_empty_space height_medium" style="height: 32px;"><span class="vc_empty_space_inner"></span></div>
                                                        <button onclick="myFunction()" id="myBtn" class="btn btn-default" style="background-color: #f6a2de;">Read more</button>
                                                        <script>
                                                            function myFunction() {
                                                                var dots = document.getElementById("dots");
                                                                var moreText = document.getElementById("more");
                                                                var btnText = document.getElementById("myBtn");

                                                                if (dots.style.display === "none") {
                                                                    dots.style.display = "inline";
                                                                    btnText.innerHTML = "Read more";
                                                                    moreText.style.display = "none";
                                                                } else {
                                                                    dots.style.display = "none";
                                                                    btnText.innerHTML = "Read less";
                                                                    moreText.style.display = "inline";
                                                                }
                                                            }
                                                        </script>
                                                    </div>
                                                </div>

                                                <!-- <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                    <div class="wpb_column vc_column_container vc_col-sm-9 sc_layouts_column_icons_position_left">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div class="vc_progress_bar wpb_content_element vc_progress-bar-color-bar_grey vc_progress_bar_narrow">
                                                                    <div class="vc_general vc_single_bar">
                                                                        <small class="vc_label">Fertility Preservation <span class="vc_label_units">85%</span></small>
                                                                        <span class="vc_bar" data-percentage-value="85" data-value="85" style="background-color: #ff966b;"></span>
                                                                    </div>
                                                                    <div class="vc_general vc_single_bar">
                                                                        <small class="vc_label">Egg Freezing <span class="vc_label_units">75%</span></small>
                                                                        <span class="vc_bar" data-percentage-value="75" data-value="75" style="background-color: #ff966b;"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="vc_empty_space height_medium" style="height: 32px;"><span class="vc_empty_space_inner"></span></div>
                                                <div class="sc_item_button sc_button_wrap">
                                                    <a href="/book-an-appointment" id="sc_button_1055736610" class="sc_button sc_button_default sc_button_size_normal sc_button_icon_left sc_button_hover_slide_left" style="background-color: #0069aa;">
                                                        <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                                                        <!-- /.sc_button_text -->
                                                    </a>
                                                    <!-- /.sc_button -->
                                                </div>
                                                <!-- /.sc_item_button -->
                                                <div class="vc_empty_space" style="height: 32px;"><span class="vc_empty_space_inner"></span></div>
                                            </div>
                                        </div>
                                    </div>
@endsection

@section('details')
<section class="section">
                                    <div class="container" style="padding-bottom:30px;">
                                        <div class="tab-holder">
                                            <a id="tab-1" class="bg-light" href="javascript:void(0);" onclick="showContent(1);">Professional qualifications </a>


                                            <a id="tab-3" class="bg-light active" href="javascript:void(0);" onclick="showContent(3);">Area of Expertise</a>
                                            <a id="tab-4" class="bg-light" href="javascript:void(0);" onclick="showContent(4);">Workshops and Presentations</a>
                                            <a id="tab-5" class="bg-light" href="javascript:void(0);" onclick="showContent(5);">Professional Membership</a>
                                            <a id="tab-2" class="bg-light" href="javascript:void(0);" onclick="showContent(2);">Achievements/Recognition</a>
                                            <a id="tab-7" class="bg-light" href="javascript:void(0);" onclick="showContent(7);">Gallery</a>
                                        </div>
                                        <div class="content-holder expertise-content" style="display: none;">
                                            <h4>QUALIFICATIONS:</h4>
                                            <ul>
                                                <li>FIRM: RIDGE IVF CENTER, DELHI- 2019</li>
                                                <li>MS (OBG): VIMS, BALLARI - 2017</li>
                                                <li>MBBS: BMCRI, BANGALORE - 2013</li>
                                            </ul>
                                        </div>
                                        <!-- <div class="content-holder professinal-content">
        <h4>PUBLICATIONS:</h4>
           <ul>
            <li>Presented a poster on ‘Comparison of IVM with and without Embryoscope’ at KISAR Conference (2012)</li>
            <li>Co-authored a chapter on ‘Oocyte Donation’ in the Manual of Infertility by Dr.Kamini Rao (2012)</li>
            <li>Co-authored a chapter on ‘Embryo Donation’ in ‘The Art and Science of Assisted Reproductive Technology’ edited by SunitaTandulwadkar (2014).</li>
            <li>Poster presentation at ESHRE 2018 , Barcelona , Spain- ‘ Relationship between pregnancy rate and post wash motile sperm count in IUI cycles: Perspective from a developing country’ co-author, 2018</li>
          </ul>
        </div> -->
                                        <div class="content-holder publication-content" style="display: none;">
                                            <h4>Area of Expertise :</h4>
                                            <ul>
                                                <li>Comprehensive Fertility Care</li>
                                                <li>Assisted Reproductive Techniques</li>
                                                <li>Reproductive Endocrinology</li>
                                                <li>Fertility Preservation</li>
                                                <li>Reproductive Immunology</li>
                                                <li>High Risk Pregnancy</li>
                                                <li>Laparoscopy</li>
                                                <li>Hysteroscopy</li>
                                                <li>Urogynaecology</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder awards-content" style="display: none;">
                                            <h4>Workshops and Presentations:</h4>
                                            <ul>
                                                <li>Choices Of Endometrial Preparation In Fet Mild Stimulation Versus Hormone Therapy - Yuva-Isar Agra 2019</li>
                                                <li>Oocyte Retrieval Rates Using Gnrh Antagonist Verusus Mpa For Cos In Ivf-Icsi - Fertivision New Delhi 2019</li>
                                                <li>Maternal Outcome In Peripartum Hysterectomy At Tertiary Center – Aicog 2019</li>
                                                <li>Cme And Workshop On Gynec Oncology - Rdt Hospital, Aogs, Bathalapalli, Anathpur 2017</li>
                                                <li>Live Vaginalsurgeries And Laparoscopic Workshop And Cme – Vims, Bogs, Ballari. 2016</li>
                                                <li>Poster Presentation At Ksoga Conference 2015, Mysuru</li>
                                                <li>Paper Presentation At Aicog, Agra 2016.</li>
                                                <li>Live Gynec Endoscopy/Fetal Medicine And Art Workshop – Ksoga 2015, Mysuru.</li>
                                                <li>Preventing Early Pregnancy Loss – Cme On Epl, Bellary Obstetrics And Gynaecology Society (Bogs) 2014</li>
                                                <li>Medicolegal Workshop – Bellary Obstetrics And Gynaecology Society (Bogs) 2014</li>
                                                <li>Ovulation Induction And Iui Workshop – Bellary Obstetrics And Gynecological Society (Bogs) 2014</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder gallery-content" style="display: none;">
                                            <h4>Gallery:</h4>
                                            <div class="row">
                                                <div class="col-md-4 img-wrap">
                                                    <img class="img-responsive w-100" src="images/doctor/dr-prakrutha-sreenath/Receiving accolades for National paper presentation.jpeg" alt="">
                                                    <p>Receiving accolades for National paper presentation</p>
                                                </div>
                                                <div class="col-md-4 img-wrap">
                                                    <img class="img-responsive w-100" src="images/doctor/dr-prakrutha-sreenath/Speaker Fertivision National conference.jpeg" alt="">
                                                    <p>Speaker Fertivision National conference</p>
                                                </div>
                                                <div class="col-md-4 img-wrap">
                                                    <img class="img-responsive w-100" src="images/doctor/dr-prakrutha-sreenath/With Dr Ashok Agarwal at Chai pe Charcha - Fertivision 2019.jpeg" alt="">
                                                    <p>With Dr Ashok Agarwal at Chai pe Charcha - Fertivision 2019</p>
                                                </div>
                                                <div class="col-md-4 img-wrap">
                                                    <img class="img-responsive w-100" src="images/doctor/dr-prakrutha-sreenath/With IFS President Dr Gouri Devi and Dr Meeta Sharma.jpeg" alt="">
                                                    <p>With IFS President Dr Gouri Devi and Dr Meeta Sharma</p>
                                                </div>
                                                <div class="col-md-4 img-wrap">
                                                    <img class="img-responsive w-100" src="images/doctor/dr-prakrutha-sreenath/Yuva Isar National level scientific paper presentation.jpeg" alt="">
                                                    <p>Yuva Isar National level scientific paper presentation</p>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="content-holder surgical-content" style="display: none;">
                                            <h4>Professional Memebership:</h4>
                                            <ul>
                                                <li>IMA - INDIAN MEDICAL ASSOCIATION</li>
                                                <li>FOGSI - FEDERATION of OBSTERRIC AND GYNAECOLOGICAL SOCIETIES OF INDIA</li>
                                                <li>BSOG - BANGALORE SOCIETY OF OBSTETRICS &amp; GYNAECOLOGY</li>
                                                <li>IFS - INDIAN FERTILITY SOCIETY</li>
                                                <li>ISAR - INDIAN SOCIETY FOR ASSISTED REPRODUCTION</li>
                                                <li>IMP - Indian Menopause Society</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder professinal-content" style="display: none;">
                                            <h4>Achievements/Recognition:</h4>
                                            <ul>
                                                <li>Moderated Andrology workshop - live session with Dr Ashok Agarval - ACE Andrologist. - Fertivision, NewDelhi 2019</li>
                                                <li>Guest speaker - preventing early pregnancy loss - BOGS, Ballari 2017</li>
                                                <li>Speaker - choice of estradiol in HRT preparations -YUVAISAR, Agra 2018</li>
                                                <li>Speaker - Choices Of Endometrial Preparation In Frozen Embryo Transfer - YUVA ISAR, Agra 2019</li>
                                                <li>Speaker - Pregnancy outcomes using PPOS protocol in PCOD women - Fertivision, NewDelhi 2018</li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
@endsection

@section('doctor_blogs')
<div>

    <div class="container-Fluid">
        <div>
            <!--owl carousel 2-->
            <link rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
            <link rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
            <!--owl carousel 2 end-->
            <style>
            .banner-carousel .owl-nav {
                /* position: absolute; */
                width: 100%;
                /* display: none !important; */
            }

            .banner-carousel .owl-nav button.owl-prev,
            .banner-carousel .owl-nav button.owl-next,
            .banner-carousel .owl-nav button.owl-prev:hover,
            .banner-carousel .owl-nav button.owl-next:hover {
                position: absolute;
                background: #00000063;
                color: #fff;
                width: 45px;
                height: 45px;
                font-size: 25px;
                margin: 0;
            }

            .banner-carousel .owl-nav button.owl-prev:hover,
            .banner-carousel .owl-nav button.owl-next:hover {
                background: #000000d4;
            }

            .banner-carousel .owl-nav button.owl-prev {
                left: 0;
                top: 32%;
            }

            .banner-carousel .owl-nav button.owl-next {
                left: 0;
                top: calc(32% + 46px);
            }

            .banner-carousel .owl-nav .owl-prev span,
            .banner-carousel .owl-nav .owl-next span {
                color: #fff;
            }

            @media(min-width: 768px) {

                .banner-carousel.owl-carousel .owl-stage,
                .banner-carousel.owl-carousel .slide-item {
                    height: 100%;
                    max-height: 650px;
                    display: flex;
                }
            }
            </style>


         


            <section class="pt-5 pb-5 text-center">
                <div class="container" style="padding-top:30px;">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="sc_title" style="color:#000;text-align:center;font-weight: 600;">Doctor's blogs</h2>
                            <div class="row">
                                <div class="owl-carousel blogs-carousel owl-theme">
                                    <div class="item" id="loading">
                                        <center>
                                            <a href="#"><img src="images/about-our-centers-img/ivf.jpg" alt="" class="">
                                                <p class="text-center"
                                                    style="color:#012e5d;font-weight: 600;font-size: 12px;">Over 60%
                                                    successful IVF</p>
                                            </a>
                                        </center>
                                    </div>
                                    <div class="item" id="loading">
                                        <center>
                                            <a href="#"><img src="images/about-our-centers-img/FET.jpg" alt="" class="">
                                                <p class="text-center"
                                                    style="color:#012e5d;font-weight: 600;font-size: 12px;">Over 64%
                                                    successful FET (Frozen Embryo Transfer)</p>
                                            </a>
                                        </center>
                                    </div>
                                    <div class="item" id="loading">
                                        <center>
                                            <a href="#"><img src="images/about-our-centers-img/IUI.jpg" alt="" class="">
                                                <p class="text-center"
                                                    style="color:#012e5d;font-weight: 600;font-size: 12px;">Over 18%
                                                    successful IUI</p>
                                            </a>
                                        </center>
                                    </div>
                                    <div class="item" id="loading">
                                        <center>
                                            <a href="#"><img src="images/about-our-centers-img/IVF with dono eggs.jpg"
                                                    alt="" class="">
                                                <p class="text-center"
                                                    style="color:#012e5d;font-weight: 600;font-size: 12px;">Over 60%
                                                    successful IVF with donor eggs</p>
                                            </a>
                                        </center>
                                    </div>
                                    <div class="item" id="loading">
                                        <center>
                                            <a href="#"><img src="images/about-our-centers-img/1250 families.jpg" alt=""
                                                    class="">
                                                <p class="text-center"
                                                    style="color:#012e5d;font-weight: 600;font-size: 12px;">Over 1250
                                                    happy families </p>
                                            </a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>


            <script>
            jQuery(".blogs-carousel").owlCarousel({
                autoplay: true,
                autoplayTimeout: 12000,
                autoplayHoverPause: true,
                lazyLoad: true,
                loop: true,
                margin: 20,
                responsiveClass: true,
                nav: false,
                dots: true,
                margin: 0,
                responsive: {
                    0: {
                        items: 3
                    },

                    600: {
                        items: 3
                    },

                    1024: {
                        items: 3
                    },

                    1366: {
                        items: 3
                    }
                }
            });
            </script>

        </div>

    </div>
    @endsection