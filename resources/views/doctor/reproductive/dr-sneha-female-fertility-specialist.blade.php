@extends('layouts/doctor-layout')

@section('meta-title')
<title>Dr. Sneha | Fertility specialist | Best fertility doctor in bangalore | Reproductive Medicine | NU Fertility</title>

@endsection
@section('meta-description')
<meta name="description" content="Dr Sneha is an Infertility Specialist & Best fertility doctor in Bangalore associated with NU Fertility. She has successfully treated many Infertile couples with all the available techniques of Infertility Management & Endoscopic procedures." />

@endsection

@section('banner_image')
<div>
    <img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/doctor/Dr-Banner-Sneha.jpg')}}" alt="">

</div>
@endsection

@section('left_content')
<div class="box-shadow">
                                      
                                            <div class="profile-text text-center">
                                             
                                                <p><b>Senior Consultant- Reproductive Medicine (Female fertility) &amp; Gynaecologist</b></p>
                                                <hr>
                                                <p>NU Hospitals, Bengaluru</p>
                                          

                                            </div>
                                        </div>
@endsection

@section('right_content')
<div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper">
                                                        <p>Dr. Sneha J is an Infertility specialist &amp; Cosmetic Gynaecologist practising in Bangalore. After gaining extensive clinical experience from All India Institute of Medical Sciences (AIIMS), New Delhi, she completed a fellowship in infertility from RGUHS. She has completed observerships under reputed fertility specialists in Madrid (Spain) and Chicago (USA). She specializes in providing the whole range of infertility treatment starting from infertility workup, laparoscopic and hysteroscopic surgeries, tubal surgeries, ovarian stimulation, IUI and, IVF /ICSI treatment with good success rate. Her expertise lies in treating patients with PCOS related infertility, recurrent IVF failure, and endometriosis. She assists in donor egg IVF and donor sperm IUI/IVF services.

                                                            
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="vc_empty_space height_medium" style="height: 32px;"><span class="vc_empty_space_inner"></span></div>
                                                <!-- <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                    <div class="wpb_column vc_column_container vc_col-sm-9 sc_layouts_column_icons_position_left">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div class="vc_progress_bar wpb_content_element vc_progress-bar-color-bar_grey vc_progress_bar_narrow">
                                                                    <div class="vc_general vc_single_bar">
                                                                        <small class="vc_label">Fertility Preservation <span class="vc_label_units">85%</span></small>
                                                                        <span class="vc_bar" data-percentage-value="85" data-value="85" style="background-color: #ff966b;"></span>
                                                                    </div>
                                                                    <div class="vc_general vc_single_bar">
                                                                        <small class="vc_label">Egg Freezing <span class="vc_label_units">75%</span></small>
                                                                        <span class="vc_bar" data-percentage-value="75" data-value="75" style="background-color: #ff966b;"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->

                                                <div class="sc_item_button sc_button_wrap">
                                                    <a href="/book-an-appointment" id="sc_button_1055736610" class="sc_button sc_button_default sc_button_size_normal sc_button_icon_left sc_button_hover_slide_left" style="background-color: #0069aa;">
                                                        <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                                                        <!-- /.sc_button_text -->
                                                    </a>
                                                    <!-- /.sc_button -->
                                                </div>
                                                <!-- /.sc_item_button -->
                                                <div class="vc_empty_space" style="height: 32px;"><span class="vc_empty_space_inner"></span></div>
                                            </div>
@endsection

@section('details')
<section class="section">
                                    <div class="container" style="padding-bottom:30px;">
                                        <div class="tab-holder">
                                            <a id="tab-1" class="bg-light active" href="javascript:void(0);" onclick="showContent(1);">Professional qualifications </a>
                                            <a id="tab-7" class="bg-light" href="javascript:void(0);" onclick="showContent(7);">Gallery</a>
                                            <a id="tab-2" class="bg-light" href="javascript:void(0);" onclick="showContent(2);">Publications</a>
                                            <a id="tab-3" class="bg-light" href="javascript:void(0);" onclick="showContent(3);">Area of Expertise</a>
                                            <a id="tab-4" class="bg-light" href="javascript:void(0);" onclick="showContent(4);">International Training</a>
                                            <a id="tab-5" class="bg-light" href="javascript:void(0);" onclick="showContent(5);">Professional Membership</a>
                                            <a id="tab-6" class="bg-light" href="javascript:void(0);" onclick="showContent(6);">Conference Presentations</a>
                                            <!--<a id="tab-7" class="bg-light" href="javascript:void(0);" onclick="showContent(7);">Professional awards and Special recognitions    </a> -->
                                        </div>
                                        <div class="content-holder expertise-content" >
                                            <h4>QUALIFICATIONS:</h4>
                                            <ul>
                                                <li>MBBS: Bangalore Medical College, Bangalore (2005- 2011)</li>
                                                <li>MD (OBG): All India Institute of Medical Sciences (AIIMS), New Delhi (2012-2015)</li>
                                                <li>DNB (OBG): National Board of Examinations (NBE), New Delhi. (2016)</li>
                                                <li>FIRM: Rajiv Gandhi University of Health Sciences (RGUHS), Bangalore. (2015-2016)</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder gallery-content">

                                        <div class="row">
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor//dr-sneha-gallery/image1.jpg">
                                                <p>Dr Sneha recieving her post-graduation degree certificate at AIIMS, New Delhi from the honourable Dr. Harsh Vardhan (Minister of Health and Family Welfare) in 2015</p>
                                            </div>

                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor//dr-sneha-gallery/image2.jpg">
                                                <p>Dr Sneha with Dr Antonio Gosalvez Vega at Hospital Universitario Quironsalud, Madrid, Spain during her Reproductive Medicine Observership in 2018</p>
                                            </div>
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor//dr-sneha-gallery/image3.jpg">
                                                <p>Dr Sneha with Dr Christopher Sipe at Fertility Centre of Illinois (FCI) Chicago, USA during her Reproductive Medicine Observership in 2019</p>
                                            </div>
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor//dr-sneha-gallery/image4.jpg">
                                                <p>Dr Sneha, with the Embryologists at the Embryology lab at Hospital Universitario Quironsalud, Madrid, Spain during her Reproductive Medicine Observership (2018)</p>
                                            </div>
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor//dr-sneha-gallery/image5.jpg">
                                                <p>Dr Sneha delivering health talk on “Gynaecological issues in women” on the occasion of Women’s day celebration organised by Karnataka State Primary school teachers organisation – 2019</p>
                                            </div>
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor//dr-sneha-gallery/image6.jpg">
                                                <p>Dr Sneha presenting her poster at the annual meeting of European Society of Human Reproduction and Embryology (ESHRE 2018) at Barcelona, Spain</p>
                                            </div>

                                            </div>

                                        </div>
                                        <div class="content-holder professinal-content">
                                            <h4>PUBLICATIONS:</h4>
                                            <ul>
                                                <h5>Papers:</h5>
                                                <li>Sharma JB, Sneha J, Singh UB et al. Effect of Antitubercular Therapy on Endometrial Function in Infertile Women with Female Genital Tuberculosis, Infect Disord Drug Targets 2016;16(2):101-8.
                                                </li><li>Sharma JB, Sneha J, Singh UB et al. Comparative Study of Laparoscopic Abdominopelvic and Fallopian Tube Findings Before and After Antitubercular Therapy in Female Genital Tuberculosis With Infertility. J Minim Invasive Gynecol 2016 Feb 1;23(2):215-22.
                                                </li><li>Sharma JB, Sneha J, Singh UB et al. Effect of antitubercular treatment on ovarian function in female genital tuberculosis with infertility. J Hum Reprod Sci 2016 Jul-Sep;9(3):145-150.</li>
                                            </ul>
                                            <ul>
                                                <h5>Book chapters:</h5>
                                                <li>J Sneha. Menstrual cycle. In Kamini AR, editor. Infertility Manual. New Delhi:Jaypee Brothers; 2017.p 63-8</li>
                                                <li>J Sneha. Malpresentations. In Kamini AR, editor. Infertility. Obstetric emergencies,3rd ed. New Delhi: Jaypee Brothers; 2020.p 379-85</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder publication-content">
                                            <h4>Area of Expertise :</h4>
                                            <ul>
                                                <li>Infertility Work-up, Ovulation Induction &amp; Follicular Monitoring, IUI (Intrauterine insemination, In Vitro Fertilization (IVF / ICSI), In Vitro Maturation (IVM), Egg / Sperm / Embryo Freezing, Surrogacy, Blastocyst Culture, Frozen Embryo Transfer, Preimplantation Genetic Testing (PGT), PGD, Laparoscopic &amp; Hysteroscopic Surgeries, Fertility Preservation</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder awards-content">
                                            <h4>International Training:</h4>
                                            <ul>
                                                <li>Observership at Assisted Reproduction Unit in QuirónSalud University Hospital, Madrid, (Spain) in June 2018 under Dr Antonio Gosalves Vega.</li>
                                                <li>Observership at Fertility Centres of Illinois (FCI), Chicago (USA) in April-May 2019 under Dr Christopher Sipe.</li>
                                                <li>Pre-congress workshop on “Addressing the broad scope of fertility preservation” in July 2018 at ESHRE 2018, Barcelona (Spain).</li>
                                                <li>World Congress of International Association of Aesthetic Gynaecology and Sexual Wellbeing (IAAGSW), London (UK) in October 2018.</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder surgical-content">
                                            <h4>Professional Memebership:</h4>
                                            <ul>
                                                <li>European Society of Human Reproduction and Embryology (ESHRE).</li>
                                                <li>International Society of Sexual Medicine (ISSM).</li>
                                                <li>Federation of Obstetricians and Gynaecologists of India (FOGSI).</li>
                                                <li>Indian Medical Association (IMA).</li>
                                                <li>Association of Medical Consultants, Bangalore (AMC).</li>
                                                <li>Indian Fertility Society (IFS).</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder presentation-content">
                                            <h4>Conference Presentations :</h4>
                                            <ul>
                                                <li>“Relationship between pregnancy rate and post sperm wash motile sperm count in IUI cycles: perspective from a developing country” P-371 at 34th Annual meeting of European Society of Human Reproduction and Embryology (ESHRE) in Barcelona, Spain in July 2018.</li>
                                                <li>“Effect of Antitubercular therapy on endometrial function in women with genital tuberculosis” at AICOG 2015 on 25th Jan 2016 in Chennai, India.</li>
                                                <li>“Estrogen-oocyte ratio and its effect on pregnancy rate in women undergoing IVF-ET antagonist cycles” at LIFE 2015 on 30th Oct 2016 in Bangalore, India.</li>
                                            </ul>
                                        </div>
                                        <!--<div class="content-holder gallery-content">
            <h4> Professional awards and Special recognitions:</h4>
        </div> -->
                                    </div>

            
                                </section>

                                @endsection
@section('banner_carosal')

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 " src="images/doctor/test-bg.jpg" alt="First slide">
            <div class="carousel-caption">
                <h5>Happy Families About  Dr. Sneha J</h5>
                <p>TESTIMONIALS</p><br>
                <div class="sc_testimonials_item">
                    <div class="sc_testimonials_item_content">

                    <p>She is Super friendly ! Anytime ready for her patient ! M very lucky who got treatment with her for pregnancy ! Success results at the very first time ! Thank you so much doctor.</p>
                   

                    </div>

                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/doctor/test-bg.jpg" alt="Second slide">
            <div class="carousel-caption">
                <h5>Happy Families About  Dr. Sneha J</h5>
                <p>TESTIMONIALS</p><br>
                <div class="sc_testimonials_item">
                    <div class="sc_testimonials_item_content">
                    <p>Dr.sneha is very friendly person with positive attitude, supportive and explains things in a simple way</p>
                                                                                                     
            

                    </div>

                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/doctor/test-bg.jpg" alt="Third slide">
            <div class="carousel-caption">
                <h5>Happy Families About  Dr. Sneha J</h5>
                <p>TESTIMONIALS</p><br>
                <div class="sc_testimonials_item">
                    <div class="sc_testimonials_item_content">

                    <p class="sc_testimonials_item_content_responsive">Dr. Sneha J was very friendly & polite . Makes understand the pros and cons of the IVF. Listens to us and is transparent in every profession prosecure.<br>
                                                                        Even responses on non- Duty hours for any consultant.<br>
                                                                        Helps patient to be positive</p>

                    </div>

                </div>
            </div>
        </div>
    </div>

</div>


@endsection

@section('doctors_video')
<div>
    <section class="doctors_video">
        <div class="data-vc-full-width=" true" data-vc-full-width-init="true"
            class="vc_row wpb_row vc_row-fluid vc_custom_1480401432866 vc_row-has-fill"
            style="position: relative;; box-sizing: border-box;">
            <h2 class="sc_title" style="color:#000;text-align:center;font-weight: 600;">Doctor's Video</h2>
            <div class="row">
                                        <div class="col-md-4" style="padding-top:20px; min-height: 294px;">
                                            <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/6LEXMKwVciY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                                            <a class="mediabox" href="https://www.youtube.com/embed/6LEXMKwVciY">
                                                <img  class="img-fluid" src="images/video-thumbnail.png" style="cursor:pointer" />
                                            </a>
                                            <p class="text-center video-title">What to expect during the first visit to fertility specialist</p>
                                        </div>
                                        <div class="col-md-4" style="padding-top:20px; min-height: 294px;">
                                            <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/ZWX4RlPE7vE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                                            <a class="mediabox" href="https://www.youtube.com/embed/ZWX4RlPE7vE">
                                                <img  class="img-fluid" src="images/video-thumbnail.png" style="cursor:pointer" />
                                            </a>
                                            <p class="text-center video-title">IVF - How is it done?</p>
                                        </div>
                                        <div class="col-md-4" style="padding-top:20px; min-height: 294px;">
                                            <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/K2Ij_ga4vMI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                                            <a class="mediabox" href="https://www.youtube.com/embed/K2Ij_ga4vMI">
                                                <img  class="img-fluid" src="images/video-thumbnail.png" style="cursor:pointer" />
                                            </a>
                                            <p class="text-center video-title">Fertility problems related to Polycystic Ovarian Syndrome</p>
                                        </div>
                                        <div class="col-md-4" style="padding-top:20px; min-height: 294px;">
                                            <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/K2Ij_ga4vMI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                                            <a class="mediabox" href="https://www.youtube.com/embed/226Edc_5mFg">
                                                <img  class="img-fluid" src="images/video-thumbnail.png" style="cursor:pointer" />
                                            </a>
                                            <p class="text-center video-title">Can Thyroid Problems affect Fertility?</p>
                                        </div>
                                    </div>


        </div>
    </section>

</div>
@endsection


