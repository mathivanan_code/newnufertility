@extends('layouts/doctor-layout')

@section('meta-title')
<title>Dr Kavya V Pradeep |  Reproductive Medicine, Gynaecologist &amp; Laparoscopic Surgeon Department of Reproductive Medicine | NU Fertility</title>

@endsection
@section('meta-description')
<meta name="description" content="Dr Kavya V Pradeep  Reproductive Medicine, Gynaecologist & Laparoscopic Surgeon Department of Reproductive Medicine in Bangalore" />

@endsection




@section('banner_image')
<div>
    <img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/doctor/Dr-Banner-Kavya.jpg')}}" alt="">

</div>
@endsection

@section('left_content')
<div class="box-shadow">
                                            <!-- <div class="profile-img">
                                    <center><img src="assets/images/doctors/Dr-Kavya-2.jpg" class="img-department"></center>
                                </div> -->
                                            <div class="profile-text text-center">
                                                <!-- <h4>Dr Kavya V Pradeep</h4> -->
                                                <p><b>Consultant Reproductive Medicine, Gynaecologist &amp; Laparoscopic Surgeon Department of Reproductive Medicine</b></p>
                                                <hr>
                                                <p>NU Fertility, Shivamogga</p>
                                                <!-- <a href="/book-an-appointment" id="sc_button_1055736610" class="btn btn-profile row">
                                        <div class="appointment-text">Make an Appointment</div>
                                    </a> -->

                                            </div>
                                        </div>
@endsection

@section('right_content')
<div class="wpb_column vc_column_container vc_col-sm-7 sc_layouts_column_icons_position_left">
                                        <div class="vc_column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper">
                                                        <p>Dr. Kavya V Pradeep, an alumnus of JJM Medical College , is an expert in the management of Reproductive Medicine, Gynaecology and Laparoscopic Surgery. She is DGO, DNB (OBG), FIRM certified and will be practicing at NU Fertility in NU Hospitals, Shivamogga. <br>She specializes in providing the whole range of infertility treatment starting from infertility workup, In Vitro Fertilization (IVF / ICSI), In Vitro Maturation (IVM), Laparoscopic and Hysteroscopic surgeries, Fertility Preservation and many more treatments with good success rates
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="vc_empty_space height_medium" style="height: 32px;"><span class="vc_empty_space_inner"></span></div>
                                              

                                                <div class="sc_item_button sc_button_wrap">
                                                    <a href="/book-an-appointment" id="sc_button_1055736610" class="sc_button sc_button_default sc_button_size_normal sc_button_icon_left sc_button_hover_slide_left" style="background-color: #0069aa;">
                                                        <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                                                        <!-- /.sc_button_text -->
                                                    </a>
                                                    <!-- /.sc_button -->
                                                </div>
                                                <!-- /.sc_item_button -->
                                                <div class="vc_empty_space" style="height: 32px;"><span class="vc_empty_space_inner"></span></div>
                                            </div>
                                        </div>
                                    </div>
@endsection

@section('details')
<section class="section">
                                    <div class="container" style="padding-bottom:30px;">
                                        <div class="tab-holder">
                                            <a id="tab-1" class="bg-light active" href="javascript:void(0);" onclick="showContent(1);">Qualifications</a>
                                            <a id="tab-3" class="bg-light" href="javascript:void(0);" onclick="showContent(3);">Work Experience</a>
                                            <a id="tab-7" class="bg-light" href="javascript:void(0);" onclick="showContent(7);">Gallery</a>
                                            <a id="tab-2" class="bg-light" href="javascript:void(0);" onclick="showContent(2);">Area of Expertise</a>
                                            <a id="tab-8" class="bg-light" href="javascript:void(0);" onclick="showContent(8);">Workshops</a>

                                            <a id="tab-4" class="bg-light" href="javascript:void(0);" onclick="showContent(4);">Awards</a>
                                            <a id="tab-5" class="bg-light" href="javascript:void(0);" onclick="showContent(5);">Professional Membership</a>
                                            <a id="tab-6" class="bg-light" href="javascript:void(0);" onclick="showContent(6);">Conference Presentations &amp; Publications</a>
                                            <a id="tab-9" class="bg-light" href="javascript:void(0);" onclick="showContent(9);">CME/Conferences Attended</a>

                                        </div>
                                        <div class="content-holder expertise-content">
                                            <h4>Qualifications:</h4>
                                            <ul>
                                                <li>MBBS: JJM Medical College, Davengere (2001-2006)</li>
                                                <li>DGO: Sri Devraj Urs Medical College &amp; research Institute, Kolar (2008-2010)</li>
                                                <li>DNB (OBG) : Apollo BGS Hospitals, Mysore ( 2015-2017)</li>
                                                <li>FIRM: Gunasheela Hospitals, Bangalore (2019-2020)</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder professinal-content">
                                            <h4>Area of Expertise:</h4>
                                            <ul>
                                                <li>Infertility Work-up, Ovulation Induction &amp; Follicular Monitoring</li>
                                                <li>IUI (Intrauterine insemination)</li>
                                                <li>In Vitro Fertilization (IVF / ICSI)</li>
                                                <li>In Vitro Maturation (IVM)</li>
                                                <li>Egg / Sperm / Embryo Freezing</li>
                                                <li>Surrogacy, Blastocyst Culture</li>
                                                <li>Frozen Embryo Transfer</li>
                                                <li>Preimplantation Genetic Testing (PGT)</li>
                                                <li>PGD</li>
                                                <li>Laparoscopic &amp; Hysteroscopic Surgeries</li>
                                                <li>Fertility Preservation</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder publication-content">
                                            <h4>Work Experience:</h4>
                                            <ul>
                                                <li>Medical officer at SNCU, S.N.R. District hospital , Kolar in 2010</li>
                                                <li>Consultant in the department of Obstertics and Gynecology, at Joshi Hospital,Bangalore from sep 2010 to feb 2011</li>
                                                <li>Under NRHM ,worked at chickbalapur District hospital,Chickbalapur in 2011</li>
                                                <li>Consultant in the department of Obstertics and Gynecology, at Pragathi Speciality Hospital,Puttur,Mangalore from May 2011 to 2014</li>
                                                <li>2014-2017- at Apollo BGS hospitals, Mysore</li>
                                                <li>Consultant in the department of Obstertics and Gynecology, AT SARJI HOPSITAL,SHIMOGA,(2017 -2019)</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder awards-content">
                                            <h4>Awards:</h4>
                                            <ul>
                                                <li>1st prize in E Poster presentation on * Is Embryogenesis and ART outcome different in PCOS.* At 5th international Annual virtual conference. PCOS society of India. 2020</li>
                                                <li><b>1st Prize</b> in Paper presentation at FOGSI ICOG NCD VIRTUAL CONFERENCE FEB 2021 <br> Topic: Comparison of Art Outcome In Frozen Embryo Transfer Cycle Using Estradiol Valerate And Estradiol Transdermal Gel</li>
                                                <li><b>Topper </b>from university of Sri Devraj Urs, kolar, awarded with certificate of Merit for postgraduation in Department of OBG</li>
                                                <li><b>2nd</b> PRIZE in SDPP in BSOG conference in 2011</li>
                                                <li>Quiz competition on High Risk Pregnancy at Focus,Thrissur,Kerala</li>
                                                <li><b>Guest speaker</b> on Gynecological problems in rotatary club, Puttur</li>
                                                <li><b>Guest speaker</b> on Adolesant health problems to St.pholomina students , Puttur</li>
                                                <li><b>Guest speaker</b> on common health problems in women to Public, Puttur</li>
                                                <li><b>Guest speaker </b><span>on common health problems in women to teachers , shimoga</span></li>
                                            </ul>
                                        </div>
                                        <div class="content-holder gallery-content">

                                        <div class="row">

                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor/dr-kavya-gallery/Patient consultation with Dr. Kavya.jpeg">
                                                <!-- <p>Invited research paper presentation at ISAR conference, Bangalore</p> -->
                                            </div>
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor/dr-kavya-gallery/Invited research paper presentation at ISAR conference. Bangalore (1).jpeg">
                                                <p>Invited research paper presentation at ISAR conference, Bangalore</p>
                                            </div>

                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor/dr-kavya-gallery/Invited research paper presentation at ISAR conference. Bangalore (2).jpeg">
                                                <p>Invited research paper presentation at ISAR conference, Bangalore</p>
                                            </div>
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor/dr-kavya-gallery/Invited research paper presentation at ISAR conference. Bangalore.jpeg">
                                                <p>Invited research paper presentation at ISAR conference. Bangalore</p>
                                            </div>
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor/dr-kavya-gallery/Receiving reproductive medicine fellowship degree at Gunasheela hospital.Bangalore ( Dr.Devika Gunasheela and Dr Madhuri Patil).jpeg">
                                                <p>Receiving reproductive medicine fellowship degree at Gunasheela hospital.Bangalore ( Dr.Devika Gunasheela and Dr Madhuri Patil)</p>
                                            </div>
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor/dr-kavya-gallery/Receiving reproductive medicine fellowship degree at Gunasheela hospital.Bangalore Dr.Devika Gunasheela.jpeg">
                                                <p>Receiving reproductive medicine fellowship degree at Gunasheela hospital.Bangalore Dr.Devika Gunasheela</p>
                                            </div>
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor/dr-kavya-gallery/With Dr. Leanee Pacella Ince (embryologist) from Australia (2019).jpeg">
                                                <p>With Dr. Leanee Pacella Ince (embryologist) from Australia (2019)</p>
                                            </div>
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor/dr-kavya-gallery/With Dr.Madhuri Patil, Infertilty specialist . Dr.Patils clinic. Bangalore at ISAR conference (2).jpeg">
                                                <p>With Dr.Madhuri Patil, Infertilty specialist . Dr.Patils clinic. Bangalore at ISAR conference</p>
                                            </div>
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor/dr-kavya-gallery/With Dr.Madhuri Patil, Infertilty specialist . Dr.Patils clinic. Bangalore at ISAR conference.jpeg">
                                                <p>With Dr.Madhuri Patil, Infertilty specialist . Dr.Patils clinic. Bangalore at ISAR conference</p>
                                            </div>
                                            <div class="col-md-4 img-wrap">
                                                <img class="img-responsive w-100" src="images/doctor/dr-kavya-gallery/Dr. Kavya.PNG">
                                                <p>First prize in moderated poster presentation at 5th International virtual conference. 2020. Topic: Embryogenesis and ART outcome different in PCOS</p>
                                            </div>
                                            </div>

                                        </div>
                                        <div class="content-holder surgical-content">
                                            <h4>Professional Memebership:</h4>
                                            <ul>
                                                <li>Life member of IFS (Indian fertility society)</li>
                                                <li>Federation of Obstetric and Gynaecological Societies of India</li>
                                                <li>Indian Medical Association.</li>
                                                <li>Shivamogga&nbsp; Obstetric and Gynaecological Society</li>
                                                <li>PCOS Society- Patron Member</li>
                                                <li>Indian Society for Assisted Reproduction (ISAR)</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder presentation-content">
                                            <h4>Conference Presentations</h4>
                                            <ul>
                                                <li>Research paper presentation at KISAR , 2019. Comparison of ART outcome in frozen embryo transfer cycle using estradiol valerate and transdermal estrogen gel.</li>
                                                <li>E Poster presentation on Is Embryogenesis and ART outcome different in PCOS. At 5th international Annual virtual conference. PCOS society of India. 2020</li>
                                            </ul>
                                            <h4>Publications</h4>
                                            <ul>
                                                <li><b>Co investigator </b><span>in the clinical research ( On Efficacy of polyclonal versus monoclonal&nbsp; anti D in prevention of hemolytic disease of newborn)</span></li>
                                                <span>Role of umblical&nbsp; artery Doppler in high risk pregnancy .- JEMDS</span>
                                                <li><span>E poster : elimination dysfunction- A&nbsp; cause of chronic pelvic pain.</span></li>
                                                <li>Immature Teratoma in the oral cavity of the fetus-rare Presentation”- JEBMH</li>
                                                <li>Comparison of ART outcome in FET cycle using oral estradiol and&nbsp; transdermal&nbsp; estradiol (Fertility &amp; Sterility).</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder conferences-content">
                                            <h4>CME/Conferences Attended</h4>
                                            <ul>
                                                <li>Wisdom conference, Bellary OBG society, Bellary in&nbsp; DEC2008 and DEC2009</li>
                                                <li>20th KSOGA conference in September 2009</li>
                                                <li>IMA monthly PUTTUR CME from 2011 to 2014</li>
                                                <li>MATERNAL MEDICINE 2013, Conducted by Mangalore obstetrics and gynecological society at Father muller medical college. Mangalore</li>
                                                <li>26th KSOGA Conference in Nov 2015, MYSORE</li>
                                                <li>APOLLO medicine update CME in May 2016,mysore</li>
                                                <li>FOCUS PROGRAMME at Thrissur, Kerala in Aug 2016</li>
                                                <li>MOGS monthly CME from 2015 to 2017</li>
                                                <li>APGAR 2 WITH FOGSI FORCE Programme in Feb 2017</li>
                                                <li>28TH KSOGA CONFERENCE, SHIMOGA, NOV 2017</li>
                                                <li>KISAR conference 2019</li>
                                            </ul>
                                        </div>
                                        <div class="content-holder work-shops">
                                            <h4>Workshops:</h4>
                                            <ul>
                                                <li><b>Neonatal Resusicitation under NRHM </b>held at St.Johns Medical college,Bangalorein june 2010</li>
                                                <li><b>HUMAN LACTATION WORKSHOP </b>NRHM held at St.Johns Medical college,Bangalorein AUGUST 2010</li>
                                                <li><b>WORKSHOP ON </b>LIVE GYNEC ENDOSCOPY,INFERTILITY,ULTRASOUND AND FETAL MEDICINE IN NOV 2015 KSOGA conference,Mysore</li>
                                                <li>LIVE<b> Colposcopy </b>workshop at JSS medical college,MYSORE in 2017</li>
                                                <li>IUI WORKSHOP AT SHIMOGA 2017</li>
                                                <li>Monthly CME at shimoga under shimoga obstetrics and Gynecological society since 2017.</li>
                                                <li>KISAR conference , Bangalore. Dec 2019</li>
                                                <li>CME on ovulation induction, Bangalore dec 2019</li>
                                                <li>PGS workshop at MANIPAL, DEC 2017</li>
                                            </ul>
                                        </div>
                                    </div>
                                </section>
@endsection

@section('banner_carosal')

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 " src="images/doctor/test-bg.jpg" alt="First slide">
            <div class="carousel-caption">
                <h5>Happy Families About Dr. Kavya Pradeep</h5>
                <p>TESTIMONIALS</p><br>
                <div class="sc_testimonials_item">
                    <div class="sc_testimonials_item_content">
                    <p>It's good service and the Dr Kavya from gynecology too humble and caring</p>
                                                            
                    </div>

                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/doctor/test-bg.jpg" alt="Second slide">
            <div class="carousel-caption">
                <h5>Happy Families About  Dr. Kavya Pradeep</h5>
                <p>TESTIMONIALS</p><br>
                <div class="sc_testimonials_item">
                    <div class="sc_testimonials_item_content_more">
                    <p>V recommend Dr. KAVYA madam, INFERTILITY specialist at NU hospitals shimoga for her best fertility services. V r married since 15 yrs, and we have visited many places in and around karnataka. And all our treatments went in vain. We lost hope. V met Dr. PRADEEP, urologist at NU hospitals for my urinary complaints and we expressed our grief near sir. Sir referred us near Dr. Kavya for fertility issue. She mentally prepared us, which was much needed at that time for us, and finally by end of 6 months under her treatment, good news entered our life and finally on 29.6.2019, I gave birth a baby.. And finally our dream came true.</p>
                                                             

                    </div>

                </div>
            </div>
        </div>
       
    </div>

</div>


@endsection