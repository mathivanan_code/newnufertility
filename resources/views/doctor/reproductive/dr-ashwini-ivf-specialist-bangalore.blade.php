@extends('layouts/doctor-layout')

@section('meta-title')
<title>Dr. Ashwini | IVF specialist | best gynecologist in bangalore | NU Fertility</title>

@endsection
@section('meta-description')
<meta name="description" content="Dr Ashwini is an IVF Specialist & Best gynecologist in Bangalore associated with NU Fertility dealing with Infertility problems. She has more than 12 years of experience.She has done more than 2000 IVF procedures with success rate of 50%.She is an expert in laparoscopic Hysteroscopic surgery for infertility in women." />

@endsection

@section('banner_image')
<div>
    <img class="img-responsive w-100 h-100" style="width:100%" src="{{asset('images/doctor/Dr-Banner-Ashwini.jpg')}}"
        alt="">

</div>
@endsection

@section('left_content')
<div class="box-shadow">

    <div class="profile-text text-center">
        <p><b>Sr. Consultant &amp; Head – Department of Reproductive Medicine</b></p>
        <hr>
        <p>NU Hospitals, Bengaluru</p>
    </div>
</div>
@endsection

@section('right_content')
<div class="wpb_column vc_column_container vc_col-sm-7 sc_layouts_column_icons_position_left">
    <div class="vc_column-inner">
        <div class="wpb_wrapper">
            <div class="wpb_text_column wpb_content_element">
                <div class="wpb_wrapper">
                    <p>Dr. Ashwini S is a Sr. Consultant &amp; Head – Department of Reproductive Medicine, NU Fertility
                        @ NU Hospitals, she completed her MBBS from Government Medical College, Mysore and DGO in
                        Obstetrics &amp; Gynaecology from Bangalore Medical College &amp; further went on to purse DNB
                        in OBG. Dr. Ashwini has specialized in Reproductive medicine through Fellowship from Rajiv
                        Gandhi University of Health Sciences (RGUHS).


                    </p>
                    <p>She has accrued more than 15 years of experience in the field of Obstetrics and
                        Gynaecology, out of which more than 10 years of expertise is exclusively in Infertility, IVF
                        and Assisted Reproductive Techniques. She has been getting the best pregnancy rates with
                        IUI, IVF / ICSI, and egg donation.<span id="dots">...</span><br><span class="read-more"
                            id="more">To her credit, she has done more than 3000 IVF procedures (a large proportion of
                            them
                            being cases of recurrent IVF failures) and embryo transfers with a consistent pregnancy rate
                            of more than 60%. She is one among the best infertility specialists in Bangalore. Her core-
                            competence is in treating patients with recurrent IVF failures, PCOD, and recurrent
                            abortions.</span>
                    </p>
                    <div class="vc_empty_space height_medium" style="height: 32px;"><span
                            class="vc_empty_space_inner"></span></div>
                    <button onclick="myFunction()" id="myBtn" class="btn btn-default"
                        style="background-color: #f6a2de;">Read more</button>
                    <script>
                    function myFunction() {
                        var dots = document.getElementById("dots");
                        var moreText = document.getElementById("more");
                        var btnText = document.getElementById("myBtn");

                        if (dots.style.display === "none") {
                            dots.style.display = "inline";
                            btnText.innerHTML = "Read more";
                            moreText.style.display = "none";
                        } else {
                            dots.style.display = "none";
                            btnText.innerHTML = "Read less";
                            moreText.style.display = "inline";
                        }
                    }
                    </script>
                </div>
            </div>
            <div class="vc_empty_space height_medium" style="height: 32px;"><span class="vc_empty_space_inner"></span>
            </div>
            <div class="sc_item_button sc_button_wrap">
                <a href="/book-an-appointment" id="sc_button_1055736610"
                    class="sc_button sc_button_default sc_button_size_normal sc_button_icon_left sc_button_hover_slide_left"
                    style="background-color: #0069aa;">
                    <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                    <!-- /.sc_button_text -->
                </a>
                <!-- /.sc_button -->
            </div>
            <!-- /.sc_item_button -->
            <div class="vc_empty_space" style="height: 32px;"><span class="vc_empty_space_inner"></span></div>
        </div>
    </div>
</div>
@endsection

@section('details')

<section class="section">
    <div class="container" style="padding-bottom:30px;">
        <div class="tab-holder">
            <a id="tab-1" class="bg-light active" href="javascript:void(0);" onclick="showContent(1);">Professional
                qualifications </a>
            <a id="tab-7" class="bg-light" href="javascript:void(0);" onclick="showContent(7);">Gallery</a>
            <!-- <a id="tab-2" class="bg-light" href="javascript:void(0);" onclick="showContent(2);">Publications</a> -->
            <a id="tab-3" class="bg-light" href="javascript:void(0);" onclick="showContent(3);">Area of Expertise</a>
            <a id="tab-4" class="bg-light" href="javascript:void(0);" onclick="showContent(4);">Awards &amp;
                Achievements</a>
            <a id="tab-5" class="bg-light" href="javascript:void(0);" onclick="showContent(5);">Professional
                Membership</a>
        </div>
        <div class="content-holder expertise-content" id="tab-content-1" style="display: none;">
            <h4>QUALIFICATIONS:</h4>
            <ul>
                <li>MBBS - Govt. Medical College, Mysore, 2003</li>
                <li>DGO - Bangalore Medical College and Research Institute, Bangalore, 2007</li>
                <li>DNB - Obstetrics &amp;Gynaecology - HAL Hospital Bangalore, 2011</li>
                <li>FIRM (Fellowship in Reproductive Medicine) - Rajiv Gandhi University of Health Sciences (RGUHS).
                </li>
            </ul>
        </div>
        <!-- <div class="content-holder professinal-content">
        <h4>PUBLICATIONS:</h4>
           <ul>
            <li>Presented a poster on ‘Comparison of IVM with and without Embryoscope’ at KISAR Conference (2012)</li>
            <li>Co-authored a chapter on ‘Oocyte Donation’ in the Manual of Infertility by Dr.Kamini Rao (2012)</li>
            <li>Co-authored a chapter on ‘Embryo Donation’ in ‘The Art and Science of Assisted Reproductive Technology’ edited by SunitaTandulwadkar (2014).</li>
            <li>Poster presentation at ESHRE 2018 , Barcelona , Spain- ‘ Relationship between pregnancy rate and post wash motile sperm count in IUI cycles: Perspective from a developing country’ co-author, 2018</li>
          </ul>
        </div> -->
        <div class="content-holder publication-content" style="display: none;">
            <h4>Area of Expertise :</h4>
            <ul>
                <li>Infertility Work-up, Ovulation Induction &amp; Follicular Monitoring, IUI (Intrauterine
                    insemination, In Vitro Fertilization (IVF / ICSI), In Vitro Maturation (IVM), Egg / Sperm / Embryo
                    Freezing, Surrogacy, Blastocyst Culture, Frozen Embryo Transfer, Preimplantation Genetic Testing
                    (PGT), PGD, Laparoscopic &amp; Hysteroscopic Surgeries, Fertility Preservation</li>
            </ul>
        </div>
        <div class="content-holder awards-content" style="display: none;">
            <h4>AWARDS / ACHIEVEMENTS:</h4>
            <ul>
                <li>Secured 9th Rank in DGO (RGUHS 2007)</li>
                <li>Best speaker award – DNB teaching program organised by BSOG (2010)</li>
                <li>Panelist in the workshop on Recurrent Pregnancy Loss organised by BSOG (2014)</li>
                <li>Speaker on ‘Leutinised Unruptured Follicle’ at Gnanavarsha conference organised by BSOG (2014).</li>
                <li>Had been a part of the IUI workshop organised by Gunasheela IVF Centre (2014)</li>
                <li>Speaker on ‘Luteal Phase Support in ART’ at an Infertility CME organised by Gunasheela IVF Centre in
                    association with IMA Bangalore (2014)</li>
                <li>Organised a CME ‘Insights into Infertility and ART’ at NU Hospital (2015).</li>
                <li>Speaker on ‘Fluid in endometrial cavity’ KISAR Conference (2018)</li>
                <li>Speaker on ‘Does reducing weight before IVF affect the outcome’ KISAR Conference (2019)</li>
                <li>Panellist in IFS Conference’ Trouble shooting in ovarian stimulation’ (Sep 2019)</li>
                <li>Speaker ‘Use and abuse of technology – Timelapse” ISAR Conference, Hyderabad (2020)</li>
                <li>Panellist ‘Controversies in ART’ ISAR Conference, Hyderabad (2020)</li>
                <li>Panellist ‘Improving the outcomes of ART’ ISAR Conference, Hyderabad (2020)</li>
            </ul>
        </div>
        <div class="content-holder gallery-content" style="display: none;">

            <div class="row">

                <div class="col-md-4 img-wrap">
                    <img class="img-responsive w-100"
                        src="images/doctor/dr-ashwin-gallery/Dr. Ashwini S awarded Service Excellence in Reproductive Medicine.jpeg">
                    <p>Dr. Ashwini S awarded Service Excellence in Reproductive Medicine</p>
                </div>

                <div class="col-md-4 img-wrap">
                    <img class="img-responsive w-100"
                        src="images/doctor/dr-ashwin-gallery/ADDRESSING THE TEAM OF DOCTORS AT VICTORIA HOSPITAL IN SEYCHELLES.jpg">
                    <p>ADDRESSING THE TEAM OF DOCTORS AT VICTORIA HOSPITAL IN SEYCHELLES</p>
                </div>

                <div class="col-md-4 img-wrap">
                    <img class="img-responsive w-100"
                        src="images/doctor/dr-ashwin-gallery/CHAIRPERSON AT THE NATIONAL CONFERENCE ISAR 2020.jpg">
                    <p>CHAIRPERSON AT THE NATIONAL CONFERENCE ISAR 2020</p>
                </div>
                <div class="col-md-4 img-wrap">
                    <img class="img-responsive w-100"
                        src="images/doctor/dr-ashwin-gallery/CHAIRPERSON NATIONAL CONFERENCE- ISAR-2019 IN MUMBAI (1).jpg">
                    <p>CHAIRPERSON AT THE NATIONAL CONFERENCE ISAR 2020</p>
                </div>
                <div class="col-md-4 img-wrap">
                    <img class="img-responsive w-100"
                        src="images/doctor/dr-ashwin-gallery/CHAIRPERSON- NATIONAL CONFERENCE- ISAR-2019 IN MUMBAI (3).jpg">
                    <p>CHAIRPERSON AT THE NATIONAL CONFERENCE ISAR 2020</p>
                </div>
                <div class="col-md-4 img-wrap">
                    <img class="img-responsive w-100"
                        src="images/doctor/dr-ashwin-gallery/CME IN BGS MEDICAL COLLEGE GUEST FACULTY (1).jpg">
                    <p>CME IN BGS MEDICAL COLLEGE GUEST FACULTY</p>
                </div>
                <div class="col-md-4 img-wrap">
                    <img class="img-responsive w-100"
                        src="images/doctor/dr-ashwin-gallery/CME IN BGS MEDICAL COLLEGE GUEST FACULTY.jpg">
                    <p>CME IN BGS MEDICAL COLLEGE GUEST FACULTY</p>
                </div>
            </div>

        </div>

        <div class="content-holder surgical-content" style="display: none;">
            <h4>Professional Memebership:</h4>
            <ul>
                <li>Indian Medical Association (IMA)</li>
                <li>Indian Society for Assisted Reproduction (ISAR)</li>
                <li>Karnataka Chapter of ISAR (KISAR)</li>
                <li>Indian Fertility Society (IFS)</li>
                <li>ANBAI</li>
                <li>Bangalore society of obstetrics and gynaecology (BSOG)</li>
            </ul>
        </div>
    </div>
</section>


@endsection
@section('banner_carosal')

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 " src="images/doctor/test-bg.jpg" alt="First slide">
            <div class="carousel-caption">
                <h5>Happy Families About Dr. Ashwini S</h5>
                <p>TESTIMONIALS</p><br>
                <div class="sc_testimonials_item">
                    <div class="sc_testimonials_item_content">

                        <p>
                            She's a great human, she knows better, she's very smart thinker, good in explaining the
                            problems to patients, fantastic doctor
                        </p>


                    </div>

                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/doctor/test-bg.jpg" alt="Second slide">
            <div class="carousel-caption">
                <h5>Happy Families About Dr. Ashwini S</h5>
                <p>TESTIMONIALS</p><br>
                <div class="sc_testimonials_item">
                    <div class="sc_testimonials_item_content">

                        <p class="sc_testimonials_item_content_responsive">
                            Dr. Ashwini is real expert in the field of infertility with repeated IVF failures.<br>
                            We have visited lot of renowned infertility centres in Bangalore and even after multiple ivf
                            cycles there was no success. After 4 ivf failures at various infertility centers, We
                            consulted Dr. Ashwini at NU hospital Rajajinagar and got success in the first cycle itself.
                            She diagnosed the problem and started the treatment considering all the previous history. I
                            strongly recommend Dr. Ashwini for those trying to conceive through IVF.
                        </p>


                    </div>

                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/doctor/test-bg.jpg" alt="Third slide">
            <div class="carousel-caption">
                <h5>Happy Families About Dr. Ashwini S</h5>
                <p>TESTIMONIALS</p><br>
                <div class="sc_testimonials_item">
                    <div class="sc_testimonials_item_content">

                        <p>
                            Doctor is a very friendly and experienced. We are very much satisfied with the treatment She
                            has given perfect guidance and has also been of great source of mental support.<br>
                            We strongly recommend Dr Ashwini mam.
                        </p>


                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

@endsection

@section('doctors_video')
<div>
    <section class="doctors_video">
        <div class="data-vc-full-width=" true" data-vc-full-width-init="true"
            class="vc_row wpb_row vc_row-fluid vc_custom_1480401432866 vc_row-has-fill"
            style="position: relative;; box-sizing: border-box;">
            <h2 class="sc_title" style="color:#000;text-align:center;font-weight: 600;">Doctor's Video</h2>
            <div class="row">
                <div class="col-md-4" style="padding-top:20px; min-height: 294px;">
                    <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/9o97Wq0EY8I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                    <a class="mediabox" href="https://www.youtube.com/embed/9o97Wq0EY8I">
                        <img src="images/video-thumbnail.png" style="cursor:pointer" class="img-fluid" />
                    </a>
                    <p class="text-center video-title">Low Ovarian Reserve</p>
                    <p></p>
                </div>
                <div class="col-md-4" style="padding-top:20px; min-height: 294px;">
                    <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/U8_E6kM6jmg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                    <a class="mediabox" href="https://www.youtube.com/embed/U8_E6kM6jmg">
                        <img src="images/video-thumbnail.png" style="cursor:pointer" class="img-fluid" />
                    </a>
                    <p class="text-center video-title">The process of ERA or Endometrial Receptivity Array</p>
                </div>
                <div class="col-md-4" style="padding-top:20px; min-height: 294px;">
                    <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/9LqUhcNF0Xk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                    <a class="mediabox" href="https://www.youtube.com/embed/9LqUhcNF0Xk">
                        <img src="images/video-thumbnail.png" style="cursor:pointer" class="img-fluid" />
                    </a>
                    <p class="text-center video-title">Endometriosis</p>
                </div>
                <div class="col-md-4" style="padding-top:20px; min-height: 294px;">
                    <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/9LqUhcNF0Xk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                    <a class="mediabox" href="https://www.youtube.com/embed/uyZL4HyeeQo">
                        <img src="images/video-thumbnail.png" style="cursor:pointer" class="img-fluid" />
                    </a>
                    <p class="text-center video-title">What are the treatment options for unexplained infertility?</p>
                </div>
                <div class="col-md-4" style="padding-top:20px; min-height: 294px;">
                    <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/9LqUhcNF0Xk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                    <a class="mediabox" href="https://www.youtube.com/embed/rfqKROx4cbY">
                        <img src="images/video-thumbnail.png" style="cursor:pointer" class="img-fluid" />
                    </a>
                    <p class="text-center video-title">Why should we consider doing a Blastocyst transfer?</p>
                </div>
                <div class="col-md-4" style="padding-top:20px; min-height: 294px;">
                    <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/9LqUhcNF0Xk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
                    <a class="mediabox" href="https://www.youtube.com/embed/wbidseOxveA">
                        <img src="images/video-thumbnail.png" style="cursor:pointer" class="img-fluid" />
                    </a>
                    <p class="text-center video-title">COVID- 19 In Pregnancy</p>
                </div>
            </div>
        </div>
    </section>

</div>
@endsection


@section('doctor_blogs')
<div>

    <div class="container-Fluid">
        <div>
            <!--owl carousel 2-->
            <link rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
            <link rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css">
            <!--owl carousel 2 end-->
            <style>
            .banner-carousel .owl-nav {
                /* position: absolute; */
                width: 100%;
                /* display: none !important; */
            }

            .banner-carousel .owl-nav button.owl-prev,
            .banner-carousel .owl-nav button.owl-next,
            .banner-carousel .owl-nav button.owl-prev:hover,
            .banner-carousel .owl-nav button.owl-next:hover {
                position: absolute;
                background: #00000063;
                color: #fff;
                width: 45px;
                height: 45px;
                font-size: 25px;
                margin: 0;
            }

            .banner-carousel .owl-nav button.owl-prev:hover,
            .banner-carousel .owl-nav button.owl-next:hover {
                background: #000000d4;
            }

            .banner-carousel .owl-nav button.owl-prev {
                left: 0;
                top: 32%;
            }

            .banner-carousel .owl-nav button.owl-next {
                left: 0;
                top: calc(32% + 46px);
            }

            .banner-carousel .owl-nav .owl-prev span,
            .banner-carousel .owl-nav .owl-next span {
                color: #fff;
            }

            @media(min-width: 768px) {

                .banner-carousel.owl-carousel .owl-stage,
                .banner-carousel.owl-carousel .slide-item {
                    height: 100%;
                    max-height: 650px;
                    display: flex;
                }
            }
            </style>


         


            <section class="pt-5 pb-5 text-center">
                <div class="container" style="padding-top:30px;">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="sc_title" style="color:#000;text-align:center;font-weight: 600;">Doctor's blogs</h2>
                            <div class="row">
                                <div class="owl-carousel blogs-carousel owl-theme">
                                    <div class="item" id="loading">
                                        <center>
                                            <a href="#"><img src="images/about-our-centers-img/ivf.jpg" alt="" class="">
                                                <p class="text-center"
                                                    style="color:#012e5d;font-weight: 600;font-size: 12px;">Over 60%
                                                    successful IVF</p>
                                            </a>
                                        </center>
                                    </div>
                                    <div class="item" id="loading">
                                        <center>
                                            <a href="#"><img src="images/about-our-centers-img/FET.jpg" alt="" class="">
                                                <p class="text-center"
                                                    style="color:#012e5d;font-weight: 600;font-size: 12px;">Over 64%
                                                    successful FET (Frozen Embryo Transfer)</p>
                                            </a>
                                        </center>
                                    </div>
                                    <div class="item" id="loading">
                                        <center>
                                            <a href="#"><img src="images/about-our-centers-img/IUI.jpg" alt="" class="">
                                                <p class="text-center"
                                                    style="color:#012e5d;font-weight: 600;font-size: 12px;">Over 18%
                                                    successful IUI</p>
                                            </a>
                                        </center>
                                    </div>
                                    <div class="item" id="loading">
                                        <center>
                                            <a href="#"><img src="images/about-our-centers-img/IVF with dono eggs.jpg"
                                                    alt="" class="">
                                                <p class="text-center"
                                                    style="color:#012e5d;font-weight: 600;font-size: 12px;">Over 60%
                                                    successful IVF with donor eggs</p>
                                            </a>
                                        </center>
                                    </div>
                                    <div class="item" id="loading">
                                        <center>
                                            <a href="#"><img src="images/about-our-centers-img/1250 families.jpg" alt=""
                                                    class="">
                                                <p class="text-center"
                                                    style="color:#012e5d;font-weight: 600;font-size: 12px;">Over 1250
                                                    happy families </p>
                                            </a>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>


            <script>
            jQuery(".blogs-carousel").owlCarousel({
                autoplay: true,
                autoplayTimeout: 12000,
                autoplayHoverPause: true,
                lazyLoad: true,
                loop: true,
                margin: 20,
                responsiveClass: true,
                nav: false,
                dots: true,
                margin: 0,
                responsive: {
                    0: {
                        items: 3
                    },

                    600: {
                        items: 3
                    },

                    1024: {
                        items: 3
                    },

                    1366: {
                        items: 3
                    }
                }
            });
            </script>

        </div>

    </div>
    @endsection



   