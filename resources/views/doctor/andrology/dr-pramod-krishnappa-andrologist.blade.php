@extends('layouts/doctor-layout')
@section('meta-title')
<title>Dr. Pramod Krishnappa |Best Andrologist & Urology Specialist |Nu fertility</title>

@endsection
@section('meta-description')
<meta name="description" content="Dr Pramod Krishnappa, is one of the best uro andrologist in Bangalore available at NU Hospitals. Providing best treatment for Male Infertility and Male Sexual Dysfunction" />

@endsection


@section('banner_image')
<div>
    <img class="img-responsive w-100 h-100" style="width:100%" src="{{asset('images/doctor/Dr-Banner-Pramod.jpg')}}"
        alt="">

</div>
@endsection

@section('left_content')
<div class="profile-card">
    <div class="blank-space"></div>
    <div class="box-shadow">
        <!-- <div class="profile-img">
                                    <center><img src="assets/images/doctors/Dr.-Pramod-Krishnappa.jpg" class="img-department"></center>
                                </div> -->
        <div class="profile-text text-center">
            <!-- <h4>Dr. Pramod Krishnappa</h4> -->
            <p class="p_dtls"><b>Consultant- Andrologist, Department of Urology</b><br></p>
            <hr>
            <p class="p_hos">NU Hospitals, Rajajinagar, Bengaluru</p>
            <!-- <a href="/book-an-appointment" id="sc_button_1055736610" class="btn btn-profile row">
                                        <div class="appointment-text">Make an Appointment</div>
                                    </a> -->

        </div>
    </div>
</div>
@endsection

@section('right_content')
<div class="wpb_column vc_column_container vc_col-sm-7 sc_layouts_column_icons_position_left">
    <div class="vc_column-inner">
        <div class="wpb_wrapper">
            <div class="wpb_text_column wpb_content_element">
                <div class="wpb_wrapper">
                    <p>
                        Dr. Pramod, an alumnus of Bangalore Medical College, is an expert in the management of Male
                        Infertility and Men's Sexual Health.<br>
                        He is the first Asian Urologist/ Andrologist to do the European Society for Sexual Medicine
                        (ESSM) Penile Implant Fellowship from Spain. He further specialized in Penile Reconstruction in
                        Belgrade and Male Infertility at the University of Illinois at Chicago (UIC).<br>
                        NU Andrology is the centre of excellence for Penile Implants and Penile Cosmetic Surgeries. He
                        is a pioneer in various surgical sperm retrieval procedures and microsurgeries.
                    </p>
                </div>
            </div>
            <div class="vc_empty_space height_medium" style="height: 32px;"><span class="vc_empty_space_inner"></span>
            </div>
            <!-- <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                    <div class="wpb_column vc_column_container vc_col-sm-9 sc_layouts_column_icons_position_left">
                                                        <div class="vc_column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div class="vc_progress_bar wpb_content_element vc_progress-bar-color-bar_grey vc_progress_bar_narrow">
                                                                    <div class="vc_general vc_single_bar">
                                                                        <small class="vc_label">Fertility Preservation <span class="vc_label_units">85%</span></small>
                                                                        <span class="vc_bar" data-percentage-value="85" data-value="85" style="background-color: #ff966b;"></span>
                                                                    </div>
                                                                    <div class="vc_general vc_single_bar">
                                                                        <small class="vc_label">Egg Freezing <span class="vc_label_units">75%</span></small>
                                                                        <span class="vc_bar" data-percentage-value="75" data-value="75" style="background-color: #ff966b;"></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> -->
            <div class="vc_empty_space height_medium" style="height: 32px;"><span class="vc_empty_space_inner"></span>
            </div>
            <div class="sc_item_button sc_button_wrap">
                <a href="/book-an-appointment" id="sc_button_1055736610"
                    class="sc_button sc_button_default sc_button_size_normal sc_button_icon_left sc_button_hover_slide_left"
                    style="background-color: #0069aa;">
                    <span class="sc_button_text"><span class="sc_button_title">Make an Appointment</span></span>
                    <!-- /.sc_button_text -->
                </a>
                <!-- /.sc_button -->
            </div>
            <!-- /.sc_item_button -->
            <div class="vc_empty_space" style="height: 32px;"><span class="vc_empty_space_inner"></span></div>
        </div>
    </div>
</div>
@endsection

@section('details')
<section class="section">
    <div class="container" style="padding-bottom:30px;">
        <div class="tab-holder">
            <a id="tab-3" class="bg-light" href="javascript:void(0);" onclick="showContent(3);">Area of Expertise</a>
            <a id="tab-1" class="bg-light active" href="javascript:void(0);" onclick="showContent(1);">Treatment /
                Procedures</a>
            <a id="tab-4" class="bg-light" href="javascript:void(0);" onclick="showContent(4);">Achievements</a>
            <a id="tab-2" class="bg-light" href="javascript:void(0);" onclick="showContent(2);">Publications</a>
            <a id="tab-6" class="bg-light" href="javascript:void(0);" onclick="showContent(6);">International
                Presentations</a>
            <a id="tab-5" class="bg-light" href="javascript:void(0);" onclick="showContent(5);">Professional
                Membership</a>
            <a id="tab-7" class="bg-light" href="javascript:void(0);" onclick="showContent(7);">Gallery</a>
            <a id="tab-10" class="bg-light" href="javascript:void(0);" onclick="showContent(10);">Blogs</a>
        </div>
        <div class="content-holder expertise-content">
            <h4>Common Andrology Conditions/Issues Treated :</h4>
            <ul>
                <li>Penile implant surgery (Inflatable and malleable)</li>
                <li>Vacuum erection device</li>
                <li>Penile intra-cavernosal injection (ICI) therapy</li>
                <li>Penile doppler scan</li>
                <li>Artificial urinary sphincter or Male Urethral sling for post-prostatectomy urinary leak</li>
                <li>Cosmetic procedures of penis: Frenuloplasty, circumcision, Penile filler injections</li>
                <li>Buried penis correction: Pubic Lipectomy, Liposuction, scrotoplasty</li>
                <li>Penile curvature corrections: Plication, Grafts, Penile Traction devices</li>
                <li>Testosterone replacement therapy: gels, Injections, Subcutaneous pellet insertion</li>
                <li>Testicular implants</li>
                <li>Vasectomy</li>
                <li>Microscopic vasectomy reversal</li>
                <li>Microscopic varicocele surgery</li>
                <li>Penile vibrator for semen collection</li>
                <li>TESA (TEsticular Sperm Aspiration)</li>
                <li>TESE (TEsticular Sperm Extraction)</li>
                <li>PESA (Percutaneous Epididymal Sperm Aspiration)</li>
                <li>Micro-TESE (Microdissection TESE)</li>
                <li>Sperm freezing/ Cryopreservation</li>
                <li>Onco-fertility</li>
                <li>Premarital fertility workup</li>
            </ul>
        </div>
        <div class="content-holder professinal-content">
            <h4>Publications :</h4>
            <ul>
                <h5><strong>Journals (Krishnappa P) :</strong></h5>
                <li>Surgical Outcomes and Patient Satisfaction With the Low-Cost, Semi-Rigid Shah Penile Prosthesis: A
                    boon to the Developing Countries. Sexual Medicine 2021 https://pubmed.ncbi.nlm.nih.gov/34274823/
                </li>
                <li>Risk Factors For Revision After Artificial Urinary Sphincter Implantation In Male Patients With
                    Stress Urinary Incontinence: A Ten-Year Retrospective Study. International Neurourology Journal
                    (INJ) 2021</li>
                <li>Cadaveric Penile Prosthesis Workshop training improves surgical confidence levels of urologists:
                    South Asian Society for Sexual Medicine course survey. International Journal of Urology 2020.
                    https://pubmed.ncbi.nlm.nih.gov/32776406/</li>
                <li>Home modeling after penile prosthesis implantation in the management of residual curvature in
                    Peyronie's disease. International Journal of Impotence Research 2020.
                    https://pubmed.ncbi.nlm.nih.gov/32641777/</li>
                <li>Vacuum Physiotherapy after First Stage Buccal Mucosa Graft (BMG) Urethroplasty in Children with
                    Proximal Hypospadias. International Brazilian Journal of Urology 2020.
                    https://pubmed.ncbi.nlm.nih.gov/32822133/</li>
                <li>Intraoperative and postoperative complications of penile implant surgery. Diagnosis and treatment.
                    Actas urologicas espan~olas 2020. https://pubmed.ncbi.nlm.nih.gov/32532509/</li>
                <li>Surgical management of Peyronie's Disease with co-existent Erectile Dysfunction. Sexual Medicine
                    2019. https://www.ncbi.nlm.nih.gov/pubmed/31540882</li>
                <li>Prevalence, assessment and surgical correction of penile curvature in hypospadias patients treated
                    at one European Referral Center: Description of the technique and surgical outcomes. World Journal
                    of Urology 2020. https://www.ncbi.nlm.nih.gov/pubmed/31654219</li>
                <li>Sildenafil/Viagra in the treatment of premature ejaculation. International Journal of Impotence
                    Research 2019. https://www.ncbi.nlm.nih.gov/pubmed/30837718</li>
                <li>Complications Of Robot Assisted Radical Prostatectomy. Asociacion Espanola de Urologia 2019.
                    https://www.ncbi.nlm.nih.gov/pubmed/30945653</li>
                <li>Penile traction therapy with the new device 'Penimaster PRO' is effective and safe in the stable
                    phase of Peyronie's disease: a controlled multicenter study. British Journal of Urology
                    International 2019. https://www.ncbi.nlm.nih.gov/pubmed/30365247</li>
                <li>Re: A Prospective Randomised Placebo-controlled Study of the Impact of Dutasteride/Tamsulosin
                    Combination Therapy on Sexual Function Domains in Sexually Active Men with Lower Urinary Tract
                    Symptoms (LUTS) Secondary to Benign Prostatic Hyperplasia (BPH). European Urology 2019.
                    https://www.ncbi.nlm.nih.gov/pubmed/30391083</li>
                <li>Corporeal penile curvature (CPC) and surgical complications in hypospadias repairs: Associations and
                    outcomes. European Urology Supplement 2019. https://doi.org/10.1016/S1569-9056(19)30552-4</li>
                <li>Malignant refractory priapism: An urologist's nightmare. Urology Annals 2019.
                    https://www.ncbi.nlm.nih.gov/pubmed/31040614</li>
                <li>The effect of annual hospital volume on perioperative outcomes after urethroplasty. European Urology
                    Supplement 2019. https://doi.org/10.1016/S1569-9056(19)30380-X</li>
                <li>A Prospective Study to Evaluate the Efficacy of Botulinum Toxin-A in the Management of Dysfunctional
                    Voiding in Women. Clinical Medicine Insights: Women's Health 2018.
                    https://doi.org/10.1177/1179562X18811340</li>
                <li>Dihydroxyadenine stone with adenine phosphoribosyltransferase deficiency. Indian Journal of Urology
                    2017. https://www.ncbi.nlm.nih.gov/pubmed/28717278</li>
                <li>Supracostal percutaneous nephrolithotomy: A prospective comparative study. Indian Journal of Urology
                    2016. https://www.ncbi.nlm.nih.gov/pubmed/26941494</li>
                <li>Scrotal calcinosis: A rare entity. Australian and New Zealand Journal of Surgery 2011.
                    https://www.ncbi.nlm.nih.gov/pubmed/21342405</li>
                <li>Unproved tuberculous lesion of penis: A rare cause of saxophone penis treated by a therapeutic trial
                    of anti-tubercular therapy. Indian Journal of Medical Sciences.
                    https://www.ncbi.nlm.nih.gov/pubmed/23250292</li>

                <h5><strong>Textbook :</strong></h5>
                <h4>Assistant Editor</h4>
                <li>ESSM Textbook of Urogenital Prosthetic Surgery. Madrid- Spain: Editorial Medica Panamericana (2020).
                    ISBN: 978-84-9110-699-9</li>

                <h4>Chapters Authored</h4>
                <li>Chapter 1: Patient and Device Selection. Penile Implant Surgery - Contemporary Challenges and
                    Controversies. 2021 (ISBN: 978-3-030-82362-7)</li>
                <li>Chapter 15: Penile Implants. Peyronie's Disease: Pathophysiology and Treatment. (ISBN:
                    978-0-12-819468-3) https://doi.org/10.1016/B978-0-12-819468-3.00015-X</li>
                <li>Chapter 8: Erectile restoration (Inflatable Penile Prosthesis Placement)- basic scrotal and
                    infrapubic techniques. Textbook of Urogenital Prosthetic Surgery: Erectile Restoration &amp; Urinary
                    Incontinence (ISBN: 978-84-9110-699-9)</li>
                <li>Chapter 13: Inflatable Penile Prosthesis &amp; corporal fibrosis. Textbook of Urogenital Prosthetic
                    Surgery: Erectile Restoration &amp; Urinary Incontinence (ISBN: 978-84-9110-699-9)</li>
                <li>Chapter 14: Inflatable Penile Prosthesis and Peyronie's Disease. Textbook of Urogenital Prosthetic
                    Surgery: Erectile Restoration &amp; Urinary Incontinence (ISBN: 978-84-9110-699-9)</li>
            </ul>
        </div>
        <div class="content-holder publication-content">
            <h4>Area of Expertise :</h4>
            <ul>
                <h5><strong>Male Fertility</strong></h5>
                <li>Primary / Secondary male infertility</li>
                <li>Semen / Sperm issues: Azoospermia (Obstructive and Non-obstructive), Oligo-astheno-teratospermia
                    (OAT)</li>
                <li>Varicocele</li>
                <li>Klinefelter syndrome</li>
            </ul>
            <ul>
                <h5><strong>Male Sexual Dysfunction:</strong></h5>
                <li>Erectile dysfunction (ED)</li>
                <li>Unconsummated marriage</li>
                <li>Premature (early) ejaculation</li>
                <li>Anejaculation (Inability to ejaculate)</li>
                <li>Hypogonadism (Low testosterone hormone)</li>
                <li>Peyronies disease (Curved penis with hard plaque in penis)</li>
                <li>Congenital penile curvature (without hard plaque)</li>
                <li>Penile fracture</li>
                <li>Small Penis</li>
            </ul>
        </div>
        <div class="content-holder awards-content">
            <h4>Achievements :</h4>
            <ul>
                <li>Urological Association of Asia (UAA) Youth Section Fellowship 2021</li>
                <li>European School of Urology (ESU) courses on Office Management Of Male Sexual Dysfunction and The
                    Infertile Couple - Urological aspects- London (UK)</li>
                <li>European Society of Sexual Medicine (ESSM) Penile Implant Fellowship Madrid (Spain)</li>
                <li>European Urology Scholarship Program for Penile Reconstruction and Gender Reassignment Surgery
                    Belgrade (Serbia)</li>
                <li>International Society of Sexual Medicine (ISSM) Scholarship to attend the ESSM School of Sexual
                    Medicine- Budapest (Hungary)</li>
                <li>Male Infertility (Andrology) Visiting Fellow in University Of Illinois at Chicago (UIC), USA</li>
                <li>"Manipal Best Published Paper" award for Penimaster PRO Penile traction therapy at Karnataka Urology
                    Association KUACON 2019 at Ballari, Karnataka</li>
                <li>Received "The Edinburgh Surgery Online Global Scholarship 2019" from the University of Edinburgh, UK
                </li>
                <li>Millennium International Urology Conference(MIUC) Travelling Fellowship 2020 awarded by Association
                    of Southern Urologists(ASU) - SZUSI</li>
                <li>European Urology Residents Education Program EUREP 2016 at Prague, Czech Republic</li>
                <li>Committee Member: Subspecialty cell (Andrology &amp; Men's Health) of Urological Society of India
                    (USI) 2020-2022.</li>
                <li>Panel member: USI "Male Infertility" guidelines (published in 2020)</li>
            </ul>
        </div>
        <div class="content-holder surgical-content">
            <h4>Professional Memebership :</h4>
            <ul>
                <li>American Urological Association (AUA)</li>
                <li>International Society for Sexual Medicine (ISSM)</li>
                <li>South Asian Society for Sexual Medicine (SASSM)</li>
                <li>Urological Association of Asia (UAA)</li>
                <li>Indian Medical Association</li>
                <li>Urological Society of India (USI)</li>
                <li>Indian Fertility Society</li>
                <li>Indian Society of Assisted Reproduction</li>
                <li>Association of Southern Urologists - India</li>
                <li>Karnataka Urology Association (KUA)</li>
                <li>Bangalore Urology Society (BUS)</li>
                <li>Association of Medical Consultants (AMC) Bangalore</li>
                <li>Association of National Board Accredited Institutions (ANBAI) Alumni</li>
            </ul>
        </div>
        <div class="content-holder presentation-content">
            <h4>International Presentations :</h4>
            <ul>
                <li>"A systematic review of modeling in Peyronie's disease" (Abstract 226) at the Virtual World Meeting
                    on Sexual Medicine (WMSM) 2021, Japan</li>
                <li>"Cadaveric Penile Prosthesis Workshop training improves surgical confidence levels of Urologists:
                    South Asian Society of Sexual Medicine Course Survey" (UOP-1103) on October 15- 17, 2020 at
                    Urological Association of Asia (UAA) Virtual Annual Congress 2020, Seoul, South Korea</li>
                <li>Home-modeling after Penile Prosthesis Implantation is a viable option for the management of residual
                    curvature in Peyronie's disease (Abstract ID: 19-6476) on 5th May 2019 at American Urological
                    Association (AUA) Annual Congress in Chicago, USA.</li>
                <li>Role of Retrograde Intrarenal Surgery in the management of Urinary Tract Infection complicated by
                    Renal Papillary Necrosis (Abstract C-EPID-5339) on 22/07/2016 at Urological Association of Asia
                    (UAA) Annual Congress 2016 in Singapore.</li>
            </ul>
        </div>
        <div class="content-holder blog-content">
            <div class="row">
                <div class="col-md-12 url_div_mbl" style="padding-top:20px;">
                    <ul class="url-list url_list_mbl">
                        <li>

                            <a class="pos_rel"
                                href="https://www.mensxp.com/health/sexual-health/87072-7-facts-about-masturabation.html">
                                <img class="blog_img"
                                    src="https://img.mensxp.com/media/content/2021/Apr/Masturbation-Facts1400_606aa7b668f2e.jpeg">
                                <div class="pos_text">
                                    <p class="pos_rel_50"> 7 Questions About Masturbation You May Have Had But Always
                                        Felt Too Embarrassed To Ask </p>
                                </div>

                            </a>
                        </li>
                        <li>
                            <a
                                href="https://www.timesnownews.com/health/article/struggling-to-have-an-intimate-relationship-here-is-your-guide-to-understanding-erectile-dysfunction/730145">
                                <img class="blog_img"
                                    src="https://imgk.timesnownews.com/story/iStock-1072755960_0.jpg?tr=w-600,h-450,fo-auto"
                                    alt="">
                                <div class="pos_text">
                                    <p class="pos_rel_50">Struggling to have an intimate relationship? Here is your
                                        guide to understanding Erectile Dysfunction </p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://www.outlookindia.com/website/story/opinion-busting-myths-around-erectile-dysfunction/374589">
                                <img class="blog_img"
                                    src="https://images.outlookindia.com/public/uploads/articles/2021/2/23/medicines_570_850.jpg"
                                    alt="">
                                <div class="pos_text">
                                    <p class="pos_rel_50">Busting Myths Around Erectile Dysfunction </p>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.nuhospitals.com/blog/falling-sperm-count-a-cause-for-introspection/">
                                <img class="blog_img"
                                    src="https://www.nuhospitals.com/blog/wp-content/uploads/2021/04/Falling-Sperm-Count-800x465.jpg"
                                    alt="">
                                <div class="pos_text">
                                    <p class="pos_rel_50">Falling Sperm Count – A Cause for Introspection </p>
                                </div>
                            </a>
                        </li>
                        <li><a href="https://www.nufertility.com/blog/erectile-dysfunction/">
                                <img class="blog_img"
                                    src="https://www.nufertility.com/blog/wp-content/uploads/2018/11/SW6-1140x589.jpg"
                                    alt="">
                                <div class="pos_text">
                                    <p class="pos_rel_50">ERECTILE DYSFUNCTION </p>
                                </div>
                            </a></li>
                        <li><a href="https://www.nufertility.com/blog/male-infertility/">
                                <img class="blog_img"
                                    src="https://www.nufertility.com/blog/wp-content/uploads/2020/08/Male-Infertility-Depression-and-Treatment_822-x-465.jpg"
                                    alt="">
                                <div class="pos_text">
                                    <p class="pos_rel_50">Male Infertility Archives</p>
                                </div>
                            </a></li>
                        <li><a href="https://ianslife.in/food-fitness/could-covid-19-cause-erectile-dysfunction-men">
                                <img class="blog_img"
                                    src="https://ianslife.in/sites/default/files/styles/detail_page/public/2021-05/pexels-photo-3756619.jpeg"
                                    alt="">
                                <div class="pos_text">
                                    <p class="pos_rel_50">Could COVID-19 cause erectile dysfunction in men? </p>
                                </div>
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content-holder gallery-content">
            <h4> Professional awards and Special recognitions:</h4>
            <div class="row">
                <div class="col-md-4 img-wrap">
                    <img class="img-responsive w-100"
                        src="https://www.nuhospitals.com/images/specialities/andrology/31.jpg" alt="">
                    <p>Dr Pramod, as a visiting Andrology Fellow with Prof Dr Craig Niederberger at the University of
                        Illinois at Chicago (UIC), USA.</p>
                </div>
                <div class="col-md-4 img-wrap">
                    <img class="img-responsive w-100"
                        src="https://www.nuhospitals.com/images/specialities/andrology/32.jpg" alt="">
                    <p>Dr Pramod with Dr Rados Djinovic at Sava Perovic Foundation, Belgrade (Serbia) during European
                        Urology Scholarship Program (EUSP) in Penile Reconstruction.</p>
                </div>
                <div class="col-md-4 img-wrap">
                    <img class="img-responsive w-100"
                        src="https://www.nuhospitals.com/images/specialities/andrology/33.jpg" alt="">
                    <p>Dr Pramod during his European Society of Sexual Medicine (ESSM) Penile Implant Fellowship in
                        Madrid (Spain). Left to right: Dr Enrique Lledo-Garcia, Dr Ignacio Moncada, Dr Juan Ignacio
                        Martinez-Salamanca and Dr Pramod.</p>
                </div>
                <div class="col-md-4 img-wrap">
                    <img class="img-responsive w-100"
                        src="https://www.nuhospitals.com/images/specialities/andrology/35.jpg" alt="">
                    <p>Dr Pramod received the International Society of Sexual Medicine ( ISSM) Scholarship to attend the
                        ESSM School of Sexual Medicine 2018 in Budapest ( Hungary).</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('banner_carosal')

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100 " src="images/doctor/test-bg.jpg" alt="First slide">
            <div class="carousel-caption">
                <h5>Happy Families About Dr. Pramod Krishnappa</h5>
                <p>TESTIMONIALS</p><br>
                <div class="sc_testimonials_item">
                    <div class="sc_testimonials_item_content">

                    <p>Dr Pramod is very approachable. Explains everything in detail and responds to all the queries to the point.</p>
                                                               

                    </div>

                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/doctor/test-bg.jpg" alt="Second slide">
            <div class="carousel-caption">
                <h5>Happy Families About Dr. Pramod Krishnappa</h5>
                <p>TESTIMONIALS</p><br>
                <div class="sc_testimonials_item">
                    <div class="sc_testimonials_item_content">
                    <p>I have started my treatment with Dr Pramod after a span of two years. I liked the way my problems are heard and diagnosed. I have total faith in the doctor and his treatment.</p>
                                                            


                    </div>

                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/doctor/test-bg.jpg" alt="Third slide">
            <div class="carousel-caption">
                <h5>Happy Families About Dr. Pramod Krishnappa</h5>
                <p>TESTIMONIALS</p><br>
                <div class="sc_testimonials_item">
                    <div class="sc_testimonials_item_content">
                    <p>I am Recommended to my friends also...Dr Pramod is Explaining clearly and clear all my doubts.</p>
                                                           
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>


@endsection

@section('doctors_video')

    <section class="doctors_video">
        <div class="data-vc-full-width=" true" data-vc-full-width-init="true"
            class="vc_row wpb_row vc_row-fluid vc_custom_1480401432866 vc_row-has-fill"
            style="position: relative;; box-sizing: border-box;">
            <h2 class="sc_title" style="color:#000;text-align:center;font-weight: 600;">Doctor's Video</h2>
            <div class="row">
                <div class="col-md-6" style="padding-top:20px;">
                    <a class="mediabox" href="https://www.youtube.com/embed/3t7pbFy9N3w">
                        <img class="img-fluid" src="/images/video-thumbnail.png" style="cursor:pointer" />
                    </a>
                    <p class="text-center video-title">Dr. Pramod Krishnappa - Consultant Andrologist talking about
                        Penile Implants</p>
                </div>
                <div class="col-md-6" style="padding-top:20px;">
                    <a class="mediabox" href="https://www.youtube.com/embed/O3dbmsX3hJs">
                        <img class="img-fluid"  src="/images/video-thumbnail.png" style="cursor:pointer" />
                    </a>
                    <p class="text-center video-title">Varicocele in Male Fertility | Dr Pramod Krishnappa, Consultant
                        Andrologist</p>
                    </div>
            
            </div>
            </div>

    </section>


@endsection