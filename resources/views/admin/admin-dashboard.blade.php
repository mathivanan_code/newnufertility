@extends('layouts/main')

@section('content')

<style>
.form-field-style{
    padding:20px 0px;
}
    
</style>

<div class="content container ">
<div class="row" style="padding:50px 10px;">
<div class="col-lg-3"></div>
<div class="col-lg-9">
            <form method="POST" class="owos-form" action="/blogpost" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        @if($mode == 'create')
                        <input type="hidden" id="blog_mode"  name="blog_mode" value="{{$mode}}">
                       @else
                        <input type="hidden" id="blog_mode" name="blog_mode" value="{{$mode}}">
                        <input type="hidden" id="blog_id" name="blog_id" value="{{$blog_id}}">
                        @endif
                        <div class="row form-field-style">
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <label for="blog_title">Blog Title</label>
                                @if($mode == 'create')
                                <input type="text" class="title form-control" id="blog_title" name="blog_title">
                                @else
                                <input type="text" class="title form-control" id="blog_title" name="blog_title" value="{{$blogs->title}}">
                                @endif
                                @if ($errors->has('blog_title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('blog_title') }}</strong>
                                    </span>
								@endif
                            </div>
                        </div>
                        <div class="row form-field-style">
                            <div class="col-md-8 col-sm-12 col-xs-12 left-form">
                                <label for="blog_subtitle">Blog Subtitle</label>
                                @if($mode == 'create')
                                <input type="text" class="subtitle form-control" id="blog_subtitle" name="blog_subtitle">   
                                @else
                                <input type="text" class="subtitle form-control" id="blog_subtitle" name="blog_subtitle" value="{{$blogs->subtitle}}">   
                                @endif
                                @if ($errors->has('blog_subtitle'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('blog_subtitle') }}</strong>
                                    </span>
								@endif
                            </div>
                        </div>
                        <div class="row form-field-style">
                            <div class="col-md-8 col-sm-12 col-xs-12 left-form">
                                <label for="blog_subtitle">Blog Url</label>
                                @if($mode == 'create')
                               <Span>blog/</Span> <input type="text" class="blog_url form-control" id="blog_url" name="blog_url">   
                                @else
                                <input type="text" class="blog_url form-control" id="blog_url" name="blog_url" value="{{str_replace("blog/","",$blogs->url)}}"> 
                                @endif
                                <div><span>url prefix with blog/</span></div>
                                @if ($errors->has('blog_url'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('blog_url') }}</strong>
                                    </span>
								@endif
                            </div>
                        </div>
                        <div class="row form-field-style">
                            <div class="col-md-8 col-sm-12 col-xs-12 left-form">
                                <label for="blog_subtitle">Blog Category</label>
                                <select class="form-control" name="category" id="category">
                                    <option value="none">Select Category</option>
                                    @foreach($categories as $key => $value)
                                        @if($mode == "edit")
                                        <option value="{{ $key }}" @if($blogs->category == $key) selected @endif>{{ $value }}</option>
                                        @else
                                        <option value="{{ $key }}">{{ $value }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row form-field-style">
                            <div class="col-md-8 col-sm-12 col-xs-12 left-form">
                                <label for="blog_subtitle">Add to Recent Post</label>
                                <select class="form-control" name="programme_list" id="programme_list" onchange="fGetEmailGroups(this.value)">
                                   @if($mode == 'edit')
                                   <option value="no" @if($mode == 'edit' && $blogs->sticky == 'no') selected @endif >NO</option>
                                    <option value="yes" @if($mode == 'edit' && $blogs->sticky  == 'no') selected @endif  >Yes</option>
                                    @else
                                    <option value="no">NO</option>
                                    <option value="yes">Yes</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                       
                        <div class="row form-field-style">
                            <div class="col-md-8 col-sm-12 col-xs-12 left-form">
                                <label for="blog_image">Banner Image</label>
                                @if($mode == 'create')


                                <input type="file" id="blog_image"  class="form-control" name="blog_image" accept="image/*">        </div>                    
                                @else


                                <input type="file" id="blog_image" class="form-control" name="blog_image" accept="image/*">    </div>
                                 <div>
                                <img style="position:relative;top:20px;left:20px;   max-width: 50%;" class="img-fluid" src="{{asset('images/blog/'.$blogs->images)}}" alt="">
                                </div>
                                @endif
                                @if ($errors->has('blog_image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('blog_image') }}</strong>
                                    </span>
								@endif
                            </div>
                        <div>


                        <div class="row form-field-style">
                            <div class="col-md-8 col-sm-12 col-xs-12 left-form">
                                <label for="tinymce">Blog Description</label>
                                @if($mode == 'create')
                                <input type="text" class="description form-control" id="tinymce" name="blog_description">
                                @else
                                <input type="text" class="description form-control" id="tinymce" name="blog_description" value="{{$blogs->description}}">
                                @endif
                                @if ($errors->has('blog_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('blog_description') }}</strong>
                                    </span>
								@endif
                                
                            </div>
                        </div>

                        <div class="row form-field-style">
                            <div class="col-md-8 col-sm-12 col-xs-12 left-form">
                                <label for="tinymce">Blog Meta Title <span>(If title is not mention it will take from blog title)</span></label>
                                @if($mode == 'create')
                                <input type="text" class="description form-control" name="blog_meta_title">
                                @else
                                <input type="text" class="description form-control"  name="blog_meta_title" value="{{$blogs->meta_title}}">
                                @endif
                                @if ($errors->has('blog_meta_title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('blog_meta_title') }}</strong>
                                    </span>
								@endif
                                
                            </div>
                        </div>
                        <div class="row form-field-style">
                            <div class="col-md-8 col-sm-12 col-xs-12 left-form">
                                <label for="tinymce">Blog Meta Description</label>
                                @if($mode == 'create')
                                <input type="text" class="description form-control" name="blog_meta_desc">
                                @else
                                <input type="text" class="description form-control"  name="blog_meta_desc" value="{{$blogs->meta_description}}">
                                @endif
                                @if ($errors->has('blog_meta_desc'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('blog_meta_desc') }}</strong>
                                    </span>
								@endif
                                
                            </div>
                        </div>

                        <div>
                            <div class="col-md-8 col-sm-12 col-xs-12 left-form">
                            @if($mode == 'create')
                            <input type="submit"    class="btn btn-danger btn-rounded btn-block" style="color: #ffffff !important; background-color: #f6a2de; border: solid 1px #f6a2de;" value="Create Blog">
                            @else
                            <input type="submit"  class="btn btn-danger btn-rounded btn-block"  style="color: #ffffff !important; background-color: #f6a2de; border: solid 1px #f6a2de;"  value="Save Blog">
                            @endif
                            </div>
                        </div>
                       
                    </form>
                    </div>
</div>
</div>

@endsection



