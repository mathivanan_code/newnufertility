@extends('layouts.main')
@section('content')
<br /><br />
<table width="100%">
    <tr>
        <td width="40%"></td>
        <td  width="20%">
            <form role="form" method="POST" action="{{ url('admin/login') }}" autocomplete="off">

                {!! csrf_field() !!}

                <div class="form-group{!! $errors->has('email') ? ' has-error' : '' !!}">
                    <div class="col-md-4">
                        <label>Email</label>
                    </div>
                    <div class="col-md-12">
                        <input type="email" name="email" id="email" placeholder="Email" class="form-control" />

                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-md-4">
                        <label>Password</label>
                    </div>
                    <div class="col-md-12">
                        <input type="password" name="password" class="form-control" placeholder="Password">

                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <button type="submit">Login</button>
                    </div><!-- /.col -->
                </div>
            </form>
        </td>
        <td width="40%"></td>
    </tr>
</table>



@endsection
