@extends('layouts.mains')
@section('content')
<br /><br />
<table width="100%">
    <tr>
        <td width="40%"></td>
        <td width="20%">
            <form role="form" method="POST" action="{{ url('admin/change-password') }}" autocomplete="off">

                {!! csrf_field() !!}
                <div class="form-group row">
                    @foreach ($errors->all() as $error)
                    <p class="text-danger">{{ $error }}</p>
                    @endforeach
                </div>
                <div class="form-group row">
                    <label for="password">Current Password *</label>
                    <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                </div>

                <div class="form-group row">
                    <label for="password">New Password *</label>
                    <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                </div>

                <div class="form-group row">
                    <label for="password">Confirm New Password *</label>
                    <input id="new_confirm_password" type="password" class="form-control" name="confirm_new_password" autocomplete="current-password">
                </div>

                <div class="form-group row mb-0">
                    <button type="submit">
                        Update Password
                    </button>
                </div>
                <br/><br/>
            </form>
        </td>
        <td width="40%"></td>
    </tr>
</table>
@endsection