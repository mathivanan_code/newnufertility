@extends('layouts.main')
@section('content')
<style>
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}
ul.d_menu {
  list-style-type: none;
  margin: 0;
  padding: 0;
  float:right;
}

.d_menu li {
  display: inline;
}
/* #stable_id_filter{
  position:fixed;
  right: 0px;
} */
</style>
<table  class="display" width="100%">
    <thead>
        <tr>
            <td>Name</td>
            <td>value</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>FirstName</td>
            <td>{{$t_dtl->first_name }}</td>
        </tr>    
        <tr>
            <td>LastName</td>
            <td>{{$t_dtl->last_name }}</td>
        </tr>
        <tr>
            <td>email</td>
            <td>{{$t_dtl->email }}</td>
        </tr>
        <tr>
            <td>Mobile</td>	 
            <td>{{$t_dtl->mobile }}</td>
        </tr>
        <tr>
            <td>Amount</td>
            <td>{{$t_dtl->amount }}</td>
        </tr>
        <tr>
            <td>Currency</td>
            <td>{{$t_dtl->currency }}</td>
        </tr>
        <tr>
            <td>Transaction Status</td>
            <td>{{$t_dtl->transaction_status }}</td>
        </tr>
       
        <tr>
            <td>Pan Card</td>
            <td>{{$t_dtl->pan_card }}</td>
        </tr>  
        <tr>
            <td>Cheque Details</td>
            <td>{{$t_dtl->cheque_details }}</td>
        </tr>
       <tr>
           <td>certificate Name</td>
           <td>{{$t_dtl->certificate_name }}</td>
       </tr>
       <tr>
           <td>Personal Message</td>
           <td>{{$t_dtl->personal_message }}</td>
       </tr>
       <tr>
           <td>Receipt Date</td>
           <td>{{$t_dtl->receipt_date }}</td>
       </tr>
       <tr>
            <td>Receipt Number</td>
            <td>{{$t_dtl->receipt_number }}</td>
       </tr>
       <tr>
           <td>Adderess</td>
           <td>{{$t_dtl->address }}</td>
       </tr>
       <tr>
           <td>Donar Type</td>
           <td>{{$t_dtl->donar_type }}</td>   
       </tr>
       <tr>
           <td>
               Donar Currency
           </td>
           <td>{{$t_dtl->donar_currency }}</td>
       </tr>
       <tr>
           <td>
               Other
           </td>
           <td>{{$t_dtl->other }}</td>
       </tr>
       <tr>
           <td>Gateway Response Code</td>
           <td>{{$t_dtl->gateway_responce_code }}</td>
       </tr>
       <tr>
           <td>Gateway Message</td>
           <td>{{$t_dtl->gateway_message }}</td>
       </tr>
       <tr>
           <td>Gateway Transaction Id</td>
           <td>{{$t_dtl->gateway_transaction_id }}</td>
       </tr>
       <tr>
           <td>Gateway Auth Code</td>
           <td>{{$t_dtl->gateway_auth_code }}</td>
       </tr>
       <tr>
           <td>Bank Reference Number</td>
           <td>{{$t_dtl->bank_reference_number }}</td>
       </tr>
       <tr>
           <td>Order id</td>
           <td>{{$t_dtl->order_id }}</td>
       </tr>
        <tr>
            <td>Tracking Id</td>
            <td>{{$t_dtl->trackingId }}</td>
        </tr>
        <tr>
            <td>
                Transaction Date
            </td>
            <td>{{$t_dtl->transaction_date_time }}</td>
        </tr>
        <tr>
            <td>Transaction Ip address</td>
            <td>{{$t_dtl->transaction_ip_adderess }}</td>
        </tr>
        <tr>
            <td>Created</td>
            <td>{{$t_dtl->created_at }}</td>
        </tr>
        </tbody>
   
</table>

@endsection