@extends('layouts/main')

@section('content')
<div class="content container">
    <div style="padding-top:50px;padding-bottom:20px;">
<a href="/blogeadd">Add Blog</a>
  </div>
<table class="table table-hover" >
    <thead>
        <tr>
            <th>Title</th>
            <!-- <th>Url</th> -->
            <th>Category</th>
            <th>Options</th>
        </tr>
    </thead>
    <tbody>
    @foreach($blogs as  $value)
        <tr>
            <td>{{$value->title}}</td>
            <!-- <td>{{$value->url}}</td> -->
            <td>{{$value->category}}</td>
            <td>
                <span> <a href="{{ url('/'.$value->url)}}">View</a> </span>                
                <span> <a href="{{ url('/blogedit/'.$value->id)}}">Edit</a></span>                
                 <a href="{{ url('/blogdelete/'.$value->id)}}" onclick="return confirm('{{ "Are you sure " }}')">Delete</a>                
            </td>
        </tr>

    @endforeach

    </tbody>
</table>



</div>

@endsection



