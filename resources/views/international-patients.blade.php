@extends('layouts/main')

@section('meta-title')
<title>International-patients |Best Andrologist & Urology Specialist |Nu fertility</title>

@endsection
@section('meta-description')
<meta name="description" content="NU Fertility is built on lasting value systems that focuses on excellence, expertise, empathy and innovation. We thrive to deliver exceptional care and comprehensive treatment choices across a number of clinical specialties" />

@endsection


@section('content')
<style>
.ip-banner{
background-image: url(images/international-service-banner.jpg) !important;
background-repeat: no-repeat;
background-size: 100% 100%;
background-size: cover;
margin: 0 auto;
height:400px;
}



.our-support-content button {
width: 100%;
padding: 0;
margin-top: 12px;
font-weight: 600;
color: #48aff0 !important;
border:none;
}
.our-support-content button:focus {
outline: none;

}
.request-default{margin-top:70px;}
.request-default h2{
line-height:1.2rem;
font-size: 30px !important;
font-family: poppins !important;
letter-spacing: .05rem !important;
}
.request-title + .request-sub-title{
    margin-top: 1.15em;
}
.request-default h6{
    color:#000;font-family:Poppins, sans-serif;
    font-size: 14px;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 0.2px;
    line-height: 1.5em;
}
.request-sub-title + .request-form-default{margin-top:4.1rem;}
textarea{overflow: auto;
    vertical-align: top;font:inherit;  font-weight: 700;width:100%;
    min-height: 13.5em;}
    textarea::-webkit-input-placeholder {
            color: #000;font-weight:700;
    }
    textarea:-moz-placeholder { /* Upto Firefox 18, Deprecated in Firefox 19  */
            color: #000;font-weight:700;
    }
    textarea::-moz-placeholder {  /* Firefox 19+ */
            color: #000;font-weight:700;  
    }
    textarea:-ms-input-placeholder {  
       color: #000;font-weight:700;  
    }
    .request-default .request-form-default input[type="text"]:hover,
     .request-default .request-form-default input[type="password"]:hover,
      .request-default .request-form-default input[type="email"]:hover,
       .request-default .request-form-default input[type="number"]:hover,
        .request-default .request-form-default input[type="tel"]:hover,
         .request-default .request-form-default input[type="search"]:hover,
          .request-default .request-form-default textarea:hover
          {border-color:#000;}

          .request-default .request-form-default input[type="text"]:focus,
     .request-default .request-form-default input[type="password"]:focus,
      .request-default .request-form-default input[type="email"]:focus,
       .request-default .request-form-default input[type="number"]:focus,
        .request-default .request-form-default input[type="tel"]:focus,
         .request-default .request-form-default input[type="search"]:focus,
          .request-default .request-form-default textarea:focus
          {border:1px solid #3eb8d7;}

    button, input, select, textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;

}

.request-default .request-form-default {color:#000000;}
.request-default .request-form-default .form_field {    margin-bottom: 1.3333em;}
input[type="text"], input[type="number"],
 input[type="email"], input[type="tel"], 
 input[type="search"], input[type="password"],
  textarea{
    font-family: 'Poppins', sans-serif;
    font-size: 1em;
    font-weight: 400;
    font-style: normal;
    line-height: 1.2em;
    text-decoration: none;
    text-transform: none;}
    .request-default .request-form-default input[type="text"],
    .request-default .request-form-default input[type="email"],
    .request-default .request-form-default textarea{
        color: #000000;font-family:sen;width:100%;
    border-color: #e5e5e5;  
    background-color: #ffffff;      
        border: 1px solid #eee;
    font-size: 1em;
    line-height: 5.1em;
    font-style: normal;
    padding: 0 3.3em;
    height: 5.1em;
    -webkit-border-radius: 3em;
    -moz-border-radius: 3em;
    border-radius: 3em;
    max-width: 100%;
    margin-bottom: 5px;
  
}

.request-default .request-form-default .form-btn button{color: #ffffff;

    background:#00aff0;background:linear-gradient(to right, #f6a2de 50%, #00aff0 50%) no-repeat scroll right bottom / 210% 100% #00aff0;
    background-size: 200%;
    transition: .5s ease-out;
    cursor: pointer;
    display: inline-block;
    text-transform: none;
    white-space: nowrap;
    padding: 1em 3.9em 1em;
    font-size: 1.071em;
    line-height: 1em;
    font-weight: 600;
    letter-spacing: 0;
    border: none;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    -webkit-border-radius: 3em;
    -moz-border-radius: 3em;
    border-radius: 3em;
    margin-bottom:20px;
}

    .request-default .request-form-default .form-btn button:hover {
    color: #ffffff !important;
    background-position: left; }
.confirm-hover{background:linear-gradient(to right, #f6a2de 50%, #00aff0 50%) no-repeat scroll right bottom / 210% 100% #00aff0 !important;}

.request-default .request-form-default input[type="text"],
    .request-default .request-form-default input[type="email"],
    .request-default .request-form-default textarea{
        color: #000000;font-family:sen;width:100%;
    border-color: #e5e5e5;  
    background-color: #ffffff;      
        border: 1px solid #eee;
    font-size: 1em;
    line-height: 5.1em;
    font-style: normal;
    padding: 0 3.3em;
    height: 5.1em;
    -webkit-border-radius: 3em;
    -moz-border-radius: 3em;
    border-radius: 3em;
    max-width: 100%;
    margin-bottom: 5px;
  
}

</style>
<div class="ip-banner">



</div>





<div class="content our-support-content">

<div class="container" style="margin-top: 30px;">
<div class="" style="padding-bottom: 30px;">
<h1 class="faqmp" style="text-align: center; font-size: 30px;">Our Support </h1>
<p>NU Fertility is built on lasting value systems that focuses on excellence, expertise, empathy and innovation. We thrive to deliver exceptional care and comprehensive treatment choices across a number of clinical specialties </p>
</div>
<div class="row">
<div class="col-lg-1 col-md-12 col-sm-12 col-xl-1"></div>
<div class="col-lg-2 col-md-12 col-sm-12 col-xl-2">
<div class="imgg text-center">
<img class="img-fluid" src="https://www.nuhospitals.com/assets/images/international/Mask-Group-3-(1).png">
<button type="button" data-toggle="modal" data-target="#myModal_1" style="background: none; color:#000;">Accommodation</button>
</div>
</div>
<div class="col-lg-2 col-md-12 col-sm-12 col-xl-2">
<div class="imgg text-center">
<img class="img-fluid" src="https://www.nuhospitals.com/assets/images/international/Mask-Group-1-(1).png">
<button type="button" data-toggle="modal" data-target="#myModal_2" style="background: none; color:#000;">Free Pick Up</button>
</div>
</div>
<div class="col-lg-2 col-md-12 col-sm-12 col-xl-2">
<div class="imgg text-center">
<img class="img-fluid" src="https://www.nuhospitals.com/assets/images/international/Mask-Group-4-(1).png">
<button type="button" data-toggle="modal" data-target="#myModal_3" style="background: none; color:#000;">Currency Exchange</button>
</div>
</div>
<div class="col-lg-2 col-md-12 col-sm-12 col-xl-2">
<div class="imgg text-center">
<img class="img-fluid" src="https://www.nuhospitals.com/assets/images/international/Mask-Group-5.png">
<button type="button" data-toggle="modal" data-target="#myModal_4" style="background: none; color:#000;">Online Consultation</button>
</div>
</div>
<div class="col-lg-2 col-md-12 col-sm-12 col-xl-2">
<div class="imgg text-center">
<img class="img-fluid" src="https://www.nuhospitals.com/assets/images/international/Mask-Group-6.png">
<button type="button" data-toggle="modal" data-target="#myModal_5" style="background: none; color:#000;">Translator</button>
</div>
</div>
<div class="col-lg-1 col-md-12 col-sm-12 col-xl-1"></div>
</div>
</div>
<div class="container">
<div class="" style="padding-top: 40px;">
<h1 class="why-choose-us" style="text-align: center; font-size: 30px;">Why Choose NU Fertility</h1>
<img class="img-fluid" src="images/footer-desk.png">
<div class="why-choose-content">
<p>NU Hospitals, together with its fertility wing NU Fertility, has over the years, established itself as one of the leading IVF centers in India, helping many overcome their kidney and bladder-related disorders as well as putting smiles on thousands of couples’ faces with good news!



<p>After successfully treating many couples on domestic grounds, NU’s state-of-the-art fertility center has decided to spread its wings and travel across nternational waters to help couples in other countries achieve their baby dreams too. We specialise in a wide array of male as well as female infertility treatment.



<p>The fertility center at NU Hospitals, Bangalore house some of the best fertility doctors in Bangalore and are well-equipped to treat and take care of patients coming in from overseas. All you have to do is navigate through our easy-to-use website to book your appointment with your desired doctor on the desired date. From there on, leave the rest to us. If you and your partner are looking for the best IVF doctor in India then look no further than NU Hospital’s fertility branch.



<p>When it comes to the best fertility hospitals in Karnataka, NU Hospital’s fertility branches are equipped to handle even the most complex cases. Its world renowned doctors coupled with the latest in medical technology and equipment including some of the best IVF doctors in India making it your go-to hospital for all your fertility needs.</p>
</div>
</div>
</div>

</div>




<!-- Modal1 -->
<div class="modal fade" id="myModal_1" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">

<h4 class="modal-title">Accommodation</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<p>Choose from our list of serviced apartments and hotels we have tied up with on https://www.airbnb.co.in/ or https://www.makemytrip.com/ We assure you that no matter which accommodation you choose, your stay will be very comfortable. Our preferred locations include – Rajajinagar, Malleshwaram, Vijayanagar, Sadashiva Nagar and Yeshwanthpur.</p>
<ul>
<li>Hotel Parijatha Gateway, Rajajinagar</li>
<li>Comort Inn Insys , Yeswanthpur</li>
<li>RG Royal Hotel, Yeswanthpur</li>
</ul>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<!-- Modal1 -->
<div class="modal fade" id="myModal_2" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">

<h4 class="modal-title">Free pickup and drop</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<p>Once your appointment with us is confirmed – you can book your appointment via the website, you will receive more details closer to your arrival date. On arrival, our concerned person will be there for free airport pick-up. Ensure to share your arrival details with us for a smooth and seamless experience.</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<!-- Modal1 -->
<div class="modal fade" id="myModal_3" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">

<h4 class="modal-title">Currency Exchange</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<p>Forgot to change money at the airport? No problem, you can exchange your own currency notes with our dedicated FOREX (foreign currency exchange) partner.</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<!-- Modal1 -->
<div class="modal fade" id="myModal_4" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">

<h4 class="modal-title">Online Consultation</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<p>To save time and money on travel, NU Fertility even offers its international patients the option of consulting with their desired doctors from the comfort of their own home, from their own country! Other pointers include –</p>
<ul>
<li>Online consultation can’t be an alternative to a physical examination done at the hospital, hence a final opinion can only be given after a thorough physical examination</li>
<li>Online consultation is not valid for emergency cases</li>
<li>The prescription will not be provided as a final diagnosis cannot be made without physical examination</li>
<li>Before online consultation, submit your reports/medical records to care@nuhospitals.com</li>
</ul>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>
<!-- Modal1 -->
<div class="modal fade" id="myModal_5" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">

<h4 class="modal-title">Translator</h4>
<button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
<p>Last but not the least, NU has a number of multi-lingual translators who can make you feel right at home; all you have to do is inform us in advance.</p>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
</div>
</div>
</div>




<!-- </.content> -->


<div class="container  request-default">

<h2 class="request-title">Request an Appointment!</h2>
<h6 class="request-sub-title">Fill in your details below and we’ll be in touch with more information.</h6>

<div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12">

<form id="request-form-default" method="post" class="request-form-default">
<div class="row">

<div class="col-lg-6 col-md-6 col-sm-12 mb-3 mt-3 form_field">
<input type="text"   id="name" placeholder="Your name" name="name"/>
 
</div>

<div class="col-lg-6 col-md-6 col-sm-12 mb-3 mt-3 form_field">
<input type="email"   id="email" placeholder="Your e-mail" name="email"/>
 
</div>

</div>
<div class="row">

<div class="col-lg-6 col-md-6 col-sm-12 mb-3 mt-3 form_field">
<input type="text"   id="phone-number" placeholder="Your Phone Number" name="phone-number"/>
</div>

<div class="col-lg-6 col-md-6 col-sm-12 mb-3 mt-3 form_field">
<input type="text"   id="your-location" placeholder="Your Location" name="your-location"/>
</div>

</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12  mb-3 mt-3 form_field">
    <textarea  name="message" id="message" aria-required="true" placeholder="Your message"></textarea>
</div>
</div>

<div class="mb-3 mt-3 form_field form-btn">
<button class="confirm" id="confirm">Send Message</button>
</div>
</form>
</div>

<div class="col-lg-6 col-md-6 col-sm-12"></div>
</div>
</div>


@endsection