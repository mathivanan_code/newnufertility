<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	  <!--owl carousel 2-->
	  <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <!--owl carousel 2 end-->
		<link href="{{ asset('css/blog.css') }}" rel="stylesheet">
		@include('include/headtag')
		<link href="{{ asset('css/andrology.css') }}" rel="stylesheet">
		@yield('meta-title')
		@yield('meta-description')
    <meta name="csrf-token" content="{{ csrf_token() }}" />

<!-- tinymce -->
<script src="{!! url('js/tinymce/tinymce.min.js') !!}"></script>
        <script>
    tinymce.init({
      selector: '#tinymce',
      plugins:' fullpage powerpaste searchreplace   hr pagebreak  anchor toc insertdatetime  lists textcolor wordcount spellchecker  ',
    //   plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker sourcecode',
    //   toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table sourcecode',
    // toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | styleselect fontselect fontsizeselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | emoticons textpattern fullscreen',

      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Mathi',
      height : "200",
    });
  </script>
   <script type="text/javascript">
        function SP_source() {
          return "{{ url('/') }}/";
        }
        </script>

  <!-- end tinymce -->
    </head>
<body>
@include('include/header')
<div class="container-Fluid">
	<div class="container">
		<div class="row layout-top-padding">
            <div class="col-lg-8 col-md-12">@yield('content')</div>
			<div class="col-lg-4 col-md-12">	@include('include/blog')</div>
		</div>
	</div>

 </div>
@include('include/footer')
	 @yield('customjs')
</body>
</html>