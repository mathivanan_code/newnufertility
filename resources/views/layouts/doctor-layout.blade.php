<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	  <!--owl carousel 2-->
	  <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <!--owl carousel 2 end-->
		@include('include/headtag')
		<link href="{{ asset('css/doctor.css') }}" rel="stylesheet">
		@yield('meta-title')
		@yield('meta-description')
    </head>
<body>
@include('include/header')
<div class="container-Fluid">
	<div>
		@yield('banner_image')
	</div>
	<div class="container">
		<div class="row layout-top-padding">
			<div class="col-lg-6 col-md-12">
            @yield('left_content')
			</div>
			<div class="col-lg-6 col-md-12 top-padding-doctors">@yield('right_content')</div>
		
		</div>
        <div class="row">
        @yield('details')
        </div>
	</div>
	<div>
		@yield('banner_carosal')
	</div>
	<div class="container">
		@yield('doctors_video')
	</div>
	<div class="container">
		@yield('doctor_blogs')
	</div>
 </div>
@include('include/footer')
	 @yield('customjs')
</body>
</html>
<style>
	
	@media only screen and (min-width: 768px) and (max-width: 1020px){
		.col-lg-6.col-md-12.top-padding-doctors{margin-top:100px;}
	}
	
	@media only screen and (max-width: 768px){
		.col-lg-6.col-md-12.top-padding-doctors{margin-top:50px;}
	}
		</style>