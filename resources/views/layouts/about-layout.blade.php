<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	  <!--owl carousel 2-->
	  <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <!--owl carousel 2 end-->
		@include('include/headtag')
		<link href="{{ asset('css/about_us.css') }}" rel="stylesheet">
		@yield('meta-title')
		@yield('meta-description')

    </head>
<body>
@include('include/header')
<div class="container-Fluid">	
@yield('content')
 </div>
@include('include/footer')
	 @yield('customjs')
</body>
</html>