<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	  <!--owl carousel 2-->
        <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">

        <!--owl carousel 2 end-->
        <link href="{{ asset('css/admin.css') }}" rel="stylesheet">

		@include('include/headtag')
        <script src="{!! url('js/tinymce/tinymce.min.js') !!}"></script>
        <script>
    tinymce.init({
      selector: '#tinymce',
      plugins:'print preview fullpage powerpaste searchreplace autolink emoticons directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount spellchecker imagetools mediaembed  contextmenu colorpicker textpattern help code',
    //   plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker sourcecode',
    //   toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table sourcecode',
    toolbar: 'formatselect | bold italic strikethrough forecolor backcolor | styleselect fontselect fontsizeselect | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | emoticons textpattern fullscreen',

      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Mathi',
    });
  </script>
  @yield('meta-title')
		@yield('meta-description')
    </head>
<body>
@include('include/header')
<div class="container-Fluid">	
@yield('content')
 </div>
 @include('include/footer')

	 @yield('customjs')

</body>
</html>