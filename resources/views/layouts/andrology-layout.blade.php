<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	  <!--owl carousel 2-->
	  <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <!--owl carousel 2 end-->
		@include('include/headtag')
		<link href="{{ asset('css/andrology.css') }}" rel="stylesheet">
		@yield('meta-title')
		@yield('meta-description')
    </head>
<body>
@include('include/header')
<div class="container-Fluid">
	<div>
		@yield('banner_image')
	</div>
	<div class="container">
		<div class="row layout-top-padding">
			<div class="col-lg-4 col-md-12">
			@include('include/andrology')
			</div>
			<div class="col-lg-8 col-md-12">@yield('content')</div>
		
		</div>
	</div>

 </div>
@include('include/footer')
	 @yield('customjs')
</body>
</html><style>
	
	@media only screen and (min-width: 768px) and (max-width: 1020px){
		.col-lg-4.col-md-12{order:2;}
	}
		</style>