@extends('layouts/blog-layout')

@section('meta-title')
<title>{{ !empty($blog->meta_title)?$blog->meta_title:$blog->title}} | NU Fertility</title>

@endsection
@section('meta-description')
@if(!empty($blog->meta_description))
<meta name="description" content="{{$blog->meta_description}}"/>
@endif
@endsection


@section('content')
<article id="post-443">
<div class="thumbnail">
                    <img
                            class="size-full wp-image-444"
                            title="{{$blog->title}}"
                            src="{{asset('images/blog/'.$blog->images)}}"
                            alt="{{$blog->title}}"
                            width="100%" height="100%">
</div>
    <div class="padding-content text-center">
        <header class="entry-header">
            <h1 class="entry-title">
                <a href="{{'/'.$blog->url}}"
                   rel="bookmark">
                     {{$blog->title}}</a></h1>
        </header>
        <!--/.entry-header -->

        <div class="entry-meta">
            <ul class="list-inline">
                <li>
                    <span class="author vcard">
                        By <a class="url fn n" href="/">NU Fertility</a>                    </span>
                </li>

                <li>/</li>

                <li>
                    <span class="posted-on"> {{ date('d-M-y', strtotime($blog->updated_at)) }}</span>
                </li>

                <li>/</li>

                <li>
                        <span class="posted-in">
                            <a href="{{'/category/'.$blog->category}}" rel="category tag">{{$blog->category}}</a>                        </span>
                </li>


            </ul>
        </div><!-- .entry-meta -->
        
        <div class="entry-content">
            <div>
                
                {!! $blog->description !!}
            </div>
            
         </div>

         <!-- start -->

   
         <div class="swp_social_panel swp_flatFresh swp_default_full_color swp_individual_full_color swp_other_full_color scale-100 scale-fullWidth swp_one swp_three" data-position="below" data-float="bottom" data-float-mobile="bottom" data-count="0" data-floatcolor="#ffffff" style="width: 670px; left: 114px;">
                  <div class="nc_tweetContainer swp_facebook" data-network="facebook">
                    <a rel="nofollow" target="_blank" href="{{url('http://www.facebook.com/share.php?u='.$blog->url)}}" data-link="{{url('http://www.facebook.com/share.php?u='.$blog->url)}}" class="nc_tweet">
                        <span class="swp_count swp_hide" style="transition: padding 0.1s linear 0s;">
                        <span class="iconFiller"><span class="spaceManWilly">
                        <i class="fa fa-facebook-f" aria-hidden="true"></i> <span class="swp_share">Share</span></span></span></span></a></div>
                <div class="nc_tweetContainer swp_twitter" data-network="twitter">
                    <a rel="nofollow" target="_blank" href="{{url('https://twitter.com/intent/tweet?text='.$blog->title.'&url='.$blog->url)}}" data-link="{{url('https://twitter.com/intent/tweet?text='.$blog->title.'&url='.$blog->url)}}" class="nc_tweet">
                        <span class="swp_count swp_hide" style="transition: padding 0.1s linear 0s;">
                        <span class="iconFiller"><span class="spaceManWilly">
                        <i class="fa fa-twitter" aria-hidden="true"></i><span class="swp_share">Tweet</span></span></span></span></a></div>
                <div class="nc_tweetContainer swp_linkedin" data-network="linkedin">
                    <a rel="nofollow" target="_blank" href="{{url('https://www.linkedin.com/cws/share?url='.$blog->url)}}" data-link="{{url('https://www.linkedin.com/cws/share?url='.$blog->url)}}" class="nc_tweet">
                        <span class="swp_count swp_hide" style="transition: padding 0.1s linear 0s;">
                        <span class="iconFiller"><span class="spaceManWilly">
                        <i class="fa fa-linkedin" aria-hidden="true"></i><span class="swp_share">Share</span></span></span></span></a></div>
         </div>
       
         <!-- end -->
    </div>
    <div class="comment">
        <input type="text" class="blog_description_comments " id="tinymce" name="blog_description">
        <div><a class="post_comment" href="javascript:;">Post</a></div>
    </div>
    <input type="hidden" value="{{$blog->id}}" id="blog_id">
    <input type="hidden" value="{{Auth::check() ? Auth::id() : 0}}" id="user_id">
    <div class="user_comments">
        @foreach($blog_comments as $blog_comment)
        <div class="user_comments_single">
            {!! $blog_comment->description !!}
        </div>
        @endforeach
    </div>

</article>

<style>
.advertisement {
  
    z-index: 1;
  
}
.hide {
    opacity:0;
  
}
.show {
    opacity:1;
 
}
.post_comment {
    float: right;
    font-size: 21px;
    padding: 0px 20px;
    border: 1px solid grey;
    background: #12aff0;
    color: wheat;
    border-radius: 6px;
}
</style>




<div id="advertisement" class="advertisement hide">

  </div>
     
    
@endsection


