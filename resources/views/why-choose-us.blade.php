@extends('layouts/about-layout')

@section('meta-title')
<title>Why Choose Us | Best Fertility Services in Bangalore, India | NU Fertility India</title>
@endsection
@section('meta-description')
<meta name="description" content="When you are struggling with the Fertility problems. NU Fertility is the right choice to solve and to provide Best Fertility Services in Bangalore, India y with our Reproductive Medicine Specialist in Bangalore, India." />
@endsection

@section('content')

<div>
    <img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/about_us/Why-Choose-Us.jpg')}}" alt="">
</div>
<div class="container" style="margin-top: 30px;">    
          
          <p class="pjust">NU Fertility at NU Hospitals, Rajajinagar, Bangalore, India was started keeping in mind the emerging needs of the infertility and ART services. The state-of-the-art embryology laboratory is built according to the ESHRE guidelines and the quality is constantly monitored, ensuring the appropriate culture and conditions are maintained. A dedicated team of reproductive consultants, embryologists and andrologists continuously strive to give the best to the patient and we are proud to achieve a success rate on par with international standards.</p>
          <p class="pjust">We offer a wide range of infertility services like IUI/AID/IVF – ICSI/ blastocyst culture/ PICSI/ oocyte freezing/embryo freezing/semen freezing/ in-vitro maturation of oocyte/ oocyte and embryo donation/ HSG/ saline infusion sonography and fertility enhancing laparoscopic or hysteroscopic surgeries. We have also extended our services for fertility preservative methods in young cancer patients. We have a robust system of Internal Quality Assessment which captures the quality indicators from time to time and guides us on how to improve ourselves. The ambiance and the warm staff further enhance your experience and ease the stream of going through the entire process of infertility. </p>
          <h5 lass="faqmp">Why should you choose us? </h5>
          <h5 class="faqmp">
          Our Strength
          </h5>
          <ol style="margin-bottom: 25px;">
            <li><b> Expert Team:</b>We have a dedicated team of clinicians and embryologists who strive to provide the best personalized care to all infertile couples while simultaneously keeping pace with newer and advanced technologies.</li>
            <li>State of the art infrastructure - OT complex and clean room facilities for assisted reproductive techniques.</li>
            <li>Specialized male infertility evaluation and treatment options.</li>
            <li> Comfortable accommodation and ambience.</li>
            <li>Clean room IVF Laboratory with bio clad walls which are anti-microbicidal.</li>
          </ol>
          <h5 class="faqmp">
          Key Success factors
          </h5>
          <ol>
            <li>Almost Zero Attrition Top Specialist Team.</li>
            <li>In Depth Domain Knowledge in Fertility Specialty.</li>
            <li>Obsession with Quality.</li>
            <li>Trendsetting Trait.</li>
            <li>Undiluted, Unwavering Focus and Competence on Fertility Specialty.</li>
            <li>Treasure Trove of Fertility Specialties.</li>
            <li>Frugal Entrepreneurial Leadership.</li>
            <li>Pioneering Initiatives.</li>
            <li>Ethical Practice.</li>
            <li>SMART –Simple-Moral-Accountable-Responsible-Transparent.</li>
            <li>Team NU.</li>
            <li>Head-on-Shoulders-Feet on-Ground-Approach.</li>
            <li>Cautious Optimism-backed Growth Plan.</li>
            <li>High Standards of Governance and Stringent Compliance.</li>
            <li>Multi Centric Operations Capable Enterprise and Infrastructure.</li>
          </ol>
          <br>
          <p>The above mentioned factors are just a few reasons <strong>why you should choose our hospital</strong> to fulfill your parenthood dreams. </p>
        </div>
@endsection