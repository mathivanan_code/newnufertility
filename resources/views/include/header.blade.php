<!--<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->

<header class="py-1 d-none d-md-block d-lg-block">
  <div class="container-fluid">
    <div class="row">
    <div class="col-4 m-auto">
    <div class="row">
      <div class="col-3">
      <a href="{{url('/')}}">
       <img  class="nuf_log" src="{{asset('images/NU-FERTILITY-logo-min.png')}}" alt="">
      </a>
      </div>
      <div class="col-5 log_right">
      DISCOVERING PARENTHOOD
      </div>
   
    </div>
    </div>
    <div class="col-4  m-auto">
      <div class="row">
      <div class="col-6 t-a-c ap-b b-l">
            <a href="{{url('/book-an-appointment')}}">
              <img class="bk-img" src="{{asset('images/Book-an-appointment.png')}}" alt="">
              <span>Request an Appointment</span> 
            </a>
        </div>
        <div class="col-6 t-a-c ap-b b-l">
            <a href="{{url('/book-video-consultation')}}">
              <img class="bk-img" src="{{asset('images/Video-consultations.png')}}" alt="">
              <span>Book a Video Consultation</span> 
            </a>
        </div>
      </div>
    </div>
    <div class="col-4  m-auto">
    <div class="row">
      <div class="col-2 t-a-c ap-b b-l">
          <img class="w-80" src="{{asset('images/ISAR-Certificate.png')}}" alt="">
        </div>
        <div class="col-4 t-a-c ap-b m-auto top_panel_top_contact_area">
        <div class="dropdown open">
           <button class="btn btn-primary dropdown-toggle border-20" id="callus" type="button" data-toggle="dropdown" aria-expanded="true">Call Us Now
              <span class="caret"></span></button>
                  <ul class="dropdown-menu" role="menu" aria-labelledby="callus" id="location-mob">
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="tel:+918046699999">
                              <h5>NU Hospitals Rajajinagar (Bengaluru)</h5>
                              <span>+91 8046699999</span>
                          </a>
                      </li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="tel:+918042489999">
                              <h5>NU Hospitals Padmanabhanagar (Bengaluru)</h5>
                              <span>+91 8042489999</span>
                          </a></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="tel:+918182400444">
                              <h5>NU Shivamogga (Clinic), Shivamogga</h5>
                              <span>+91 8182400444</span>
                          </a></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="tel:+916366178822">
                              <h5>NU Shivamogga (Hospitals), Shivamogga</h5>
                              <span>+91 6366178822</span>
                          </a></li>
                      <li role="presentation"><a role="menuitem" tabindex="-1" href="tel:+916366428822">
                              <h5>KM NU Hospitals, Ambur</h5>
                              <span>+91 6366428822</span>
                          </a></li>
                  </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
    </div>
  
  </header>
 
  <!-- top menu area-->
<header>
<div id="menu_area" class="menu-area">
    <div class="container">
        <div class="row">
        
            <nav class="navbar navbar-light navbar-expand-md mainmenu">
                         <div class="d-block d-lg-none d-md-none"> 
                         <a  class="navbar-brand" href="{{url('/')}}">
       <img  class="nuf_log" src="{{asset('images/NU-FERTILITY-logo-min.png')}}" alt="">
      </a> 
                        </div> 
                       
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">


                        <li class="active"><a href="{{url('/')}}">Home <span class="sr-only">(current)</span></a></li>                     
                       
                       
                        <li class="dropdown">
                            <a class="dropdown-toggle" href="{{url('/')}}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About NU Fertility</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                           <li><a href="{{url('/about-nu')}}">About us</a></li>
                            <li><a href="{{url('/why-choose-us')}}">Why choose us</a></li>
                            <!-- <li><a href="{{url('/')}}">NU Advantages</a></li> -->
                            <!-- <li><a href="{{url('/')}}">FAQ</a></li>  -->
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reproductive Medicine/IVF</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a href="{{url('/female-infertility')}}">Female Infertility</a></li>
                            <li><a href="{{url('/ovulation-induction')}}">Ovulation Induction</a></li>
                            <li><a href="{{url('/iui')}}">IUI</a></li>
                            <li><a href="{{url('/ivf')}}">IVF/ICSI</a></li>
                            <li><a href="{{url('/embryo-transfer')}}">Frozen Embryo Transfer</a></li>
                            <li><a href="{{url('/era')}}">ERA</a></li>
                            <li><a href="{{url('/ivm')}}">IVM</a></li>
                            <li><a href="{{url('/fertility-preservation')}}">Fertility Presevation</a></li>
                            <li><a href="{{url('/laparoscopic-surgery-and-hysteroscopic')}}">Laparoscopy & Hysteroscopy</a></li>
                           
                            </ul>
                        </li>


                        <li class="dropdown">
                            <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Andrology</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li><a href="{{url('/andrology')}}">Overview</a></li>
                            <li><a href="{{url('/men-sexual-health')}}">Men's Sexual Health</a></li>
                            <li><a href="{{url('/male-fertility-problems')}}">Male Fertility Problems</a></li>
                            <li class="dropdown">
                                    <a class="dropdown-toggle" href="{{url('/')}}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Packages</a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li><a href="{{url('/premarital-package')}}">Premarital package</a></li>
                                        <li><a href="{{url('/mens-health-clinic')}}">Men’s Health Clinic</a></li>      
                                    </ul>
                                </li>
                            </ul>
                        </li>


                        <li class="dropdown">
                            <a class="dropdown-toggle" href="{{url('/')}}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Our Doctors</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li class="dropdown">
                                    <a class="dropdown-toggle" href="{{url('/')}}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reproductive Medicine</a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li><a href="{{url('/dr-ashwini-ivf-specialist-bangalore')}}">Dr. Ashwini S</a></li>
                                        <li><a href="{{url('/dr-sneha-female-fertility-specialist')}}">Dr. Sneha J</a></li>    
                                        <li><a href="{{url('/dr-kavya-pradeep-gynaecologist')}}">Dr. Kavya V Pradeep</a></li>
                                        <li><a href="{{url('/prakrutha-sreenath')}}">Dr. Prakrutha Sreenath</a></li>    
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" href="{{url('/')}}" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Andrology</a>
                                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <li><a href="{{url('/dr-pramod-krishnappa-andrologist')}}">Dr. Pramod Krishnappa</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li><a href="{{url('/international-patients')}}">International Services</a></li>
                        <li><a href="{{url('/contact')}}">Contact Us</a></li>
                        <li><a href="{{url('/blog')}}">Blog</a></li>



                       
                    </ul>
                </div>
            </nav>

        </div>
    </div>
</div>
</header>