  <section class="footer-section">
      <div class="container">


          <div class="row f">
              <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 col-xl-12">

                  <a class="logo-img" style="text-align:center;" href="{{url('/')}}">
                      <img class="nuf_log" src="{{asset('images/NU-FERTILITY-logo-min.png')}}" alt="">
                  </a>
              </div>

              <div class="col-lg-5">
                  <div class="f-tit">
                      <span> Stay in touch <span>
                  </div>



                  <form class="form-inline" action="">
                      <div class="form-group">

                          <input type="email" class="form-control" id="email" placeholder="Subscribe Now" name="email">
                      </div>

                      <div class="form-group f-button">
                          <button type="submit" class="btn btn-default "
                              style="background-color: #ee5da0;color:white">Subscribe</button>
                      </div>
                  </form>

              </div>

              <div class="col-lg-2">


              </div>


              <div class="col-lg-3">

                  <h4>Contacts</h4>
                  <div class="contacts_center">
                      <span class="contacts-address">
                          <a href="mailto:fertility@nuhospitals.com" class="contacts-address-icon"><i
                                  class="fa fa-envelope"></i>&nbsp; fertility@nuhospitals.com</a>
                      </span><br>
                      <span class="contacts_address" class="contacts-address-icon">
                          <a href="mailto:Andrology@nuhospitals.com" class="contacts-address-icon"><i
                                  class="fa fa-envelope"></i>&nbsp; andrology@nuhospitals.com</a>
                      </span><br>
                      <span class="contacts_address" class="contacts-address-icon">
                          <a href="mailto:care@nuhospitals.com" class="contacts-address-icon"><i
                                  class="fa fa-envelope"></i>&nbsp; care@nuhospitals.com</a>
                      </span><br>
                      <span class="contacts_address" class="contacts-address-icon">
                          <a href="tel: +91 80 46699999" class="contacts-address-icon"><i class="fa fa-phone"></i> +91
                              80 46699999</a> /

                          <a href="tel: +91 80 42489999" class="contacts-address-icon"> +91 80 42489999</a>
                      </span>

                      <div class="contacts-address-social-icons">
                          <span class="social-item"> <a href="#" target="_blank" class="social_icons social_twitter"><i
                                      class="fa fa-twitter"></i></a> </span>
                          <span class="social-item"> <a href="#" target="_blank" class="social_icons social_facebook"><i
                                      class="fa fa-facebook"></i></a> </span>
                          <span class="social-item"> <a href="#" target="_blank"
                                  class="social_icons social_instagram"><i class="fa fa-instagram"></i></a> </span>
</div>


                  </div>








              </div>



          </div>




      </div>


      <div class="container-fluid ">
          <div class="row f-rights">
              <div class="col-md-12 text-center">
                  <p><a href="http://nufertility.com/">NU Fertility</a> © 2020. All Rights Reserved. <a
                          href="https://www.nufertility.com/#">Terms of Use</a> and <a
                          href="https://www.nufertility.com/#">Privacy Policy</a></p>


                  <hr class="line">
              </div>
          </div>

      </div>



  </section>




  <!-- JavaScript Libraries -->

  @include('include/social-link')
  <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('lib/jquery/jquery-migrate.min.js') }}"></script>
  <script src="{{ asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('lib/superfish/hoverIntent.js') }}"></script>
  <script src="{{ asset('lib/superfish/superfish.min.js') }}"></script>
  <script src="{{ asset('lib/easing/easing.min.js') }}"></script>
  <script src="{{ asset('lib/modal-video/js/modal-video.js') }}"></script>
  <script src="{{ asset('lib/owlcarousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('lib/wow/wow.min.js') }}"></script>
  <script src="{{ asset('js/custom.js') }}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{ asset('contactform/contactform.js') }}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ asset('js/main.js') }}"></script>

  <script>
jQuery('document').ready(function() {
    jQuery('.navbar-toggler').click(function(e) {
        if ($(this).hasClass('collapsed')) {
            $(this).removeClass('collapsed');
            $('#navbarCollapse').addClass('show');
        } else {
            $(this).addClass('collapsed');
            $('#navbarCollapse').removeClass('show');
        }
    })
})
  </script>