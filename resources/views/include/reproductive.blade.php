<div class="sidebar_inner">
        <aside id="nav_menu-2" class="widget widget_nav_menu">
            <h5 class="widget_title">Services</h5>
            <div class="menu-services-menu-container">
            <ul id="menu-services-menu" class="menu prepared">
                    <li id="menu-item-192" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-192"><a href="./female-infertility">Female Infertility</a></li>
                    <li id="menu-item-183" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-183"><a href="./ovulation-induction">Ovulation Induction</a></li>
                    <li id="menu-item-183" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-183"><a href="./iui">IUI</a></li>
                    <li id="menu-item-190" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-190"><a href="./ivf">IVF/ICSI</a></li>
                    <li id="menu-item-189" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-189"><a href="./embryo-transfer">Frozen Embryo Transfer</a></li>
                    <li id="menu-item-191" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-191"><a href="./era">ERA </a></li>
                    <li id="menu-item-183" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-183"><a href="./ivm">IVM</a></li>
                    <li id="menu-item-183" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-183"><a href="./fertility-preservation">Fertility Presevation</a></li>
                    <li id="menu-item-183" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-183"><a href="./laparoscopic-surgery-and-hysteroscopic">Laparoscopy &amp; Hysteroscopy</a></li>

                    <!-- <li id="menu-item-192" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-192"><a href="./male-infertility">Infertility Evaluation (Male)</a></li>
                    <li id="menu-item-183" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-183"><a href="./sperm-donation-new">Sperm Donation</a></li>
                    <li id="menu-item-183" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-183"><a href="./microTESE">MicroTESE</a></li> -->
                </ul>
            </div>
        </aside>
</div>