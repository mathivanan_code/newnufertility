<div class="sidebar_inner">
<aside id="search-2" class="widget widget_search"><form role="search" method="get" id="searchform" action="https://www.nufertility.com/blog/">
    <div>
		<input type="text" placeholder="Search and hit enter..." name="s" id="s">
	 </div>
</form></aside>
        <aside id="nav_menu-2" class="widget widget_nav_menu">
        <div>
        <h5 class="widget_title">RECENT POSTS</h5>
                <div class="menu-services-menu-container">
                    <ul id="menu-services-menu" class="menu prepared">
                        @foreach($blog_recents as $key => $blog_recent)
                        <li id="{{'menu-item-'.$key}}" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-192"><a href="{{'/'.$blog_recent}}">{{$key}}</a></li>
                        @endforeach
                    </ul>
                </div>
        </div>
        </aside>
        <aside id="nav_menu-2" class="widget widget_nav_menu">   
            <div>
                <h5 class="widget_title">CATEGORIES</h5>
                <div class="menu-services-menu-container">
                    <ul id="menu-services-menu" class="menu prepared">
                        @foreach($blog_categorys as $key => $blog_category)
                        <li id="{{'menu-item-'.$key}}" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-192"><a href="{{'/category/'.$key}}".>{{$blog_category}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </aside>
</div>
