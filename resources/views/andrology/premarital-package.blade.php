@extends('layouts/andrology-layout')

@section('meta-title')
<title>Premarital package| NU Fertility Hospitals</title>

@endsection
@section('meta-description')
<meta name="description" content="Males who want to know their fertility status prior to their marriage can get themselves examined and evaluated with appropriate investigations." />

@endsection


@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/andrology/Packages_1366.jpg')}}" alt="">
@endsection


@section('content')
<div class="content">
								<article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
									<section class="services_page_header">
										<div class="services_page_featured"> <img class="img-fluid" src="images/andrology/Packages_650.jpg" class="attachment-felizia-thumb-huge size-felizia-thumb-huge wp-post-image" alt="Fetal Madcine" itemprop="image" 
> </div>
										<h2 class="services_page_title">Overview</h2> </section>
									<section class="services_page_content entry-content" itemprop="articleBody">
										<div class="vc_row wpb_row vc_row-fluid">
											<div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
												<div class="vc_column-inner">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element">
															<div class="wpb_wrapper">
																<div class="panel-group" id="accordion">
																	<div class="panel panel-default">
																		<div class="panel-heading">
																			<h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Premarital package<i class="fa fa-arrow-down pull-right panel-icon" aria-hidden="true"></i></a>
                                                                            </h4> 
                                                                        </div>
																		<div id="collapse1" class="panel-collapse collapse in">
																			<div class="panel-body">Males who want to know their fertility status prior to their marriage can get themselves examined and evaluated with appropriate investigations. This involves:
																				<ul>
																					<li>General Tests: HbA1C (Blood sugars), CBC, Serum Creatinine, urine analysis</li>
																					<li>Blood Hormones: Testosterone, FSH, LH, Estradiol, Prolactin</li>
																					<li>Semen analysis</li>
																					<li>Ultrasound scrotum</li>
																					<li>Andrology Consultation</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																	
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
									</div></section>
								</article>
								</div>
@endsection