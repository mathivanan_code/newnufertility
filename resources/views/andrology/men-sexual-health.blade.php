@extends('layouts/andrology-layout')

@section('meta-title')
<title>Men's Sexual Health | NU Fertility Hospitals</title>

@endsection
@section('meta-description')
<meta name="description" content="Erectile Dysfunction (ED) is a persistent inability to achieve or maintain an erection that is firm enough to have sexual intercourse." />

@endsection

@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/andrology/Men-Sexual-Health_1366.jpg')}}" alt="">
@endsection


@section('content')

<div class="content">
                            <article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
                                <section class="services_page_header">
                                    <div class="services_page_featured">
                                        <img  src="images/andrology/Men-Sexual-Health_650.jpg"
                                         class="img-fluid attachment-felizia-thumb-huge size-felizia-thumb-huge wp-post-image" 
                                         alt="Fetal Madcine" itemprop="image" 
                                        >
                                    </div>
                                    <h2 class="services_page_title">Overview</h2>
                                </section>
                                <section class="services_page_content entry-content" itemprop="articleBody">
                                    <div class="vc_row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element">
                                                        <div class="wpb_wrapper">
           <div class="panel-group" id="accordion">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" class="collapsed">Erectile Dysfuction (ED)<i class="fa fa-arrow-down pull-right panel-icon" aria-hidden="true" ></i></a>
                </h4>
              </div>
              <div id="collapse1" class="panel-collapse collapse in" aria-expanded="false" style="height: 0px;">
                <div class="panel-body">Erectile Dysfunction (ED) is a persistent inability to achieve or maintain an erection that is firm enough to have sexual intercourse.
                <span style="text-align:center; padding:20px 0;">
                    <img class="img-fluid" src="https://www.nuhospitals.com/assets/images/specialities/andrology/1.jpg">
                </span>
                  <h4>How common?</h4>
                  <p>In a study from John Hopkins Institute in 2007, the overall prevalence of ED in men aged above 20 years was 18.4% suggesting that ED affects 18 million men in the USA. Among men with diabetes, the prevalence of ED was 51.3%, so it’s a fairly common problem.</p>
                  <p><br><br><b>What causes ED?</b><br></p>
                <ul>
                      <li>Lifestyle choices (smoking, excessive alcohol, obesity, lack of
                          exercise)</li>
                      <li>Diabetes</li>
                      <li>Medications (blood pressure, antidepressants)</li>
                      <li>Cardiovascular disease (high blood pressure heart disease)</li>
                      <li>Hormone problems</li>
                      <li>Prostate cancer treatment</li>
                      <li>Surgery (prostate, bladder, colon)</li>
                      <li>Spinal cord injuries</li>
                  </ul>
                  <center>
                      <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/T1.jpg" alt="Male Infertility Treatment">
                  </center>
                  <p>
                      <b>Treatment options?</b><br><br>
                      Penile Doppler scan with an injection may be required occasionally
                      in special situations.<br><br>
                      Whatever is causing ED, there is a treatment option that can provide
                      a satisfying solution.<br><br>
                      If you try one of the treatment options listed and it doesn’t work
                      for you or you aren’t completely satisfied, don’t be discouraged and
                      give up hope.<br><br>
                      These treatment options have varying degrees of success for each man
                      depending on the cause of the ED. Irreversible blood vessel or nerve
                      damage may impact the success of some of these treatments.<br>
                      It is important to know all of your available options and discuss
                      them with your doctor to determine which will be appropriate for you
                      and your lifestyle.<br><br>
                      Lifestyle modifications: Exercise regularly (5 times a week),
                      healthy weight, avoid smoking, restrict alcohol intake to 2 drinks
                      or less per day, adopt better sleep habits, take care of your other
                      health issues such as high blood sugar and heart, artery or kidney
                      disease.<br><br>
                      Non-Surgical Options: Oral medications, penile injections, vacuum
                      erection device<br><br>
                      Surgical option: Penile implants
                </p>
                <h4>Non-Surgical options</h4>
                <br>
                <p>
                    <b>Oral medications:</b><br>
                    These drugs are known as phosphodiesterase type 5 (PDE-5)
                    inhibitors. They work to relax muscle cells in the penis for better
                    blood flow and to produce a rigid erection. These medicines work in
                    about 7 out of every 10 men with ED. They can be effective
                    regardless of age or race. However, they only work if a man is
                    sexually stimulated. Their effects last for only a set amount of
                    time. Men should take these medications 30-60 minutes before sexual
                    activity. These drugs do not treat a lack of sexual desire. As with
                    any drug, some men may experience side effects when taking PDE-5
                    inhibitors. The most common are headaches, flushing (redness) of the
                    face, runny or stuffy nose, upset stomach, dizziness and muscle
                    aches. Those side effects are usually mild-moderate, but taking
                    these drugs with alcohol may make them worse. Be sure you tell your
                    doctor about all drugs you are taking, including prescriptions,
                    over-the-counter medications or supplements or recreational drugs
                    before you take any PDE-5 inhibitors.
                </p>
                <center>
                  <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/34.jpg" alt="Andrology">
                </center>
                <p><b>Penile self-injection:</b><br>Alprostadil (Prostaglandin)
                    Injection therapy uses a needle to inject medication directly into
                    the base or side of the penis. These medications improve blood flow
                    into the penis to cause an erection. The recommended frequency of
                    injection is no more than three times weekly and should produce an
                    erection in 5-20 minutes. Beyond a possible fear of needles, men may
                    experience pain, fibrosis and risk of a persistent erection with
                    these injections. 60-65% of men discontinue this mode of treatment
                    after 1 year.</p>

                    <center>
                       <img  class="img-fluid" src="https://www.nuhospitals.com/assets/images/specialities/andrology/2.jpg" alt="Andrology Surgery"><br>
                         <span>Fig: Penile Alprostadil Injection</span><br><br>
                    </center>
                    <p><b>Vacuum Erection Devices (VED):</b><br>A mechanical ED pump used to
                      pull blood into the penis can cause an erection. The system includes
                      a plastic cylinder, an external penile pump and a tension band to
                      place at the base of the penis. When the penis is erect, the ring is
                      placed at the base to maintain an erection long enough to have sex
                      (up to 30 minutes). This is a drug-free non-invasive method of
                      treatment, but the person will not be able to ejaculate soon after
                      the orgasm due to a constrictive ring at the penile base.</p>
                      <center>
                        <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/3.jpg" alt="Vacuum erection device "><br>
                        <span>Fig: Vacuum Erection Device (VED)</span><br><br>
                     </center>
                     <h4>Surgical option</h4>
                     <br>
                     <p><b>Penile Implants:</b><br>In use since 1971, penile implants have
                        helped many men return to active sex life. A penile implant is a
                        medical implant that is implanted into the penis in the operation
                        theatre. The implant is entirely concealed within the body. Two
                        basic types of implants are available. With malleable or bendable
                        implants, two silicon-type cylinders are inserted into the penis. To
                        have an erection, a man bends his penis upward into an erect
                        position. The second type, an inflatable implant has a pair of
                        inflatable cylinders which is attached to a fluid reservoir and a
                        pump hidden inside the body. To have an erection, a man presses on
                        the pump. This transfers fluid into the cylinders, making the penis
                        rigid. To return the penis to a natural flaccid state, the pump is
                        deflated.</p>
              </div>
              <div style="text-align:center; padding:20px 0;">
                    <img class="img-fluid" src="https://www.nuhospitals.com/assets/images/specialities/andrology/4.jpg"><br>
                    <span>Fig: Malleable Implant</span>
                </div>
                <div style="text-align:center; padding:20px 0;">
                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/5.jpg"><br>
                    <span>Fig: Malleable Implant- Shah</span>
                </div>
                <div style="text-align:center; padding:20px 0;">
                    <img class="img-fluid" src="https://www.nuhospitals.com/assets/images/specialities/andrology/6.jpg"><br>
                    <span>Fig: Inflatable penile implant</span>
                </div>
                <div style="text-align:center; padding:20px 0;">
                    <img class="img-fluid" src="https://www.nuhospitals.com/assets/images/specialities/andrology/7.jpg"><br>
                    <span>Fig: Inflatable penile Implant in anatomical position</span>
                </div>
                <h4 style="padding: 0 20px">Features of Penile Implant Surgery:</h4>
                <ul style="padding: 0 35px;">
                        <li>Permanent ED TreatmentSmall </li>
                        <li>external scar</li>
                        <li>Concealed within the body</li>
                        <li>Maintain erection as long as desired</li>
                        <li>Spontaneous-sex when the mood strikes</li>
                        <li>Doesn’t interfere with orgasm or ejaculation</li>
                        <li>High patient and partner satisfaction</li>
                        <li>Low risk of device failure. Generally inflatable implants last 10-15 years. It is possible to replace them in case the device fails.</li>
                    </ul>
                </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" class="collapsed" aria-expanded="false">Ejaculatory dysfunction (EJD)<i class="fa fa-arrow-down pull-right panel-icon" aria-hidden="true"></i></a>
                </h4>
              </div>
              <div id="collapse2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                <div class="panel-body">Ejaculatory dysfunction: Premature ejaculation, Retrograde ejaculation, Anejaculation.
                <h4>Premature Ejaculation:</h4>
                <p>Premature ejaculation is when semen is released sooner than a man or his partner would like. PE might not be a cause for worry. But, PE can be frustrating if it makes sex less enjoyable and impacts your relationship.</p>

                <p>A study looking at 500 couples from five different countries found the average time taken to ejaculate during intercourse was around 5 minutes. However, it's up to each couple to decide if they’re happy with the time taken – there’s no definition of how long sex should last. Occasional episodes of premature ejaculation are common and aren't a cause for concern. However, if you're finding that around half of your attempts at sex result in premature ejaculation, it might help to get treatment.</p>

                <p>There are many reasons why men have PE. There can be biological, chemical, and/or emotional reasons. There may be issues with the brain signals that rule sexual excitement.</p>

                <p>Common treatments are behavioral therapy, tablets and creams. Many people try more than one option at the same time.</p>
                <ul>
                    <li>Behavioural Therapy: Makes men aware of the feelings that lead to the climax, so they can delay ejaculation. The goal is to train your body and increase control. The stop-start method is when you stop stimulation, regain control, and then start again. You will need your partner’s help with these exercises.</li>
                    <p>Medical Therapy: They lower serotonin levels. You’ll usually be advised to take it one hour before sex, but not more than once a day. Your response to the treatment will then be reviewed after four weeks (or after six doses), and again every six months.
                    </p><p>Numbing Creams or Sprays: There are creams and sprays that you can put on the head and shaft of the penis before sex to lower sensation. They also cause vaginal numbness, so they should be washed off before sex.
                </p></ul>
                <h4>Retrograde Ejaculation:</h4>
                <p>It happens when semen travels backward into the bladder instead of through the urethra (the tube that urine passes through).</p>
                <div style="text-align:center; padding:20px 0;">
                    <img class="img-fluid" src="https://www.nuhospitals.com/assets/images/specialities/andrology/couples-img.jpg">
                </div>
                <p>Men with retrograde ejaculation still experience the feeling of an orgasm and the condition doesn't pose a danger to health. However, it can affect the ability to father a child.</p>
                <p>Prostate gland surgery or bladder surgery is the most common cause of retrograde ejaculation. Other causes are diabetes, multiple sclerosis, and a class of medicines known as alpha-blockers, which are often used to treat high blood pressure (hypertension).</p>
                <p>Most men do not need treatment for retrograde ejaculation because they are still able to enjoy healthy sex life and the condition does not have adverse effects on their health.</p>
                <p>Men who want to have children can have sperm taken from their urine for use in artificial intrauterine insemination (IUI) or in-vitro fertilization (IVF).</p>

                <h4>Anejaculation</h4>
                <p>Anejaculation is the inability to ejaculate semen despite stimulation of the penis by intercourse or masturbation. If anejaculation is caused by medications, stopping the medicine will most likely restore normal function.</p>
                <p>Vibrostimulation: The vibrator acts by providing a strong stimulus for a long duration (20-30min) to the penis. Vibrator stimulation results in ejaculation in about 60% of men suffering from a neurological (spinal cord) injury. This is a simple and quite effective way of retrieving semen in order to proceed with artificial insemination (inserting sperm directly into the uterus).</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" class="collapsed" aria-expanded="false">Low testosterone (Hypogonadism)<i class="fa fa-arrow-down pull-right panel-icon" aria-hidden="true"></i></a>
                </h4>
              </div>
              <div id="collapse3" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                <div class="panel-body">Male hypogonadism means the testicles do not produce enough of the male sex hormone testosterone. When levels are low, men might have decreased sex drive, less muscle mass, erectile dysfunction, and fatigue. Testosterone is responsible for male reproductive and sexual functions. It affects puberty, fertility, muscle mass, body composition, bone strength, fat metabolism, sex drive, mood and mental processes.
                <div style="text-align:center; padding:20px 0;">
                    <img class="img-fluid" src="https://www.nuhospitals.com/assets/images/specialities/andrology/13.jpg"><br>
                </div>
                <h4>Types of Hypogonadism:</h4>
                    <p>Primary hypogonadism is caused by a problem in the testes. This type is most frequent and usually affects development in childhood and adolescence.<br>
                    Secondary hypogonadism is caused by a problem in glands (pituitary gland, hypothalamus) that tell the testes to make testosterone. This type is more common among older men.</p>
                    <p>One must give it time. On the other hand, there are chances of multiple births, i.e, if more than one embryo is transferred into your uterus. The use of multiple embryos is done to increase the chances of conception. Stress can also be brought on by the length of the process, hormonal changes, and medication. </p>
                    <p>The chances of a fetus born with malformation is similar to natural conception.</p>
                  <div style="text-align:center; padding:20px 0;">
                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/15.jpg"><br>
                  </div>
                  <h4>Symptoms:</h4>
                  <p>Hypogonadism can occur at any age. The symptoms will be different depending on your age when it develops. Common symptoms in adult men include:</p>
                  <ul>
                      <li>Fatigue</li>
                      <li>Hot</li>
                      <li>flushes</li>
                      <li>Low sex drive</li>
                      <li>Erectile dysfunction</li>
                      <li>Mood changes</li>
                      <li>Difficulty concentrating</li>
                      <li>Problem in sleeping</li>
                      <li>Loss of muscle mass</li>
                      <li>Decreased bone density</li>
                      <li>Enlarged breastsLoss of body hair</li>
                      <li>Infertilit</li>
                  </ul>
                  <h4>Diagnosis:</h4>
                  <p>Male hypogonadism is diagnosed based on:</p>
                  <ul>
                      <li>Long-term discomfort from symptoms</li>
                      <li>Low testosterone levels in the blood</li>
                      <li>Size of the testes on clinical examination</li>
                  </ul>
                  <h4>Treatment:</h4>
                  <div style="text-align:center; padding:20px 0;">
                    <img class="img-fluid" src="https://www.nuhospitals.com/assets/images/specialities/andrology/16.jpg"><br>
                  </div>
                  <div style="text-align:center; padding:20px 0;">
                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/17.jpg"><br>
                  </div>

                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" class="collapsed" aria-expanded="false">Penile curvature / Peyronie’s disease:<i class="fa fa-arrow-down pull-right panel-icon" aria-hidden="true"></i></a>
                </h4>
              </div>
              <div id="collapse4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                <div class="panel-body">Penile curvature could be from birth (congenital penile curvature) or acquired later in life (Peyronie’s Disease). One will usually notice a curved penis only during the penile erection and not when the penis is flaccid (resting state).
                <p>Congenital penile curvature although present since birth will become obvious during erection when he reaches puberty or early adult life.</p>
                <p>Peyronie’s Disease is caused by the way a person’s body heals wounds. Injury or damage to the outer tissues of the penis causes scar-like tissue (plaque) to form.</p>
                <div style="text-align:center; padding:20px 0;">
                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/8.jpg"><br>
                     <span>“Peyronie’s was named after the French surgeon François Gigot de La Peyronie, who described it in 1743”</span>
                  </div>
                  <div style="text-align:center; padding:20px 0;">
                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/9.jpg">
                  </div>
                  <p style="text-align:center;">PEYRONIE’S DISEASE IS NOT A DISEASE YOU CAN CATCH FROM SOMEONE ELSE AND IT IS NOT CAUSED BY ANY KNOWN DISEASE THAT CAN BE PASSED TO OTHERS.</p>
                  <div style="text-align:center; padding:20px 0;">
                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/10.jpg">
                  </div>
                  <p style="text-align:center;">Peyronie’s disease usually occurs in two phases — the acute (or active) phase and the chronic (or stable) phase. The first painful phase can last up to about 18 months. For most men, the chronic, or stable, phase begins 12-18 months after symptoms first appear.</p>
                  <div style="text-align:center; padding:20px 0;">
                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/11.jpg">
                  </div>
                  <p style="text-align:center;">Fig: Peyronie’s disease with penile curvature and plaque</p>
                  <h4>Signs of Peyronie’s disease may involve:</h4>
                  <ul>
                      <li>a curve in the penis</li>
                      <li>hard lumps on one or more sides of the penis</li>
                      <li>painful erectionsno or soft erections</li>
                      <li>Spontaneous-sex when the mood strike</li>
                      <li>shaving trouble with sex or having sex that hurts because of a bent/curved penis</li>
                  </ul>
                  <div style="text-align:center; padding:20px 0;">
                    <img class="img-fluid" src="https://www.nuhospitals.com/assets/images/specialities/andrology/12.jpg">
                  </div>
                  <h4>Treatment:</h4>
                  <p>Andrologists may treat Peyronie’s using non-surgical or surgical treatments.</p>
                  <p>Non-surgical treatments may include tablets, penile traction devices and shots/injections directly into the plaque which brings higher doses of the drug directly to the problem.</p>
                  <p>Surgery is an option for men with severe penile curvature that find it difficult to have sex. There are three surgeries used to help men with Peyronie’s Disease:</p>
                  <ul>
                      <li>Making the side of the penis opposite the plaque shorter (Plication surgery)</li>
                      <li>Making the side of the penis with plaque longer with a graft (Graft surgery)</li>
                      <li>Making the penis straight with a prosthetic device (penile implant)</li>
                  </ul>
                  <div style="text-align:center; padding:20px 0;">
                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/14.jpg">
                  </div>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" class="collapsed">Penile enlargement surgeries<i class="fa fa-arrow-down pull-right panel-icon" aria-hidden="true"></i></a>
                </h4>
              </div>
              <div id="collapse5" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                <div class="panel-body">
                  <ul>
                      <li>Penile fillers </li>
                      <li>Suprapubic fat reduction </li>
                      <li>Pubic lipectomy </li>
                      <li>Pubic liposuction </li>
                      <li>Penile implant and multiple corporotomy incisions</li>
                </ul></div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" class="">Priapism<i class="fa fa-arrow-down pull-right panel-icon" aria-hidden="true"></i></a>
                </h4>
              </div>
              <div id="collapse6" class="panel-collapse collapse in" aria-expanded="true" style="">
                <div class="panel-body">Priapism is a rare condition involving an erection that lasts for an unusually long time. It can be painful. This type of erection is not related to sexual stimulus. Immediate treatment is important to prevent tissue damage and erectile dysfunction (ED).
                <div style="text-align:center; padding:20px 0;">
                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/19.jpg">
                </div>
                <p>Priapism got the name from Priapus. Roman statue showing Priapus – the god of fertility. The distinguishing feature of the deity is the great erect penis, which was to symbolize the economic well-being of the owners of the House of the Vettii in Pompeii.</p>
                <h4>There are two types of priapism:</h4>
                <ul>
                    <li>Ischemic priapism: when blood cannot leave the penis. This erection can last for more than four (4) hours. The penis shaft may be very hard, while the tip is soft. It is known to cause pain and discomfort. This type may stop and start (stuttering priapism).</li>
                    <li>Nonischemic priapism: when too much blood flows into the penis. This is a less painful erection, but it can also last for more than four (4) hours. The penis shaft is erect but not rigid.</li>
                </ul>
                <p>Priapism can happen in young boys (age 5-10), young adults (around age 20) and mature men (over age 50).</p>
                <p>Priapism happens when blood flow to the penis doesn’t work correctly. Some things that could cause this are:</p>
                <ul>
                    <li>Blood disorders, like sickle cell anemia and leukemia</li>
                    <li>Prescription drugs, like some ED drugs, e.g. Sildenafil (Viagra), Tadalafil, mental health drugs, e.g. Fluoxetine, Bupropion, Risperidone and Olanzapine and blood thinners, e.g. Warfarin and Heparin</li>
                    <li> Alcohol and drug use</li>
                    <li> Injury to your genitals, pelvis or the area between the penis and the anus; or to the spinal cord</li>
                    <li> Tumours</li>
                <ul>
                    <h4>WHY IS TREATMENT IMPORTANT?</h4>
                    <p>When an erection lasts for too long, the blood becomes trapped in the penis. The blood trapped in the penis is unable to go to other parts of the body. The lack of oxygen can damage or destroy tissue in the penis. This can disfigure the penis. It may also cause problems like erectile dysfunction (when the penis cannot become erect) in the future.</p>
                    <h4>Treatment:</h4>
                    <p>Ischemic priapism (most common, 95%) calls for emergency care. Blood must be drained from the penis. There are several ways to do this:</p>
                    <ul>
                        <li>Aspiration (when a surgical needle and syringe is used) to drain excess blood</li>
                        <li>Medicine or a saline mix may be injected into penile veins to improve blood flow. The veins are flushed to relieve pain, remove oxygen-poor blood and stop the erection</li>
                        <li>A surgeon may perform a “shunt” to vent blood from the penis or penile implant surgery if presented beyond 36 hours of having priapism.</li>
                    </ul>
                    <p>Nonischemic priapism (less common, 5%) often goes away without treatment. Simple ice and pressure on the perineum may help end the erection. A watch and wait approach is used before surgery.</p>
                </ul></ul></div>
              </div>
          </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </div></section>
                            </article>
                        </div>
@endsection