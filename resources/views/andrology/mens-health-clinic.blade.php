@extends('layouts/andrology-layout')

@section('meta-title')
<title>Men’s Health Clinic| NU Fertility Hospitals</title>

@endsection
@section('meta-description')
<meta name="description" content="In general, men have poorer health habits and a shorter life expectancy than women. This may be because they are more likely to engage in unhealthy behaviour, and are less likely than women to adopt preventive health measures." />

@endsection
@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/andrology/Packages_1366.jpg')}}" alt="">
@endsection


@section('content')
<div class="content">
	<article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
									<section class="services_page_header">
										<div class="services_page_featured"> <img src="images/andrology//Packages_650.jpg" class="img-fluid attachment-felizia-thumb-huge size-felizia-thumb-huge wp-post-image" alt="Fetal Madcine" itemprop="image" 
> </div>
										<h2 class="services_page_title">Overview</h2> </section>
									<section class="services_page_content entry-content" itemprop="articleBody">
										<div class="vc_row wpb_row vc_row-fluid">
											<div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
												<div class="vc_column-inner">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element">
															<div class="wpb_wrapper">
																<div class="panel-group" id="accordion">
																	
																	<div class="panel panel-default">
																		<div class="panel-heading">
																			<h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Men’s Health Clinic<i class="fa fa-arrow-down pull-right panel-icon pull-right panel-icon pull-right panel-icon" aria-hidden="true"></i></a>
                </h4> </div>
																		<div id="collapse2" class="panel-collapse collapse in">
																			<div class="panel-body">In general, men have poorer health habits and a shorter life expectancy than women. This may be because they are more likely to engage in unhealthy behaviour, and are less likely than women to adopt preventive health measures. But men’s health issues don’t affect only men- they have a significant impact on their family and friends too. Unfortunately a lot of issues particularly the ones having to do with sex or masculinity- are very hard for men to talk about.
																				<div style="text-align:center; padding:20px 0;"> <img src="https://www.nuhospitals.com/images/andrology/T2.jpg"> </div>
																				<p>Men’s health clinic involves the evaluation of the following:</p>
																				<ul>
																					<li>Erectile Dysfunction: Blood Testosterone, Lipid profile, HbA1C (blood sugar)</li>
																					<li>Andropause: Blood Total Testosterone, SHBG (Sex Hormone Binding Globulin) and Serum Albumin to calculate Bio-available Testosterone</li>
																					<li>Prostate (Prostate enlargement or Prostate cancer): Serum PSA, Digital rectal examination, ultrasound abdomen pelvis (pre void/post-void residual urine) and uroflowmetry</li>
																					<li>Testicular cancer: Clinical examination and Ultrasound scrotum</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
									</div></section>
								</article>
								</div>
@endsection