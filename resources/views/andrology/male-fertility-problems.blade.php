@extends('layouts/andrology-layout')

@section('meta-title')
<title>Male Fertility Problems | NU Fertility Hospitals</title>

@endsection
@section('meta-description')
<meta name="description" content="Abnormalities in sperm due to various reasons can deprive an otherwise happy couple of having a child. Of all infertility cases, approximately 40–50% is due to “male factor” infertility." />

@endsection


@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/andrology/Male-Fertility-Problems_1366.jpg')}}" alt="">
@endsection


@section('content')
<div class="content">
                                <article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
                                    <section class="services_page_header">
                                        <div class="services_page_featured">
                                            <img  class="img-fluid" src="images/andrology/Male-Fertility-Problems_650.jpg" class="attachment-felizia-thumb-huge size-felizia-thumb-huge wp-post-image" alt="Fetal Madcine" itemprop="image" 
                       >
                                        </div>
                                        <h2 class="services_page_title">Overview</h2>
                                    </section>
                                    <section class="services_page_content entry-content" itemprop="articleBody">
                                        <div class="vc_row wpb_row vc_row-fluid">
                                            <div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
                                                <div class="vc_column-inner">
                                                    <div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element">
                                                            <div class="wpb_wrapper">
                                                                <div class="panel-group" id="accordion">
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Male Infertility<i class="fa fa-arrow-down pull-right panel-icon" aria-hidden="true"></i></a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapse1" class="panel-collapse collapse in">
                                                                            <div class="panel-body">Abnormalities in sperm due to various reasons can deprive an otherwise happy couple of having a child. Of all infertility cases, approximately 40–50% is due to “male factor” infertility.
                                                                                <div style="text-align:center; padding:20px 0;">
                                                                                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/20.jpg">
                                                                                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/21.jpg" alt="Male Infertility Treatment">
                                                                                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/22.jpg" alt="Treating Male Infertility">
                                                                                </div>
                                                                                <div style="text-align:center; padding:20px 0;">
                                                                                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/23.jpg" alt="Male Infertility causes">
                                                                                </div>
                                                                                <p>A recent international study analysis revealed that there is a significant decline in sperm counts between 1973 and 2011, driven by a 50-60% decline among men from North America, Europe, Australia and New Zealand. The reasons could be multifactorial being birth defects, genetic abnormalities, radiation exposure, lifestyle changes, exposure to various toxins, stress or sometimes unknown.</p>
                                                                                <p>In such conditions with abnormalities in semen/sperm, one has to approach the Andrologist for possible solutions without much delay because the fertility potential of a couple decreases significantly with advancing age.</p>
                                                                                <h4>Diagnosis:</h4>
                                                                                <p>Both male and female partners have to be simultaneously investigated, to categorise infertility.</p>
                                                                                <p>Basic evaluation of male infertility involves:</p>
                                                                                <ul>
                                                                                    <li>General physical examination including scrotum and penis</li>
                                                                                    <li>Basic blood hormone profile such as Testosterone, FSH, LH</li>
                                                                                    <li>Semen analysis</li>
                                                                                </ul>
                                                                                <p>Semen collection: A dedicated room with adequate privacy would be given. If the male partner is unable to ejaculate, a penile vibrator could be used for stimulation.</p>
                                                                                <div style="text-align:center; padding:20px 0;">
                                                                                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/24.jpg">
                                                                                </div>
                                                                                <h4>Surgical sperm retrieval:</h4>
                                                                                <p>Depending on several factors such as testicular size, previous scrotal surgery and hormone levels, the Andrologist will decide which would be the best option.</p>
                                                                                <p>Different surgical options to retrieve sperms:</p>
                                                                                <ul>
                                                                                    <li>PESA: Percutaneous Epididymal Sperm Aspiration</li>
                                                                                    <li>TESE (Testicular Sperm Extraction)</li>
                                                                                    <li>Micro- TESE: using a surgical microscope</li>
                                                                                </ul>
                                                                                <div style="text-align:center; padding:20px 0;">
                                                                                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/25.jpg">
                                                                                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/26.jpg" alt="Urological Condition">
                                                                                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/27.jpg" alt="Sperm Testing">
                                                                                    <p>Fig: TESE (Testicular Sperm Extraction)</p>
                                                                                </div>
                                                                                <h4>What other options are available if no sperms are found with surgery?</h4>
                                                                                <ul>
                                                                                    <li><strong>Donor insemination (DI)</strong><br>
                                                                                        Donor semen is carefully screened for infections and a donor selected to have similar attributes to you. This is the only viable option if you have no sperms at all and you do not have obstruction which can be relieved surgically.
                                                                                    </li><li><strong>Adoption</strong><br>
                                                                                        If you are unfortunate and do not to have any success with other treatments, you may wish to consider adopting a child.</li>
                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Varicocele<i class="fa fa-arrow-down pull-right panel-icon" aria-hidden="true"></i></a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapse2" class="panel-collapse collapse">
                                                                            <div class="panel-body">A varicocele is when veins become enlarged inside your scrotum (the pouch of skin that holds your testicles). These veins are called the pampiniform plexus.
                                                                                <div style="text-align:center; padding:20px 0;">
                                                                                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/28.jpg">
                                                                                </div>
                                                                                <p>About 15 out of 100 men have varicoceles. It's hard to predict which of these 15 will have fertility problems caused by their varicocele. But about 4 in every 10 men tested for fertility problems have a varicocele and decreased sperm movement. Varicoceles can affect fertility by reducing blood flow and raising the temperature of the testicles. This can cause the testicles to produce fewer sperm, and sperm that is produced might not be healthy.</p>

                                                                                <p>Often, varicoceles are not treated. Treatment is offered for males who have:</p>
                                                                                <ul>
                                                                                    <li>fertility problems (problems fathering a child)</li>
                                                                                    <li>pain</li>
                                                                                    <li>testicular hypotrophy</li>
                                                                                </ul>
                                                                                <p>There are no drugs to treat or prevent varicoceles.</p>

                                                                                <p>There are many ways to do varicocele surgery. All involve blocking the blood flow in the pampiniform plexus veins. Microscopic varicocelectomy with a micro doppler gives the best results.</p>
                                                                                <p>The patient may be able to return to normal, non-strenuous activities after 2 days. As long as they are not uncomfortable, they may return to more strenuous activity, such as exercising, after 2–4 weeks.</p>
                                                                                <p>Most men do not need treatment for retrograde ejaculation because they are still able to enjoy healthy sex life and the condition does not have adverse effects on their health.</p>
                                                                                <p>Men who want to have children can have sperm taken from their urine for use in artificial intrauterine insemination (IUI) or in-vitro fertilization (IVF).</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Vasectomy<i class="fa fa-arrow-down pull-right panel-icon" aria-hidden="true"></i></a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapse3" class="panel-collapse collapse">
                                                                            <div class="panel-body">
                                                                                <div style="text-align:center; padding:20px 0;">
                                                                                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/29.jpg">
                                                                                    <img src="https://www.nuhospitals.com/assets/images/specialities/andrology/30.jpg">
                                                                                </div>
                                                                                <p>It's time men came forward to get a simple vasectomy done instead of forcing their wives to get complex tubectomy done as a birth control measure. Vasectomy procedure takes around an hour (including the anesthesia time) and is a day-care procedure. After the surgery, one should wait for 3 months to check the semen (or 20 ejaculates, whichever comes first) to see if you’re sperm-free. Till then, one should wear a condom during sexual intercourse.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div></section>
                                </article>
                            </div>
@endsection