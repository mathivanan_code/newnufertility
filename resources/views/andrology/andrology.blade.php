@extends('layouts/andrology-layout')

@section('meta-title')
<title>Best Andrology Treatment Hospital In Bangalore | NU Fertility Hospitals</title>

@endsection
@section('meta-description')
<meta name="description" content="Andrology is a subspecialty of Urology which involves treatment of various conditions causing male infertility and male sexual dysfunction. Get best Best Andrology Treatment in Bangalore at NU Fertility." />

@endsection


@section('banner_image')
<img class="img-responsive w-100 h-100"  style="width:100%" src="{{asset('images/andrology/Overview_1366.jpg')}}" alt="">
@endsection


@section('content')
<div class="content">
                            <article id="post-187" class="services_page itemscope post-187 cpt_services type-cpt_services status-publish has-post-thumbnail hentry cpt_services_group-services" itemscope="" itemtype="http://schema.org/Article">
                                <section class="services_page_header">
                                    <div class="services_page_featured">
                                        <img 
                                         src="images/andrology/Overview_650.jpg" 
                                         class="img-fluid attachment-felizia-thumb-huge size-felizia-thumb-huge wp-post-image" alt="Fetal Madcine" itemprop="image" 
             >
                                    </div>
                                    <h2 class="services_page_title">Overview</h2>
                                </section>
                                <section class="services_page_content entry-content" itemprop="articleBody">
                                    <div class="vc_row wpb_row vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 sc_layouts_column_icons_position_left">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_text_column wpb_content_element">
                                                        <div class="wpb_wrapper">
                                                            <p class="pjust">Andrology is a subspecialty of Urology which involves treatment of various conditions causing male infertility and male sexual dysfunction. In short, Andrology deals with male reproductive health, a counterpart to Gynaecology. </p>
                                                            <p class="pjust">
                                                            The late arrival of the andrological discipline on the stage of modern medicine gave it the advantage of utilizing all the modern achievements of basic and clinical science, from molecular biology to genetics, reaching levels of high quality. NU Hospitals is one of the few centres to proudly have a dedicated Andrology unit with up-to-date infrastructure offering international expertise.
                                                            </p>
                                                            <div style="text-align:center;padding:20px 0;">
                                                              <img
                                                              class="img-fluid"
                                                              src="https://www.nuhospitals.com/assets/images/specialities/andrology/couples-img.jpg">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                   
                                </section>
                            </article>
                        </div>
@endsection