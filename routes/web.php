<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AdminController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/about-nu', function () {
    return view('about-us');
});
Route::get('/why-choose-us', function () {
    return view('why-choose-us');
});

// fertility routes

Route::get('/female-infertility', function () { return view('reproductive/female-infertility');});
Route::get('/ovulation-induction', function () { return view('reproductive/ovulation-induction');});
Route::get('/iui', function () { return view('reproductive/iui');});
Route::get('/ivf', function () { return view('reproductive/ivf');});
Route::get('/embryo-transfer', function () { return view('reproductive/embryo-transfer');});
Route::get('/era', function () { return view('reproductive/era');});
Route::get('/ivm', function () { return view('reproductive/ivm');});
Route::get('/fertility-preservation', function () { return view('reproductive/fertility-preservation');});
Route::get('/laparoscopic-surgery-and-hysteroscopic', function () { return view('reproductive/laparoscopic-surgery-and-hysteroscopic');});


// andrology routes
Route::get('/andrology', function () { return view('andrology/andrology');});
Route::get('/men-sexual-health', function () { return view('andrology/men-sexual-health');});
Route::get('/male-fertility-problems', function () { return view('andrology/male-fertility-problems');});
Route::get('/premarital-package', function () { return view('andrology/premarital-package');});
Route::get('/mens-health-clinic', function () { return view('andrology/mens-health-clinic');});


// doctors
Route::get('/dr-ashwini-ivf-specialist-bangalore', function () { return view('doctor/reproductive/dr-ashwini-ivf-specialist-bangalore');});
Route::get('/dr-sneha-female-fertility-specialist', function () { return view('doctor/reproductive/dr-sneha-female-fertility-specialist');});
Route::get('/dr-kavya-pradeep-gynaecologist', function () { return view('doctor/reproductive/dr-kavya-pradeep-gynaecologist');});
Route::get('/prakrutha-sreenath', function () { return view('doctor/reproductive/prakrutha-sreenath');});
Route::get('/dr-pramod-krishnappa-andrologist', function () { return view('doctor/andrology/dr-pramod-krishnappa-andrologist');});

//international-patients

Route::get('/international-patients', function () {
    return view('international-patients');
});

//contact
Route::get('/contact', function () {
    return view('contact');
});

// blog
Route::get('/blog', function () {
    return view('blog');
});



// appointment
Route::get('/book-an-appointment', function () {
    return view('consultation/book-an-appointment');
});
Route::get('/book-video-consultation', function () {
    return view('consultation/book-video-consultation');
});



// blog

// blog/egg-freezing-you-can-have-control-over-your-ticking-biological-clock/
// blog/the-easiest-way-to-get-rid-of-infertility/
// blog/5-things-you-need-to-know-about-pelvic-floor-disorder/
// blog/dealing-with-pcos-related-infertility/
// blog/penile-implant-in-the-era-of-viagra/
// blog/low-testosterone-hypogonadism/
// blog/erectile-dysfunction-ed/
// things-to-know-before-opting-for-ivf/
// blog/tips-to-boost-male-fertility-sperm-count/
// blog/effects-of-alcohol-on-fertility/
// blog/seeing-an-infertility-doctor-does-not-mean-you-have-to-do-an-ivf/
// blog/smoking-and-fertility/
// blog/things-you-should-know-when-battling-with-infertility/
// blog/ovulation-induction/
// blog/myths-about-infertility/
// blog/egg-freezing-you-can-have-control-over-your-ticking-biological-clock/


Route::get('/login', function () {
    return redirect('/admin/login');
});

Route::get('/dashboard', function () {
    return redirect('/admin/dashboard');
});

Route::get('/admin/login',[AdminController::class,'login']);
Route::get('/admin/dashboard',[AdminController::class,'dashboard']);
Route::group(['middleware' => 'web','prefix' => 'admin'], function () {
    Route::get('login',[AdminController::class,'login']);
    Route::get('dashboard',[AdminController::class,'dashboard']);
    Route::post('login',[AdminController::class,'postLogin']);
});
Route::get('logout',[AdminController::class,'logout']);
Route::post('blogpost',[AdminController::class,'blogPost']);
Route::get('blogedit/{id}',[AdminController::class,'blogEdit']);
Route::get('blogdelete/{id}',[AdminController::class,'deleteBlog']);
Route::get('blogeadd',[AdminController::class,'blogAdd']);
Route::get('blog',[AdminController::class,'categoryView']);
Route::get('blog/{id}',[AdminController::class,'blogView']);
Route::get('category/{id}',[AdminController::class,'categoryView']);
Route::post('ajax/add-comments',[AdminController::class,'BlogComments']);




Route::get('/page-not-found', function () {
    return view('page-not-found');
});