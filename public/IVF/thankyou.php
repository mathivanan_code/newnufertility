<!DOCTYPE html>
<html>

<head>
	<title>Thank You</title>
	<!-- Event snippet for IVF Treatment 2022 conversion page --> <script> gtag('event', 'conversion', {'send_to': 'AW-964416900/CpiXCIToktwBEISr78sD'}); </script>

	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1" />
	<link rel="stylesheet" type="text/css" href="index.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

	<link rel="stylesheet" type="text/css" href="owlcarousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="owlcarousel/assets/owl.theme.default.min.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- Global site tag (gtag.js) - Google Ads: 964416900 -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=AW-964416900"></script>
	<script>
		window.dataLayer = window.dataLayer || [];

		function gtag() {
			dataLayer.push(arguments);
		}
		gtag('js', new Date());
		gtag('config', 'AW-964416900');
	</script>
	<!-- Event snippet for IVF - 2020 conversion page -->
	<script>
		gtag('event', 'conversion', {
			'send_to': 'AW-964416900/CpiXCIToktwBEISr78sD'
		});
	</script>
</head>

<body>

	<style type="text/css">
		.owl-nav {
			display: none;
		}

		.owl-theme .owl-nav {

			display: none;
		}

		.owl-theme .owl-dots .owl-dot {
			display: inline-block;
			zoom: 1;
			width: auto;
		}

		.owl-carousel .owl-item img {
			width: auto;
		}

		.owl-carousel .owl-item .inherit2 {
			width: inherit;
		}

		.subsection2 {
			padding-left: 100px;
			padding-right: 100px;
			padding-bottom: 15px;
			display: flex;
			align-items: center;
			padding-bottom: 15px;
		}


		.section2 {
			border: 1px solid white;
			background-color: #eef6fd;
			border-radius: 20px;
			padding: 0px 0px;
		}

		.section4 {
			background-color: #eef6fd;
			border-radius: 20px;
			padding: 15px;
		}

		.title3 {
			font-family: 'Playfair Display', serif;
			padding: 0px 0px;
		}

		.bannerSection {
			float: right;
		}

		.r-width {
			max-width: 100%;
		}

		footer.container-fluid.bg-4.pt-4.pb-4 {
			padding: 35px;
		}

		.bg-4 {
			background-color: #eef6fd;
			/* Black Gray */
		}


		@media(min-width: 320px) and (max-width: 375px) {
			.title {
				font-size: x-large;
			}

			.list {
				font-size: medium;
			}

		}

		@media(min-width: 320px) and (max-width: 350px) {
			.bookButton {
				padding: 0px 15px;
			}
		}

		@media(min-width: 768px) and (max-width: 1020px) {
			.extrapad2 {
				padding-bottom: 70px;
			}

			.para {
				font-size: 15px;
			}

			#para1 {
				padding-top: 15px;
			}

			#para2 {
				padding-top: 5px;
			}

			.extrapad {
				padding-bottom: 85px;
			}

			.procedure {
				font-size: inherit;
			}

			.conditionText p {
				font-size: inherit;
			}

			.head {
				font-size: inherit;
			}

			.title {
				font-size: xx-large;
			}
		}

		@media (min-width: 320px) and (max-width: 500px) {
			.section1 {
				background-size: 100%;
			}

			.bannerSection {
				padding-top: 100px;
			}

			.extrapad {
				padding-bottom: 15px;
				padding-top: 27px;
			}

			.extrapad2 {
				padding-bottom: 15px;
			}
		}

		@media(min-width: 1024px) and (max-width: 1440px) {
			.p-size {
				display: inline-block;
				font-size: 15px;
			}

			.p-size2 {
				display: inline-block;
				font-size: 15px;
			}

			.p-size3 {
				display: inline-block;
				font-size: 14px;
			}


		}

		@media (min-width: 1024px) and (max-width: 1440px) {
			#para2 {
				padding-top: 5px;
			}

			.extrapad2 {
				padding-bottom: 45px;
			}

			#para1 {
				padding-top: 25px;
			}
		}

		@media(min-width: 320px) and (max-width: 1020px) {
			.subsection2 {
				padding-left: 0px;
				padding-right: 0px;
			}
		}

		.owl-carousel .item {
			min-height: 297px;
		}
	</style>

	<!-- Start Of Header -->

	<nav class="navbar navbar-light bg-light" style="position: fixed;top: 0;width: 100%;z-index: 1;">
		<a class="navbar-brand" href="https://www.nuhospitals.com/">
			<img src="https://www.nuhospitals.com/Urogynaecology/images/NU-Logo.png" width="100px" height="auto" alt="">
		</a>
		<form class="form-inline book">
			<button type="button" onclick="focusFrm()" class="bookButton my-2 my-sm-0" id="button2">Book an Appointment</button>
		</form>
	</nav>
	<!-- Start of Andrology division Section -->
	<section>
		<div class="container section2 mb-5 text-center" style="margin-top: 7rem;">
			<h1 class="title">Thank You</h1>
			<p>Our Customer Care Team Will Call You Shortly</p>
		</div>
	</section>
	<!-- End  of Testimonials -->

	<!-- Start of Why NU -->

	<!-- End of Why NU -->

	<!-- Start of Virtual Tour -->

	<div class="container mb-5">
		<h1 class="title3">Virtual Tour</h1>
		<div style="border-radius: 10px;">
			<iframe src="https://www.google.com/maps/embed?pb=!4v1555065986205!6m8!1m7!1sCAoSLEFGMVFpcFBYNFR5YXRDSzA4SEFLSFFGOWdUaDNFUURxMVg5akZrWVN1emNl!2m2!1d13.01192608813473!2d77.55285048587712!3f23.21952!4f0!5f0.7820865974627469" width="100%" height="300" frameborder="0" style="border-radius: 10px;" allowfullscreen=""></iframe>
		</div>
	</div>

	<!-- End of Virtual Tour -->

	<!-- Start of Footer -->

	<!-- <div class="container-fluid">
			<img src="images/footer.jpg" width="100%" height="auto">
		</div> -->

	<footer class="container-fluid bg-4 pt-4 pb-4">
		<img src="https://www.nuhospitals.com/Urogynaecology/images/nu-hospitals-logo.png" alt="NU Hospitals Logo" class="img-fluid log-width lazyload" width="105px">

		<p class="mt-3"><b>About NU</b> –NU Hospitals has been pioneering in Nephrology, Urology and Fertility (IVF) treatment over the last 20 years. Our highly specialised experts are richly experienced in treating rare and complex conditions. It was one of the first in Karnataka to conduct a successful kidney transplantation surgery and also the first to equip Urology department with the most advanced flexible endoscopes. Having a robust team of subspecialty specific urology team at NU has significantly improved the overall success rates. Men’s Health Clinic is one of its kind in South-India dealing with Men’s Sexual and Reproductive Health. Our Male and Female Fertility (IVF) specialists comprehensively support the couples with the latest treatment and heartfelt compassion every step along the path to parenthood. At NU, every aspect of healthcare is coordinated and the entire team works together to provide exactly the care one needs. We are constantly updating the policies and collaborations to ensure that people in need are getting access to the most cutting-edge care and treatment. </p>
		<hr>


		<p class=""><b>Public Notice:</b> NU Hospitals would like to inform the general public that NU Hospitals practices all organ transplants in accordance with The Transplantation of Human Organs Act 1994.</p>
		<p class="">NU Hospitals do not buy or sell any organ and seriously condemn the act. NU Hospitals do not by any nature seek your personal information such as name, telephone, address or banking details for any purpose.</p>
		<hr>

		<div class="row r-width">

			<div class="col-sm-12 col-md-6 col-lg-6">
				<p><b>Our Locations</b></p>
				<p><b>Karnataka -</b> Bengaluru (Rajajinagar &amp; Padmanabhanagar), Shivamogga</p>
				<p><b>Tamil Nadu -</b> Ambur</p>
			</div>


			<div class="col-sm-12 col-md-2 col-lg-2">
				<div>
					<img src="https://www.nuhospitals.com/Urogynaecology/images/ISAR.png" alt="nabh-bl-footer-logo" class="img-fluid" style="background: #fcfeff;padding: 15px 15px 0px;">
				</div>
			</div>
			<div class="col-sm-12 col-lg-4 col-md-4">
				<img src="https://www.nuhospitals.com/Urogynaecology/images/NABH-NABL-etc.jpg" alt="isar-footer-logo" class="img-fluid">
			</div>

		</div>
		<!-- <div class="whatsapp-wrapper">
		  	
		        <a href="https://wa.me/919606470118" target="_blank"><img src="https://www.nuhospitals.com/laproscopy/img/whatsapp-icon.png" alt="Whatsapp Icon" class="img-fluid" width="60px;"></a>
		    </div> -->
	</footer>

	<!-- End of Footer -->

	<script src="owlcarousel/owl.carousel.js"></script>
</body>

</html>

<!-- https://www.nuhospitals.com/Urogynaecology/ -->