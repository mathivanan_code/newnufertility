(function($){
	$('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
	  if (!$(this).next().hasClass('show')) {
		$(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
	  }
	  var $subMenu = $(this).next(".dropdown-menu");
	  $subMenu.toggleClass('show');

	  $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
		$('.dropdown-submenu .show').removeClass("show");
	  });

	  return false;
	});

    $(window).scroll(function(){
        var sticky = $('.menu_area'),
            scroll = parseInt($(window).scrollTop());
          console.log(scroll);
        if (scroll >= 140){
          $('#menu_area').addClass('fixed');
        } else{
         $('#menu_area').removeClass('fixed');
      
        }

      var  myID =$('advertisement');
      if (scroll >= 140){
        $('#advertisement').addClass('show');
      } else{
       $('#advertisement').removeClass('hide');    
      }


      });




      $('#ctry_sel_id').click(function(e){
              if($(this).hasClass('up')){
                  $(this).removeClass('up');
                  $(this).addClass('down');
                  $('.changes_arrow').removeClass('fa-caret-up').addClass('fa-caret-down');
                  $('#country_code').show();
              }else{
                  $(this).removeClass('down');
                  $(this).addClass('up');
                  $('.changes_arrow').removeClass('fa-caret-down').addClass('fa-caret-up');
                  $('#country_code').hide();
              }
          })
          $('.country_sel').click(function(e){
          $('#ctry_sel_id').attr('src',$(this).find('img').attr('src'));
          $c_code =$(this).attr('data-value');
          $('#country_code_val').val($c_code);
          $('#ctry_sel_id').trigger('click');
          });
})(jQuery)
// var stickyOffset = $('.menu_area').offset().top;



function toggleContent1() {
  var x = document.getElementById("more-content-1");   
  if (x.style.display === "none") {
    x.style.display = "block";
    document.getElementById("read-more-btn-1").innerHTML = "Read less";
  } else {
    x.style.display = "none";
     document.getElementById("read-more-btn-1").innerHTML = "Read More";
  }
}

function toggleContent2() {
  var x = document.getElementById("more-content-2");   
  if (x.style.display === "none") {
    x.style.display = "block";
    document.getElementById("read-more-btn-2").innerHTML = "Read less";
  } else {
    x.style.display = "none";
     document.getElementById("read-more-btn-2").innerHTML = "Read More";
  }
}

function toggleContent3() {
  var x = document.getElementById("more-content-3");   
  if (x.style.display === "none") {
    x.style.display = "block";
    document.getElementById("read-more-btn-3").innerHTML = "Read less";
  } else {
    x.style.display = "none";
     document.getElementById("read-more-btn-3").innerHTML = "Read More";
  }
}


function showContent(par) {
  if (par == 1) {
      $(".expertise-content").show();
      $(".professinal-content").hide();
      $(".publication-content").hide();
      $(".awards-content").hide();
      $(".surgical-content").hide();
      $(".presentation-content").hide();
      $(".gallery-content").hide();
      $(".work-shops").hide();
      $(".conferences-content").hide();
      $(".blog-content").hide();

      $("#tab-7").removeClass("active");
      $("#tab-1").addClass("active");
      $("#tab-2").removeClass("active");
      $("#tab-3").removeClass("active");
      $("#tab-4").removeClass("active");
      $("#tab-5").removeClass("active");
      $("#tab-6").removeClass("active");
      $("#tab-8").removeClass("active");
      $("#tab-9").removeClass("active");
      $("#tab-10").removeClass("active");

  } else if (par == 2) {
      $(".expertise-content").hide();
      $(".professinal-content").show();
      $(".publication-content").hide();
      $(".awards-content").hide();
      $(".surgical-content").hide();
      $(".presentation-content").hide();
      $(".gallery-content").hide();
      $(".work-shops").hide();
      $(".conferences-content").hide();
      $(".blog-content").hide();

      $("#tab-7").removeClass("active");
      $("#tab-1").removeClass("active");
      $("#tab-2").addClass("active");
      $("#tab-3").removeClass("active");
      $("#tab-4").removeClass("active");
      $("#tab-5").removeClass("active");
      $("#tab-6").removeClass("active");
      $("#tab-8").removeClass("active");
      $("#tab-9").removeClass("active");
      $("#tab-10").removeClass("active");
  } else if (par == 3) {
      $(".expertise-content").hide();
      $(".professinal-content").hide();
      $(".publication-content").show();
      $(".awards-content").hide();
      $(".surgical-content").hide();
      $(".presentation-content").hide();
      $(".gallery-content").hide();
      $(".work-shops").hide();
      $(".conferences-content").hide();
      $(".blog-content").hide();

      $("#tab-7").removeClass("active");
      $("#tab-1").removeClass("active");
      $("#tab-2").removeClass("active");
      $("#tab-3").addClass("active");
      $("#tab-4").removeClass("active");
      $("#tab-5").removeClass("active");
      $("#tab-6").removeClass("active");
      $("#tab-8").removeClass("active");
      $("#tab-9").removeClass("active");
      $("#tab-10").removeClass("active");
  } else if (par == 4) {
      $(".expertise-content").hide();
      $(".professinal-content").hide();
      $(".publication-content").hide();
      $(".awards-content").show();
      $(".surgical-content").hide();
      $(".presentation-content").hide();
      $(".gallery-content").hide();
      $(".work-shops").hide();
      $(".conferences-content").hide();
      $(".blog-content").hide();
      $("#tab-7").removeClass("active");
      $("#tab-1").removeClass("active");
      $("#tab-2").removeClass("active");
      $("#tab-3").removeClass("active");
      $("#tab-4").addClass("active");
      $("#tab-5").removeClass("active");
      $("#tab-6").removeClass("active");
      $("#tab-8").removeClass("active");
      $("#tab-9").removeClass("active");
      $("#tab-10").removeClass("active");
  } else if (par == 5) {
      $(".expertise-content").hide();
      $(".professinal-content").hide();
      $(".publication-content").hide();
      $(".awards-content").hide();
      $(".surgical-content").show();
      $(".presentation-content").hide();
      $(".gallery-content").hide();
      $(".work-shops").hide();
      $(".conferences-content").hide();
      $(".blog-content").hide();
      $("#tab-7").removeClass("active");
      $("#tab-1").removeClass("active");
      $("#tab-2").removeClass("active");
      $("#tab-3").removeClass("active");
      $("#tab-4").removeClass("active");
      $("#tab-5").addClass("active");
      $("#tab-6").removeClass("active");
      $("#tab-8").removeClass("active");
      $("#tab-9").removeClass("active");
      $("#tab-10").removeClass("active");
  } else if (par == 6) {
      $(".expertise-content").hide();
      $(".professinal-content").hide();
      $(".publication-content").hide();
      $(".awards-content").hide();
      $(".surgical-content").hide();
      $(".presentation-content").show();
      $(".gallery-content").hide();
      $(".work-shops").hide();
      $(".conferences-content").hide();
      $(".blog-content").hide();
      $("#tab-7").removeClass("active");
      $("#tab-1").removeClass("active");
      $("#tab-2").removeClass("active");
      $("#tab-3").removeClass("active");
      $("#tab-4").removeClass("active");
      $("#tab-5").removeClass("active");
      $("#tab-6").addClass("active");
      $("#tab-8").removeClass("active");
      $("#tab-9").removeClass("active");
      $("#tab-10").removeClass("active");
  } else if (par == 7) {
      $(".expertise-content").hide();
      $(".professinal-content").hide();
      $(".publication-content").hide();
      $(".awards-content").hide();
      $(".surgical-content").hide();
      $(".presentation-content").hide();
      $(".gallery-content").show();
      $(".work-shops").hide();
      $(".conferences-content").hide();
      $(".blog-content").hide();
      $("#tab-7").addClass("active");
      $("#tab-1").removeClass("active");
      $("#tab-2").removeClass("active");
      $("#tab-3").removeClass("active");
      $("#tab-4").removeClass("active");
      $("#tab-5").removeClass("active");
      $("#tab-6").removeClass("active");
      $("#tab-8").removeClass("active");
      $("#tab-9").removeClass("active");
      $("#tab-10").removeClass("active");
  }else if (par == 8) {
    $(".expertise-content").hide();
    $(".professinal-content").hide();
    $(".publication-content").hide();
    $(".awards-content").hide();
    $(".surgical-content").hide();
    $(".presentation-content").hide();
    $(".gallery-content").hide();
    $(".conferences-content").hide();
    $(".blog-content").hide();
    $(".work-shops").show();
    $("#tab-8").addClass("active");
    $("#tab-1").removeClass("active");
    $("#tab-2").removeClass("active");
    $("#tab-3").removeClass("active");
    $("#tab-4").removeClass("active");
    $("#tab-5").removeClass("active");
    $("#tab-6").removeClass("active");
    $("#tab-7").removeClass("active");
    $("#tab-9").removeClass("active");
    $("#tab-10").removeClass("active");
}
else if (par == 9) {
  $(".expertise-content").hide();
  $(".professinal-content").hide();
  $(".publication-content").hide();
  $(".awards-content").hide();
  $(".surgical-content").hide();
  $(".presentation-content").hide();
  $(".gallery-content").hide();
  $(".work-shops").hide();
  $(".conferences-content").show();
  $(".blog-content").hide();

  $("#tab-9").addClass("active");
  $("#tab-1").removeClass("active");
  $("#tab-2").removeClass("active");
  $("#tab-3").removeClass("active");
  $("#tab-4").removeClass("active");
  $("#tab-5").removeClass("active");
  $("#tab-6").removeClass("active");
  $("#tab-7").removeClass("active");
  $("#tab-8").removeClass("active");
  $("#tab-10").removeClass("active");
}
else if (par == 10 ){
  $(".expertise-content").hide();
  $(".professinal-content").hide();
  $(".publication-content").hide();
  $(".awards-content").hide();
  $(".surgical-content").hide();
  $(".presentation-content").hide();
  $(".gallery-content").hide();
  $(".work-shops").hide();
  $(".conferences-content").hide();
  $(".blog-content").show();


  $("#tab-10").addClass("active");
  $("#tab-1").removeClass("active");
  $("#tab-2").removeClass("active");
  $("#tab-3").removeClass("active");
  $("#tab-4").removeClass("active");
  $("#tab-5").removeClass("active");
  $("#tab-6").removeClass("active");
  $("#tab-7").removeClass("active");
  $("#tab-8").removeClass("active");
  $("#tab-9").removeClass("active");
}

 
}


window.onload = function() {
  $(".expertise-content").show();
}
var  myID =$('advertisement');

      if (scroll >= 140){

        $('#advertisement').addClass('show');

      } else{

       $('#advertisement').removeClass('hide');    

      }


      jQuery(document).ready(function(){
jQuery.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    // Join/Joined the event guests  by  logged user
    jQuery('.post_comment').on('click', function (e) {
      e.preventDefault();
      var blog_id = $('#blog_id').val();
      var user_id = $('#user_id').val();
      var description = tinyMCE.get('tinymce').getContent();
      // alert(tinyMCE.get('tinymce').getContent({format : 'text'}));
      var text =tinyMCE.get('tinymce').getContent({format : 'text'}).trim();
      if(text == ""){
        alert("comment can't empty");
        return true;
      }else if(text.length < 5){
        alert("comment character should be greater than 4");
        return true;
      }      
      jQuery.post(
        SP_source() + 'ajax/add-comments', 
        {user_id: user_id,blog_id:blog_id,description:description, csrf_token: jQuery('[name="csrf-token"]').attr('content')}, function (data) {
          if (data.status == 200) {
              tinyMCE.activeEditor.setContent('');
              jQuery('.user_comments').prepend('<div class="user_comments_single">'+description+'</div>');
          }
      });
  });

      });