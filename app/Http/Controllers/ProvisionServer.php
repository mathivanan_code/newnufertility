<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProvisionServer extends Controller
{
    public function login() {
        if(Auth::user()){
            return redirect('admin/dashboard');
        }
        return view('admin.login');
    }
}
