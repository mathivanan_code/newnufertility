<?php

namespace App\Http\Controllers;

use App\Rules\MatchOldPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\Blog;
use App\Models\Comment;
use DNS2D ;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\File;

class AdminController extends Controller
{

   
    public function __construct() {
        $this->middleware('web', ['except' => ['login', 'postLogin', 'changePassword','postChangePassword','BlogComments']]);
   //dashboard
    }


    public function login() {
        if(Auth::user()){
            return redirect('admin/dashboard');
        }
        return view('admin.login');
    }

    public function changePassword() {
        return view('admin.change_password');
    }
    public function postChangePassword(Request $request) {

        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'confirm_new_password' => ['same:new_password'],
        ]);

        // $messages = [
        //     'new_confirm_password.same' => 'Password Confirmation should match the Password',
        // ];
        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);
   
        Auth::logout();
        return redirect('admin/login');
    }


    public function dashboard(){
        if(!Auth::user()){
            return redirect('admin/login');
        }
        $blogs = Blog::all();
        return view('admin.admin',compact('blogs'));
    }


    public function blogAdd(){
        if(!Auth::user()){
            return redirect('admin/login');
        }
        $categories =$this->category(); 
        $mode = 'create';
        return view('admin.admin-dashboard',compact('mode','categories'));
    }
    public function blogEdit($id){
        $blogs = Blog::find($id);
        $mode = 'edit';
        $blog_id = $id;
        $categories =$this->category(); 
        return view('admin.admin-dashboard',compact('mode','categories','blogs','blog_id'));
    }

    public function blogPost(Request $request){
       $imageupload = true;
       if($request->blog_mode == 'edit'){
           $blog_create = Blog::find($request->blog_id);
           if(isset($_FILES['blog_image']) && $_FILES['blog_image']['name'] != $blog_create->image){
            $imageupload = true;
           }else{
            $imageupload = false;
            $request->blog_image = 'null';
           }
           
       }else{
        $blog_create = new Blog;
       }
       $request->validate([
        'blog_title' => ['required'],
        'blog_subtitle' => ['required'],
        'blog_url' => ['required'],
        'blog_description' => ['required'],
        'blog_image' => ['exclude_if:blog_mode,edit|required'],
    ]);

        if(isset($_FILES['blog_image']) && $imageupload)
        {
          $filename = $_FILES['blog_image']['name'];
          $name_of_uploaded_file =  $_FILES['blog_image'];
          $file_tmp = $_FILES['blog_image']['tmp_name'];
          $file_ext = explode(".", $filename);
          $file_ext = strtolower(end($file_ext));
          $expensions= array("jpeg","jpg","png","pdf","xls","xlsx");
          if(in_array($file_ext,$expensions) === true)
          {
            // $mail['attachpath'] =  storage_path().'/uploads/Contact_form/'.$filename;
            move_uploaded_file($file_tmp,SITE_ROOT.'/images/blog/'.$filename);
          }
          $blog_create->images =$filename;
        }

        $blog_create->title = $request->blog_title;   
        $blog_create->subtitle = $request->blog_subtitle;
        $blog_create->url = 'blog/'.$request->blog_url;
        
        $blog_create->description = $request->blog_description;
        $blog_create->category = $request->category;
        $blog_create->sticky = $request->programme_list;
        if(!empty($request->blog_meta_title)){
            $blog_create->meta_title = $request->blog_meta_title;
        }
        if(!empty($request->blog_meta_desc)){
            $blog_create->meta_description = $request->blog_meta_desc;
        }
        $blog_create->save();
        return redirect()->back();
    }


    public function deleteBlog($id){
        $delEvent = Blog::where('id',$id)->delete();
        return redirect()->back();
    }

    public function logout() {
        Auth::logout();
        return redirect('admin/login');
    }

    public function postLogin(Request $request) {
        if(Auth::user()){
            return redirect('admin/dashboard');
        }else{
        $validate = validator($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        
        if ($validate->fails()) {
            return back()->withErrors($validate)->withInput();
        }
       
        $credentials = ['email' => filter_var($request->get('email'),FILTER_SANITIZE_EMAIL), 'password' => $request->get('password')];
        $loginAttempt = Auth::attempt($credentials);

        if ($loginAttempt) {
            return redirect('admin/dashboard');
        }
        $notification = array(
            'message' => 'Whoops! There were some problems with your input.Invalid credentials.',
            'alert-type' => 'error'
        );
        return back()->with($notification);
        }
    }

    public function category(){
        return array(
        'erectile-dysfunction'=>"Erectile Dysfunction",
        'fertility'=>"Fertility",
        'hysteroscopy'=>"Hysteroscopy",
        'in-vitro-fertilization'=>"In Vitro Fertilization",
        'infertility'=>"Infertility",
       'intrauterine-insemination' =>"Intrauterine Insemination(IUI)",
        'laparoscopy-treatment'=>"Laparoscopy treatment",
        'male-infertility'=>"Male Infertility",
        'mother-hood'=>"Mother Hood",
        'pcos'=>"PCOS",
        'pregnancy'=>"Pregnancy",
        'sperm-donation'=>"Sperm Donation"
        );
    }
    public function blogView($id){
        $url = 'blog/'.$id;
        $blog = Blog::where('url',$url)->first();
        if($blog == null){
            return redirect('/page-not-found');
        }
        $blog_comments=$this->getComment($blog->id);
        $blog_rects = Blog::limit(5)->orderBy('updated_at','DESC')->get();
        $blog_stickys = Blog::where('sticky','yes')->limit(5)->orderBy('updated_at','DESC')->get();
        $blog_recents = array();
        foreach($blog_stickys as $blog_sticky){
            $blog_recents[$blog_sticky->title] = $blog_sticky->url;
        }
        foreach($blog_rects as $blog_rect){
            $blog_recents[$blog_rect->title] = $blog_rect->url;
        }
        $blog_recents = array_slice($blog_recents,0,5);
        $blog_categorys = $this->category();
        return view('blog',compact('blog_recents','blog_categorys','blog','blog_comments'));
    }
    public function categoryView($id=null){
        
        // dd($blogs);
        if($id != null){
            $blogs = Blog::where('category',$id)->paginate(5);
        }else{
            $blogs = Blog::paginate(5);
        }
        if($blogs == null){
            return redirect('/page-not-found');
        }
        $blog_rects = Blog::limit(5)->orderBy('updated_at','DESC')->get();
        $blog_stickys = Blog::where('sticky','yes')->limit(5)->orderBy('updated_at','DESC')->get();
        $blog_recents = array();
        foreach($blog_stickys as $blog_sticky){
            $blog_recents[$blog_sticky->title] = $blog_sticky->url;
        }
        foreach($blog_rects as $blog_rect){
            $blog_recents[$blog_rect->title] = $blog_rect->url;
        }
        $blog_recents = array_slice($blog_recents,0,5);
        $blog_categorys = $this->category();
        //resources/views/.blade.php
        return view('caterogy',compact('blog_recents','blog_categorys','blogs'));
    }
    public function getComment($id)
    {   
    $post = Blog::with('comments')->find($id);
    return $post->comments;
    }

    public function BlogComments(Request $request){
        
        $comments = new Comment;
        $comments->user_id = (int)$request->user_id;
        $comments->blog_id = (int)$request->blog_id;
        $comments->description = $request->description;
        $comments->save();
        return response()->json(['status' => '200', 'data' => 'Added succesfully']);
    }

}
