<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Blog;


class Comment extends Model
{
    use HasFactory;


    public function blogs(){
        return $this->belongsTo('App\Models\Blog');
    }
 
}
